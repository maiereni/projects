/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.archiving.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Petre Maierean
 */
class Compressor {
    /**
     * Compresses the
     * @param input It can be either a single file or a directory
     * @return
     * @throws IOException
     */
    protected void compress(final File input, final File fOut) throws Exception {
        try (FileOutputStream os = new FileOutputStream(fOut);
             GzipCompressorOutputStream gos = new GzipCompressorOutputStream(os);
             TarArchiveOutputStream out = new TarArchiveOutputStream(gos)){
            out.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
            out.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
            out.setAddPaxHeadersForNonAsciiNames(true);
            addToArchiveCompression(out, input, null);
        }
    }

    private void addToArchiveCompression(TarArchiveOutputStream out, File file, String dir) throws IOException {
        String entry = StringUtils.isBlank(dir)? file.getName() : dir + File.separator + file.getName();
        if (file.isFile()){
            out.putArchiveEntry(new TarArchiveEntry(file, entry));
            try (FileInputStream in = new FileInputStream(file)){
                IOUtils.copy(in, out);
            }
            out.closeArchiveEntry();
        } else if (file.isDirectory()) {
            File[] children = file.listFiles();
            if (children != null){
                for (File child : children){
                    addToArchiveCompression(out, child, entry);
                }
            }
        } else {
            throw new IOException(file.getName() + " is not supported");
        }
    }

}
