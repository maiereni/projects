/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.encryption.impl;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;

/**
 * An implementation of the Encryption provider which makes use of the secret key hosted in a keystore file that is stored
 * on both ends of the exchange
 *
 * @author Petre Maierean
 */
public class SymetricEncryptionProviderImpl extends BaseEncryptionProviderImpl  {

    public SymetricEncryptionProviderImpl() throws Exception {
        super();
    }

    public SymetricEncryptionProviderImpl(String keyStorePath, String keyStorePassword, String keyPass, String keyAlias)
            throws Exception {
        super(keyStorePath, keyStorePassword, keyPass, keyAlias);
    }

    /**
     * Decrypt a string payload
     * @param payload
     * @return
     * @throws Exception
     */
    public String decrypt(String payload) throws Exception {
        assertNotBlank(payload);
        byte[] buffer = decoder.decode(payload);
        try (ByteArrayInputStream is = new ByteArrayInputStream(buffer);
             DataInputStream dat = new DataInputStream(is);
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int len = dat.readInt();
            byte[] buf = new byte[len];
            dat.readFully(buf);
            IvParameterSpec iv = new IvParameterSpec(buf);
            Cipher cipher = StringUtils.isNotBlank(cipherProvider) ?
                    Cipher.getInstance(fileEncryptionCipher, cipherProvider) : Cipher.getInstance(fileEncryptionCipher);
            cipher.init(Cipher.DECRYPT_MODE, privateKey, iv);
            byte[] rest = IOUtils.toByteArray(dat);
            buf = cipher.doFinal(rest);
            return new String(buf);
        }
    }

    /**
     * Encrypt a string payload
     * @param payload
     * @return
     * @throws Exception
     */
    public String encrypt(String payload) throws Exception {
        assertNotBlank(payload);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             DataOutputStream dat = new DataOutputStream(out);){
            Cipher cipher = getCipher(fileEncryptionCipher, cipherProvider, Cipher.ENCRYPT_MODE, privateKey);
            byte[] buffer = cipher.doFinal(payload.getBytes(StandardCharsets.UTF_8));
            byte[] iv = cipher.getIV();
            int len = iv.length;
            dat.writeInt(len);
            dat.write(iv);
            dat.write(buffer);
            dat.flush();
            out.flush();
            byte[] ret = out.toByteArray();

            return encoder.encodeToString(ret);
        }
    }

    /**
     * Encrypt a file using the private key in the cacerts
     *
     * @param in the file to encrypt
     * @return a temporary file
     * @throws Exception
     */
    @Override
    public File encryptFile(File in) throws Exception {
        assertFile(in);
        Cipher cipher = getCipher(fileEncryptionCipher, cipherProvider, Cipher.ENCRYPT_MODE, privateKey);
        File ret = null;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             DataOutputStream dat = new DataOutputStream(out);
             FileInputStream is = new FileInputStream(in);) {
            byte[] iv = cipher.getIV();
            int len = iv.length;
            dat.writeInt(len);
            dat.write(iv);

            byte[] buf = new byte[1000];
            int r = -1;
            while( (r = is.read(buf)) != -1 ) {
                byte[] sec = cipher.update(buf, 0, r);
                if (sec != null)
                    dat.write(sec);
            }
            dat.write(cipher.doFinal());
            dat.flush();
            out.flush();
            ret = saveToTempFile(out.toByteArray());
        }
        return ret;
    }

    /**
     * Decrypt a file using the public key in the cacerts
     * @param in the file to decrypt
     * @return a temporary file
     * @throws Exception
     */
    @Override
    public File decryptFile(File in) throws Exception {
        assertFile(in);
        File ret = null;
        try (FileInputStream is = new FileInputStream(in);
             DataInputStream dat = new DataInputStream(is);
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int len = dat.readInt();
            byte[] buf = new byte[len];
            dat.readFully(buf);
            IvParameterSpec iv = new IvParameterSpec(buf);
            Cipher cipher = StringUtils.isNotBlank(cipherProvider) ?
                    Cipher.getInstance(fileEncryptionCipher, cipherProvider) : Cipher.getInstance(fileEncryptionCipher);
            cipher.init(Cipher.DECRYPT_MODE, privateKey, iv);

            buf = new byte[1000];
            int r = -1;
            while( (r = dat.read(buf)) != -1 ) {
                byte[] sec = cipher.update(buf, 0, r);
                if (sec != null)
                    out.write(sec);
            }
            out.write(cipher.doFinal());
            out.flush();
            ret = saveToTempFile(out.toByteArray());
        }

        return ret;
    }

    @Override
    protected boolean processEntry(String alias, KeyStore keyStore) throws Exception {
        boolean ret = false;
        if (StringUtils.isBlank(keyAlias) || keyAlias.equals(alias)) {
            keyAlias = alias;
            KeyStore.SecretKeyEntry pk = null;
            if (StringUtils.isNotBlank(keyPass)) {
                pk = (KeyStore.SecretKeyEntry) keyStore.getEntry(alias, new KeyStore.PasswordProtection(keyPass.toCharArray()));
            }
            else {
                pk = (KeyStore.SecretKeyEntry) keyStore.getEntry(alias, null);
            }
            privateKey = pk.getSecretKey();
            publicKey = pk.getSecretKey();
            ret = true;
        }
        return ret;
    }
}
