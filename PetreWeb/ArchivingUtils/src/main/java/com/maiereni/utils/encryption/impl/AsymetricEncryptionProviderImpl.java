/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.encryption.impl;

import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.util.Base64;

/**
 * A file encryptor that auto generates a secret key to encrypt the file. The secret key is than encrypted using the
 * private key of the key store and stored in that form with the file. At decryption time, the utility is using the public
 * key of the key store to decrypt the secret key from the file which is than used to decrypt the entire file
 *
 * @author Petre Maierean
 */
public class AsymetricEncryptionProviderImpl extends BaseEncryptionProviderImpl {
    protected static final String INTERMEDIATE_CIPHER = "RSA/ECB/OAEPWithSHA1AndMGF1Padding";
    private String keyEncryptionCipher = INTERMEDIATE_CIPHER;


    /**
     * Constructor of the class
     * @throws Exception
     */
    public AsymetricEncryptionProviderImpl() throws Exception {
        super();
    }

    public AsymetricEncryptionProviderImpl(String keyStorePath, String keyStorePassword, String keyPass, String keyAlias)
            throws Exception {
        super(keyStorePath, keyStorePassword, keyPass, keyAlias);
    }

    /**
     * Get the name of the cipher used to encrypt the auto generated key
     *
     * @return
     */
    public String getKeyEncryptionCipher() {
        return keyEncryptionCipher;
    }

    /**
     * Set the name of the cipher to encrypt the auto generated key that is used for the encryption of the entire file
     * @param keyEncryptionCipher
     */
    public void setKeyEncryptionCipher(String keyEncryptionCipher) {
        this.keyEncryptionCipher = keyEncryptionCipher;
    }

    /**
     * Decrypt a string payload
     * @param payload
     * @return
     * @throws Exception
     */
    public String decrypt(String payload) throws Exception {
        assertNotBlank(payload);
        Cipher rsa = getCipher(keyEncryptionCipher, cipherProvider, Cipher.DECRYPT_MODE, publicKey);
        byte[] buffer = decoder.decode(payload);
        buffer = rsa.doFinal(buffer);
        return new String(buffer);
    }

    /**
     * Encrypt a string payload
     * @param payload
     * @return
     * @throws Exception
     */
    public String encrypt(String payload) throws Exception {
        assertNotBlank(payload);
        Cipher rsa = getCipher(keyEncryptionCipher, cipherProvider, Cipher.ENCRYPT_MODE, privateKey);
        byte[] buffer = rsa.doFinal(payload.getBytes(StandardCharsets.UTF_8));
        return encoder.encodeToString(buffer);
    }

    /**
     * Encrypt a file using the private key in the cacerts
     *
     * @param in the file to encrypt
     * @return a temporary file
     * @throws Exception
     */
    @Override
    public File encryptFile(File in) throws Exception {
        assertFile(in);

        KeyGenerator gen = KeyGenerator.getInstance(AES);
        gen.init(256);
        SecretKey key = gen.generateKey();

        byte[] keyEnc = key.getEncoded();
        Cipher rsa = getCipher(keyEncryptionCipher, cipherProvider, Cipher.ENCRYPT_MODE, privateKey);
        byte[] keySec = rsa.doFinal(keyEnc);

        File ret = null;
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();
             DataOutputStream dat = new DataOutputStream(os);
             FileInputStream is = new FileInputStream(in);) {
            int len = keySec.length;
            dat.writeInt(len);
            dat.write(keySec);
            Cipher cipher = getCipher(fileEncryptionCipher, cipherProvider, Cipher.ENCRYPT_MODE, key);
            byte[] iv = cipher.getIV();
            len = iv.length;
            dat.writeInt(len);
            dat.write(iv);

            byte[] buf = new byte[1000];
            int r = -1;
            while( (r = is.read(buf)) != -1 ) {
                byte[] sec = cipher.update(buf, 0, r);
                if (sec != null)
                    dat.write(sec);
            }
            dat.write(cipher.doFinal());
            dat.flush();
            os.flush();
            ret = saveToTempFile(os.toByteArray());
        }
        return ret;
    }

    /**
     * Decrypt a file using the private key in the cacerts
     * @param in the file to decrypt
     * @return a temporary file
     * @throws Exception
     */
    @Override
    public File decryptFile(File in) throws Exception {
        assertFile(in);
        File ret = null;
        try (FileInputStream is = new FileInputStream(in);
             DataInputStream dat = new DataInputStream(is);
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int len = dat.readInt();
            byte[] buf = new byte[len];
            dat.read(buf, 0, len);
            Cipher rsa = getCipher(keyEncryptionCipher, cipherProvider, Cipher.DECRYPT_MODE, publicKey);

            SecretKeySpec key = new SecretKeySpec(rsa.doFinal(buf), AES);
            len = dat.readInt();
            buf = new byte[len];
            dat.readFully(buf);
            IvParameterSpec iv = new IvParameterSpec(buf);

            Cipher aes = StringUtils.isNotBlank(cipherProvider) ?
                    Cipher.getInstance(fileEncryptionCipher, cipherProvider) : Cipher.getInstance(fileEncryptionCipher);
            aes.init(Cipher.DECRYPT_MODE, key, iv);

            buf = new byte[1000];
            int r = -1;
            while ((r = dat.read(buf)) != -1) {
                byte[] sec = aes.update(buf, 0, r);
                if (sec != null)
                    out.write(sec);
            }
            out.write(aes.doFinal());
            out.flush();
            ret = saveToTempFile(out.toByteArray());
        }
        return ret;
    }

    @Override
    protected boolean processEntry(String alias, KeyStore keyStore) throws Exception {
        boolean ret = false;
        if (keyStore.isKeyEntry(alias)) {
            if (StringUtils.isBlank(keyAlias) || keyAlias.equals(alias)) {
                keyAlias = alias;
                KeyStore.PrivateKeyEntry pk = null;
                if (StringUtils.isNotBlank(keyPass)) {
                    pk = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, new KeyStore.PasswordProtection(keyPass.toCharArray()));
                }
                else {
                    pk = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
                }
                privateKey = pk.getPrivateKey();
                publicKey = pk.getCertificate().getPublicKey();
                ret = true;
            }
        }
        else if (StringUtils.isNotBlank(keyAlias) && keyAlias.equals(alias)) {
            publicKey = keyStore.getKey(alias, null);
        }
        return ret;
    }

}
