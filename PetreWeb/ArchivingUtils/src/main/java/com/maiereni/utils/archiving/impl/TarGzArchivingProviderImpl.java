/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.archiving.impl;
import java.io.File;

/**
 * @author Petre Maierean
 */
public class TarGzArchivingProviderImpl extends BaseArchivingProviderImpl {
    private Compressor compressor;
    private Decompressor decompressor;

    public TarGzArchivingProviderImpl() throws Exception {
        super();
        compressor = new Compressor();
        decompressor = new Decompressor();
    }

    /**
     * Decompresses an archive file into the destination directory
     * @param archiveFile
     * @return
     * @throws Exception
     */
    @Override
    public File decompress(final File archiveFile) throws Exception {
        assertFile(archiveFile);
        File ret = getTemporaryDirectory();
        decompressor.decompress(archiveFile, ret);
        return ret;
    }

    /**
     * Compresses the input file or directory to the output file
     * @param dir
     * @throws Exception
     */
    @Override
    public File compress(final File dir) throws Exception {
        assertDirectory(dir);
        File destDir = getTemporaryDirectory();
        File ret = new File(destDir, "compressed.tar.gz");
        compressor.compress(dir, ret);
        return ret;
    }

}
