/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.encryption.impl;

import com.maiereni.utils.BaseUtility;
import com.maiereni.utils.encryption.EncryptionProvider;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Enumeration;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Petre Maierean
 */
public abstract class BaseEncryptionProviderImpl extends BaseUtility implements EncryptionProvider {
    protected static final String CACERTS = "jre/lib/security/cacerts";
    protected static final String KEY_STORE = "javax.net.ssl.keyStore";
    protected static final String KEY_STORE_PASSWORD = "javax.net.ssl.keyStorePassword";
    protected static final String KEY_PASSWORD = "javax.net.ssl.keyPassword";
    protected static final String KEY_ALIAS = "javax.net.ssl.keyAlias";
    protected static final String JKS = "JKS";
    protected static final String PKCS12 = "PKCS12";
    protected static final String PEM = "PEM";
    protected static final String FILE_CIPHER = "AES/CBC/PKCS5Padding";
    protected static final String AES = "AES";
    protected String keyStore, keyStorePass, keyPass, keyStoreType = JKS, keyAlias, fileEncryptionCipher = FILE_CIPHER,
            cipherProvider;
    protected Key privateKey, publicKey;
    protected Encoder encoder;
    protected Decoder decoder;

    private static final String DEFAULT_KEY_STORE_PATH;
    private static final String DEFAULT_KEY_STORE_PASSWORD;
    static {
        String keyStorePath = new File(System.getProperty("java.home"), CACERTS).getPath();
        DEFAULT_KEY_STORE_PATH = System.getProperty(KEY_STORE, keyStorePath);
        DEFAULT_KEY_STORE_PASSWORD = System.getProperty(KEY_STORE_PASSWORD, "changeit");
    }


    public BaseEncryptionProviderImpl() throws Exception {
        this(null, null, null, null);
    }

    public BaseEncryptionProviderImpl(String keyStorePath, String keyStorePassword, String keyPass, String keyAlias)
            throws Exception {
        this.keyStore = StringUtils.isNotBlank(keyStorePath) ? keyStorePath : DEFAULT_KEY_STORE_PATH;
        this.keyStorePass = StringUtils.isNotBlank(keyStorePassword) ? keyStorePassword : DEFAULT_KEY_STORE_PASSWORD;
        this.keyPass = keyPass;
        this.keyAlias = keyAlias;
        this.encoder = Base64.getEncoder();
        this.decoder = Base64.getDecoder();
        initialize();
    }
    /**
     * Get the name of the cipher to use for the encryption of the entire file
     * @return
     */
    public String getFileEncryptionCipher() {
        return fileEncryptionCipher;
    }

    /**
     * Sets the name of the cipher used for the encryption of the entire file
     * @param fileEncryptionCipher
     */
    public void setFileEncryptionCipher(String fileEncryptionCipher) {
        this.fileEncryptionCipher = fileEncryptionCipher;
    }

    /**
     * Get the cipher provider
     * @return
     */
    public String getCipherProvider() {
        return cipherProvider;
    }

    /**
     * Sets the cipher provider
     * @param cipherProvider
     */
    public void setCipherProvider(String cipherProvider) {
        this.cipherProvider = cipherProvider;
    }


    /**
     * Get the cipher
     * @param cipherAlgorithm the cipher algorithm
     * @param mode
     * @param key
     * @return
     * @throws Exception
     */
    protected Cipher getCipher(String cipherAlgorithm, String cipherProvider, int mode, Key key) throws Exception {
        if (StringUtils.isBlank(cipherAlgorithm)) {
            throw new Exception("The cypher algorithm cannot be -blank");
        }
        Cipher cipher = StringUtils.isNotBlank(cipherProvider) ?
                            Cipher.getInstance(cipherAlgorithm, cipherProvider) : Cipher.getInstance(cipherAlgorithm);
        cipher.init(mode, key);
        return cipher;
    }

    /**
     * Validates the availability of the cipher algorithm
     * @param cipherAlgorithm
     * @param cipherProvider
     * @throws Exception
     */
    protected void validateCipherAvailability(String cipherAlgorithm, String cipherProvider) throws Exception {
        if (StringUtils.isNotBlank(cipherAlgorithm)) {
            Set<String> algs = StringUtils.isNotBlank(cipherProvider) ?
                                    listAllCipherAlgoritms(cipherProvider) : listAllCipherAlgoritms();
            if (!algs.contains(cipherAlgorithm)) {
                throw new Exception("The cipher algorithm is not available");
            }
        }
    }

    /**
     * List all the cipher algorithms available
     * @return
     */
    protected Set<String> listAllCipherAlgoritms() {
        Set<String> algs = new TreeSet<>();
        for (Provider provider : Security.getProviders()) {
            provider.getServices().stream()
                    .filter(s -> "Cipher".equals(s.getType()))
                    .map(Provider.Service::getAlgorithm)
                    .forEach(algs::add);
        }
        return algs;
    }

    /**
     * List that cipher algorithms available for a cipher provider
     * List that cipher algorithms available for a cipher provider
     * @param cipherProvider
     * @return
     */
    protected Set<String> listAllCipherAlgoritms(String cipherProvider) {
        Set<String> algs = new TreeSet<>();
        for (Provider provider : Security.getProviders(cipherProvider)) {
            provider.getServices().stream()
                    .filter(s -> "Cipher".equals(s.getType()))
                    .map(Provider.Service::getAlgorithm)
                    .forEach(algs::add);
        }
        return algs;
    }

    protected File saveToTempFile(byte[] buffer) throws Exception{
        File ret = null;
        if (buffer != null) {
            ret = File.createTempFile("tmp", "t");
            FileUtils.writeByteArrayToFile(ret, buffer);
        }
        return ret;
    }

    /**
     * Convert the private key
     * @param algorithm
     * @param provider
     * @return
     * @throws Exception
     */
    protected SecretKey getSecretKey(String algorithm, String provider) throws Exception {

        SecretKey ret = null;
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm, provider);
        KeySpec keySpec = null;
        return ret;
    }

    private void initialize() throws Exception {
        if (StringUtils.isBlank(keyStore)) {
            throw new Exception("The Key store cannot be blank");
        }
        File f = new File(keyStore);
        try (InputStream is = f.isFile() ? new FileInputStream(f) : getClass().getResourceAsStream(keyStore)) {
            String path = f.getPath().toLowerCase();
            if (path.endsWith(".p12")) {
                this.keyStoreType = PKCS12;
            }
            else if (path.endsWith(".pem")) {
                this.keyStoreType = PEM;
            }
            else if (path.endsWith(".jks")) {
                this.keyStoreType = JKS;
            }
            initialize(is);
        }
    }

    private void initialize(InputStream is) throws Exception {
        if (is  == null) {
            throw new Exception("The input stream is null");
        }
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(is, StringUtils.isNotBlank(keyStorePass) ? keyStorePass.toCharArray(): null);
        Enumeration<String> es = keyStore.aliases();
        while(es.hasMoreElements()) {
            String alias = es.nextElement();
            if (processEntry(alias, keyStore)) {
                break;
            }
        }
        if (privateKey == null || publicKey == null) {
            throw new Exception("Could not initializa from the provided keystore");
        }
    }

    protected abstract boolean processEntry(String alias, KeyStore keyStore) throws Exception;
}
