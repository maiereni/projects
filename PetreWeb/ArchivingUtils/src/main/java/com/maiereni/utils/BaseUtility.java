/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
/**
 * @author Petre Maierean
 */
public abstract class BaseUtility {
    /**
     * Assert that the string is not null
     * @param s
     * @throws Exception
     */
    public void assertNotBlank(final String s) throws Exception {
        if (StringUtils.isBlank(s)) {
            throw new Exception("The argument is null or empty");
        }
    }

    /**
     * Assert that the string is not null
     * @param s
     * @throws Exception
     */
    public void assertNotBlank(final String ... s) throws Exception {
        if (StringUtils.isAnyBlank(s)) {
            throw new Exception("The arguments are null or empty");
        }
    }

    /**
     * Validates that the argument is a file
     * @param f
     * @throws Exception
     */
    public void assertDirectory(final File f) throws Exception {
        if (f == null) {
            throw new Exception("The argument is null");
        }
        if (!f.isDirectory()) {
            throw new Exception("The argument is not a file");
        }
    }


    /**
     * Validates that the argument is a file
     * @param f
     * @throws Exception
     */
    public void assertFile(final File f) throws Exception {
        if (f == null) {
            throw new Exception("The argument is null");
        }
        if (!f.isFile()) {
            throw new Exception("The argument is not a file");
        }
    }

    /**
     * Get the default local directory
     * @return
     * @throws IOException
     */
    public File getTemporaryDirectory()  throws IOException {
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        File ret = new File(FileUtils.getTempDirectory(), uuid);
        if (!ret.mkdirs()) {
            throw new IOException("Cannot create directory at " + ret.getPath());
        }
        return ret;
    }

    /**
     * Gets a directory
     * @param baseDir
     * @return
     * @throws IOException
     */
    public File getDirectory(String baseDir) throws IOException {
        if (baseDir == null) {
            throw new IOException("Could not get directory for null");
        }
        File ret = new File(baseDir);
        if (ret.isFile()) {
            throw new IOException("Could not override a file at " + baseDir);
        }
        if (!ret.isDirectory()) {
            if (!ret.mkdirs()) {
                throw new IOException("Cannot make base directory at " + baseDir);
            }
        }
        return ret;
    }
}
