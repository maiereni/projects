/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.encryption.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.maiereni.utils.BaseTest;
import com.maiereni.utils.RemovableTempFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.security.Security;

/**
 * @author Petre Maierean
 */
public class AsymetricEncryptionProviderImplTest extends BaseTest {


    static {
    /*
        System.setProperty(BaseEncryptionProviderImpl.KEY_STORE, "/com/maiereni/utils/encryption/impl/testing.jks");
        System.setProperty(BaseEncryptionProviderImpl.KEY_PASSWORD, "changeit");
        System.setProperty(BaseEncryptionProviderImpl.KEY_STORE_PASSWORD, "changeit");
        System.setProperty(BaseEncryptionProviderImpl.KEY_ALIAS, "testing");
    */
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    public void testEncryptDecrypt() {
        try (RemovableTempFile tmp = copyResource("/com/maiereni/utils/encryption/impl/sample.txt");
             RemovableTempFile tmpCert = copyResource("/com/maiereni/utils/encryption/impl/testing.p12")) {
            AsymetricEncryptionProviderImpl provider =
                    new AsymetricEncryptionProviderImpl(tmpCert.getPath(), "changeit", "changeit", "test");
            File enc = provider.encryptFile(tmp);
            assertNotNull(enc);
            tmp.add(enc);
            System.out.println("The file was encrypted: " + enc.getPath());
            File dec = provider.decryptFile(enc);
            assertNotNull(dec);
            tmp.add(dec);
            System.out.println("The file was encrypted: " + dec.getPath());
            String txt = FileUtils.readFileToString(dec, "UTF-8");
            String txtOrig = FileUtils.readFileToString(tmp, "UTF-8");
            assertEquals("Expected to be equal", txtOrig, txt);
            assertTrue(true);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("Failed to encrypt and decrypt a file");
        }
    }

    private static final String SAMPLE_STRING = "{\"name\":\"aubrey1\",\"login\":1234586687}";
    @Test
    public void testEncryptDecryptString() {
        try (RemovableTempFile tmpCert = copyResource("/com/maiereni/utils/encryption/impl/testing.p12")) {
            AsymetricEncryptionProviderImpl provider =
                    new AsymetricEncryptionProviderImpl(tmpCert.getPath(), "changeit", "changeit", "test");
            String encrypted = provider.encrypt(SAMPLE_STRING);
            assertNotNull("Expected not null", encrypted);
            String decrypted = provider.decrypt(encrypted);
            assertNotNull("Expected decrypted", decrypted);
            assertEquals("", SAMPLE_STRING, decrypted);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("Failed to decrypt string");
        }
    }

    @Test
    public void testEncryptDecryptStringSpecifiedKeyStore() {
        String keyStore = System.getProperty("javax.net.ssl.keyStore");
        String keyStorePass = System.getProperty("javax.net.ssl.keyStorePassword");
        String keyPass = System.getProperty("javax.net.ssl.keyPassword");
        if (StringUtils.isNoneBlank(keyStore, keyStorePass, keyPass)) {
            try {
                AsymetricEncryptionProviderImpl provider =
                        new AsymetricEncryptionProviderImpl(keyStore, keyStorePass, keyPass, null);
                String encrypted = provider.encrypt(SAMPLE_STRING);
                assertNotNull("Expected not null", encrypted);
                String decrypted = provider.decrypt(encrypted);
                assertNotNull("Expected decrypted", decrypted);
                assertEquals("", SAMPLE_STRING, decrypted);
            }
            catch (Exception e) {
                e.printStackTrace();
                fail("Failed to decrypt string");
            }
        }
    }
}
