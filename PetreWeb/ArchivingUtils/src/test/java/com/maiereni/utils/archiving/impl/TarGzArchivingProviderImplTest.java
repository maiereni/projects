/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.archiving.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.maiereni.utils.BaseTest;
import com.maiereni.utils.RemovableTempFile;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;

/**
 * Unit test class for TarGzArchivingProviderImpl
 *
 * @author Petre Maierean
 */
public class TarGzArchivingProviderImplTest extends BaseTest {

    @Test
    public void testArchiving() {
        try (RemovableTempFile fDir = copyResources(
                "/com/maiereni/utils/encryption/impl/sample.txt",
                "/com/maiereni/utils/encryption/impl/secret.p12",
                "/com/maiereni/utils/encryption/impl/testing.jks",
                "/com/maiereni/utils/encryption/impl/testing.p12")) {
            TarGzArchivingProviderImpl provider = new TarGzArchivingProviderImpl();
            File compressed = provider.compress(new File(fDir, "com"));
            assertNotNull("The compressed file is not null", compressed);
            fDir.add(compressed.getParentFile());
            assertTrue("It is expected that the compressed object to be a file", compressed.isFile());
            assertTrue("It is expected that the compressed object to have the extension tar.gz", compressed.getName().endsWith(".tar.gz"));
            File decompressed = provider.decompress(compressed);
            assertNotNull("The compressed file is not null", decompressed);
            fDir.add(decompressed);
            assertTrue("It is expected that the result is a folder", decompressed.isDirectory());
            assertTrue("Has file", new File(decompressed, "/com/maiereni/utils/encryption/impl/sample.txt").isFile());
            assertTrue("Has file", new File(decompressed, "/com/maiereni/utils/encryption/impl/secret.p12").isFile());
            assertTrue("Has file", new File(decompressed, "/com/maiereni/utils/encryption/impl/testing.jks").isFile());
            assertTrue("Has file", new File(decompressed, "/com/maiereni/utils/encryption/impl/testing.p12").isFile());
        }
        catch(Exception e) {
            e.printStackTrace();
            fail("Failed to archive files");
        }
    }
}
