/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils;

import org.apache.commons.io.FileUtils;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Petre Maierean
 */
public class RemovableTempFile extends File implements Closeable {
    private List<File> others = new ArrayList<>();

    public RemovableTempFile() throws Exception{
        this(File.createTempFile("tmp", "").getPath());
    }
    public RemovableTempFile(String pathname) {
        super(pathname);
    }

    public void add(File other) {
        if (other != null) {
            if (other.exists()) {
                others.add(other);
            }
        }
    }

    @Override
    public void close() throws IOException {
        if (isFile()) {
            if (!delete()) {
                deleteOnExit();
            }
        }
        else if (isDirectory()) {
            FileUtils.deleteQuietly(this);
        }
        for(File other : others) {
            if (other.isFile()) {
                if (!other.delete()) {
                    other.deleteOnExit();
                }
            }
            else {
                FileUtils.deleteQuietly(other);
            }
        }
    }
}
