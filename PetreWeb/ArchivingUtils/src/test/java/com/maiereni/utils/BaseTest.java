/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
public abstract class BaseTest {
    /**
     * Copy a resource to a temporary location
     * @param resource
     * @return
     * @throws Exception
     */
    public RemovableTempFile copyResource(String resource) throws Exception {
        RemovableTempFile ret = null;
        if (StringUtils.isBlank(resource)) {
            throw new Exception("Null or blank resource argument");
        }
        try (InputStream is = getClass().getResourceAsStream(resource)) {
            if (is == null) {
                throw new Exception("Could not find the resource at " + ret.exists());
            }
            byte[] buffer = IOUtils.toByteArray(is);
            ret = new RemovableTempFile();
            FileUtils.writeByteArrayToFile(ret, buffer);
        }
        return ret;
    }

    public RemovableTempFile copyResources(String ... resources) throws Exception {
        RemovableTempFile ret = null;
        File tmpDir = null;
        if (resources.length > 0) {
            tmpDir = new File(FileUtils.getTempDirectory(), UUID.randomUUID().toString().replaceAll("-", ""));
            try {
                for (String resource : resources) {
                    File ftmp = new File(tmpDir, resource);
                    File ftmpDir = ftmp.getParentFile();
                    if (!ftmpDir.isDirectory()) {
                        if (!ftmpDir.mkdirs()) {
                            throw new Exception("Could not create directory at " + ftmpDir.getPath());
                        }
                    }
                    try (InputStream is = getClass().getResourceAsStream(resource)) {
                        if (is == null) {
                            throw new Exception("Could not find the resource at " + ret.exists());
                        }
                        byte[] buffer = IOUtils.toByteArray(is);
                        FileUtils.writeByteArrayToFile(ftmp, buffer);
                    }
                }
            }
            catch(Exception e) {
                FileUtils.deleteQuietly(tmpDir);
                throw e;
            }
        }
        return new RemovableTempFile(tmpDir.getPath());
    }
}
