/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.utils.encryption.impl;

import com.maiereni.utils.BaseTest;
import com.maiereni.utils.RemovableTempFile;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.security.Security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Unit test class for the SecretKeyEncryptionProviderImpl
 * @author Petre Maierean
 */
public class SymetricEncryptionProviderImplTest extends BaseTest {
    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    public void testEncryptDecrypt() {
        try (RemovableTempFile tmp = copyResource("/com/maiereni/utils/encryption/impl/sample.txt");
             RemovableTempFile tmpCert = copyResource("/com/maiereni/utils/encryption/impl/secret.p12")) {
            SymetricEncryptionProviderImpl provider = new SymetricEncryptionProviderImpl(tmpCert.getPath(), "changeit", "testMe", "testme");
            File enc = provider.encryptFile(tmp);
            assertNotNull(enc);
            tmp.add(enc);
            System.out.println("The file was encrypted: " + enc.getPath());
            File dec = provider.decryptFile(enc);
            assertNotNull(dec);
            tmp.add(dec);
            System.out.println("The file was encrypted: " + dec.getPath());
            String txt = FileUtils.readFileToString(dec, "UTF-8");
            String txtOrig = FileUtils.readFileToString(tmp, "UTF-8");
            assertEquals("Expected to be equal", txtOrig, txt);
            assertTrue(true);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("Failed to load key");
        }
    }

    private static final String SAMPLE_STRING = "{\"name\":\"aubrey1\",\"login\":1234586687}";

    @Test
    public void testEncodeDecodeString() {
        try (RemovableTempFile tmpCert = copyResource("/com/maiereni/utils/encryption/impl/secret.p12")) {
            SymetricEncryptionProviderImpl provider = new SymetricEncryptionProviderImpl(tmpCert.getPath(), "changeit", "testMe", "testme");
            String enc = provider.encrypt(SAMPLE_STRING);
            assertNotNull("The encryption is not null", enc);
            String actual = provider.decrypt(enc);
            assertNotNull("The actual decrypted string", actual);
            assertEquals("Must match", SAMPLE_STRING, actual);
        }
        catch(Exception e) {
            e.printStackTrace();
            fail("Failed to decode");
        }
    }
}
