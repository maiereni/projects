var webpack = require('webpack');
var htmlWebpackPlugin = require('html-webpack-plugin');
var extractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');
var path = require('path');

//var distPath = path.resolve(__dirname, './dist');
var distPath = 'C:/Users/pmaie/Workspace/Combined/projects/Design/PetreWebDesign/dist/js';
module.exports = {
    entry: {
        'contactus': './src/index.js'
    },
    resolve: {
        extensions : [ '.ts', '.js' ]
    },
    output: {
        path: distPath,
        filename: 'contactus.js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|gif|jp?g|svg|woff?|ttf|eot|ico)$/,
                loader: 'file-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new htmlWebpackPlugin({
            template: "./src/contactus.html",
            filename: "./contactus.html",
            publicPath: './'
        })
    ]
};
