import React, { Component } from 'react';
import { ReCaptcha } from 'react-recaptcha-v3'
import axios from 'axios';
import publicIp from "public-ip";
import App from "./App";

class MyRecapcha extends Component {

    constructor(props) {
        super(props);

    }

    verifyCallback = (t) => {
        var v = {token: t, clientIP: this.props.clientIp };
        const json = JSON.stringify(v);
        axios.post('/api/contactus/validate', json, {
            crossDomain: true,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(r => {
                console.log('Validation response: ' + r.data.valid);
                this.props.validation(r.data.valid, r.data.reason);
            }, err => {
                this.props.validation(false, err.message);
                console.error(err);
            }
        );
    }

    updateToken = () => {
        this.recaptcha.execute();
    }

    render() {
        if (this.props.siteKey === '' || this.props.clientIp === '') {
            return (
                <div>Wait</div>
            );
        }
        else {
            return (
                <ReCaptcha
                    ref={ref => this.recaptcha = ref}
                    sitekey={ this.props.siteKey }
                    action='submit'
                    verifyCallback={ this.verifyCallback }
                />
            );
        }
    }
}

export default MyRecapcha;
