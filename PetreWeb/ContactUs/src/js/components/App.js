import React, { Component } from 'react';
import { ReCaptcha } from 'react-recaptcha-v3'
import axios from 'axios';
import publicIp from "public-ip";
import MyRecapcha from "./MyRecapcha";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email : '',
      comment : '',
      siteKey : '',
      complete: false,
      notValid: true,
      errorMsg: '',
      clientIp: ''
    };
    publicIp.v4().then( s => this.setIp(s) );
    this.loadConfiguration();
  }

  setIp = (s) => {
    this.setState( {
      clientIp: s
    })
  }

  isCommentBlank = () => {
    return this.state.comment === '';
  }

  isCommentError = () => {
    let r = true;
    if (/^[\w\s.,|]{1,256}$/.test(this.state.comment)) {
      r = false;
    }
    return r;
  }

  isEmailBlank = () => {
    return this.state.email === '';
  }

  isEmailError = () => {
    let r = true;
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)) {
      r = false;
    }
    return r;
  }

  setEmail = (s) => {
    this.setState( {
      email: s
    })
  }

  changeEmail = (e) => {
    let email = e.target.value;
    this.setEmail(email);
  };

  changeComment = (e) => {
    let s = e.target.value;
    this.setState( {
      comment: s
    })
  }

  isDisabled = () => {
    return ( this.isCommentBlank() || this.isCommentError() || this.isEmailBlank() || this.isEmailError() || this.state.notValid );
  }

  setVerify = (valid, msg) => {
    this.setState( {
      notValid: !valid,
      errorMsg: msg
    });
  }

  submitForm = () => {
    if (!this.isDisabled()) {
      let comment = btoa(this.state.comment);
      let json = JSON.stringify({ email: this.state.email, comment: comment, clientIP: this.state.clientIp });
      console.log('Submit ' + json);
      axios.post('/api/contactus/submit', json, {
        crossDomain: true,
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(r => {
            this.setState( {
              notValid: true,
              complete: true,
              errorMsg: r.data.response
            });
          }, err => {
            this.setState( {
              notValid: true
            });
            console.error(err);
          }
      );
    }
    return false;
  }

  loadConfiguration = () => {
    axios.get('/api/contactus/config', {
      crossDomain: true
    }).then(r => {
      this.setState({
        siteKey: r.data.sitekey
      });
    }).catch(reason => {
      console.log('error: ' + reason);
    });
  };

  updateToken = () => {
    this.recaptcha.execute();
  }

  render() {
    let clsEmail = 'form-text';
    let txtEmail = 'A valid email must be provided';
    if (this.isEmailError() && !this.isEmailBlank()) {
      clsEmail = 'form-text text-danger';
      txtEmail = 'The email is not valid';
    }
    let clsComment = 'form-text';
    let txtComment = 'Expected text of maximum 256 characters';
    if (!this.isCommentBlank()) {
      txtComment = 'The comment field contains ' + this.state.comment.length + ' of 256 characters';
      if (this.isCommentError()) {
        clsComment = 'form-text text-danger';
        txtComment = 'The comment field exceeds 256 characters';
      }
    }
    let headingText = 'Please let us know your concerns regarding the usage of the website. We will get back to you via the email provided as soon as we can';
    if (this.state.complete) {
      headingText = 'Your comment has been received. Thank you';
    }
    let d = this.isDisabled();
    return (
        <div className="container pt-4">
          <div className="row">
            <div className="col-12">
              <h1>Contact us</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-12">{headingText}</div>
          </div>
          <div className="row">
            <div className="col-12">
              <form>
                <div className="row g-3 pb-4 pt-4">
                  <div className="col-2">
                    <label htmlFor="inputEmail" className="col-form-label">Email: </label>
                  </div>
                  <div className="col-3">
                    <input type="email" id="inputEmail" className="form-control" aria-describedby="emailHelp" value={ this.state.email } onChange={ this.changeEmail }></input>
                  </div>
                  <div className="col-2">
                    <span id="emailHelp" className={ clsEmail }>
                      { txtEmail }
                    </span>
                  </div>
                </div>
                <div className="row pb-4 pb-4">
                  <div className="col-2">
                    <label htmlFor="inputText" className="form-label">Your comment</label>
                  </div>
                  <div className="col-3">
                    <textarea rows="6" id="inputText" className="form-control" aria-describedby="textHelp" value={ this.state.comment } onChange={ this.changeComment }></textarea>
                  </div>
                  <div className="col-2">
                    <span id="textHelp" className={ clsComment }>
                      { txtComment }
                    </span>
                  </div>
                </div>
                <div className="row pb-4">
                  <div className="col-12">
                    <MyRecapcha siteKey={ this.state.siteKey } clientIp={ this.state.clientIp } validation={ this.setVerify }></MyRecapcha>
                  </div>
                </div>
                <div className="row g-3 align-items-center mb-4">
                  <div className="col-7">
                    <button type="button" className="btn btn-primary" disabled={ d } onClick={ this.submitForm } >Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    );
  }
}

export default App;
