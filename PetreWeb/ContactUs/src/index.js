import "bootstrap";
import "jquery";
import React from "react";
import ReactDom from "react-dom";
import "./css/App.css";
import App from "./js/components/App";

let wrapper = document.getElementById("contact-us");
if (!wrapper) {
  let wrappers = document.getElementsByTagName('contact-us');
  if (wrappers.length > 0) {
    wrapper = wrappers[0];
  }
}
if (wrapper) {
  var st = wrapper.getAttribute('style');
  var config = wrapper.getAttribute('config');
  if (document.getElementsByClassName('.device-mobile-optimized').length > 0) {
    if (wrapper.parentElement.tagName === 'DIV' || wrapper.parentElement.tagName === 'div') {
      let p = wrapper.parentElement;
      p.id = 'wzd1';
    }
  }
  ReactDom.render(
    <React.StrictMode>
      <App config={ config } st={ st }/>
    </React.StrictMode>, wrapper);
}
else {
  console.log("no such element found");
};

