var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var extractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common');
var path = require('path');
const ENV = process.env.NODE_ENV = process.env.ENV = 'development';

module.exports = webpackMerge.merge(commonConfig, {
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        contentBasePublicPath: '/petersweb',
        compress: true,
        bonjour: true,
        allowedHosts: [
            'localhost', 'mylocaldomain.com'
        ],
        headers: {
            'host': 'mylocaldomain.com',
            'x-forwarded-for': '127.0.0.1',
            'x-forwarded-host': 'mylocaldomain.com',


            'x-forwarded-server': 'mylocaldomain.com'
        },
        clientLogLevel: 'debug',
        port: 8081
    }
});
