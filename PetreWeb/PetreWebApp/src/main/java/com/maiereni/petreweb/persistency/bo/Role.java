/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.bo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="ACCOUNTROLE")
@NamedQueries({
        @NamedQuery(name = Role.FIND_ROLE_BY_NAME, query = "SELECT r FROM Role r WHERE r.name = :" + Role.NAME),
        @NamedQuery(name = Role.FIND_ROLES, query = "SELECT r FROM Role r ORDER BY r.name asc"),
        @NamedQuery(name = Role.IS_ROLE, query = "SELECT count( r ) FROM Role r WHERE r.name = :" + Role.NAME)
})
public class Role implements Serializable {
    public static final String NAME = "role_name";
    public static final String FIND_ROLE_BY_NAME = "Role.Find_By_Name";
    public static final String FIND_ROLES = "Role.Find_All";
    public static final String IS_ROLE = "Role.Is_Role";
    public static final String DEFAULT_USER = "USER";
    public static final String DEFAULT_USER_DESCRIPTION = "A simple user";
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(36)")
    private String id;
    @Column(columnDefinition = "VARCHAR(32)")
    private String name;
    @Column(columnDefinition = "VARCHAR(256)")
    private String description;
    @Column
    @Temporal(TemporalType.DATE)
    private Calendar creationDate;
    @OneToMany(mappedBy="role", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserRole> userRole;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UserRole> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }
}
