/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.bo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="USERPROPERTY")
@NamedQueries({
        @NamedQuery(name=UserProperty.FIND_USER_PROPERTIES, query = UserProperty.STMT_FIND_USER_PROPERTIES)
})
public class UserProperty implements Serializable {
    public static final String FIND_USER_PROPERTIES = "UserProperties.Find_by_id";
    public static final String ATTR_NAME = "userId";
    public static final String STMT_FIND_USER_PROPERTIES = "SELECT up From UserProperty up WHERE up.user.id = :" + ATTR_NAME;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(36)")
    private String id;
    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;
    @Column(columnDefinition = "VARCHAR(32)", nullable = false)
    private String name;
    @Column(columnDefinition = "VARCHAR(256)", nullable = false)
    private String value;
    @Column(columnDefinition = "VARCHAR(16)", nullable = false)
    private String type;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }
}
