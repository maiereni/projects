/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor;

import com.maiereni.petreweb.filter.oauth2.bo.UserIdentity;
import com.maiereni.petreweb.persistency.bo.User;

import java.util.List;

/**
 * @author Petre Maierean
 */
public interface UserResolver {
    /**
     * Resolve a user
     * @param userIdentity the identity of the user
     * @return
     * @throws Exception
     */
    User getUser(UserIdentity userIdentity) throws Exception;

    /**
     * Validates that the user is known
     * @param userIdentity the identity of the user
     * @return
     * @throws Exception
     */
    boolean isUser(UserIdentity userIdentity) throws Exception;

    /**
     * Create a user
     * @param userIdentity the identity of the user
     * @param source the creation source
     * @return
     * @throws Exception
     */
    User createUser(UserIdentity userIdentity, String source) throws Exception;

    /**
     * Get a user by name
     * @param userId
     * @return
     * @throws Exception
     */
    User getUserById(String userId) throws Exception;

    /**
     * Get roles for the user identity
     * @param userIdentity
     * @return
     * @throws Exception
     */
    List<String> getRoles(UserIdentity userIdentity) throws Exception;

    /**
     * Add user role
     * @param userIdentity
     * @param roleName
     * @throws Exception
     */
    void addRole(UserIdentity userIdentity, String roleName) throws Exception;
}
