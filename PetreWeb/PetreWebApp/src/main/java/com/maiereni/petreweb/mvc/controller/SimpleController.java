/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.mvc.controller;

import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.filter.csrf.LocalCsrfTokenRepository;
import com.maiereni.petreweb.filter.oauth2.AuthenticationEndpoints;
import com.maiereni.petreweb.filter.oauth2.bo.LoginEndpoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Petre Maierean
 */
@Controller
public class SimpleController {
    private static final Logger logger = LogManager.getLogger(SimpleController.class);
    @Autowired
    Settings settings;
    @Autowired
    AuthenticationEndpoints authenticationEndpoints;
    @Autowired
    LocalCsrfTokenRepository csrfTokenRepository;

    @GetMapping(path="/index.html")
    public String getIndex(HttpServletRequest request, HttpServletResponse response, Model model) {
        addLoginEndpoints(request, response, model);
        return "index";
    }

    @GetMapping(path="/privacy.html")
    public String getPrivacy(HttpServletRequest request, HttpServletResponse response, Model model) {
        addLoginEndpoints(request, response, model);
        return "privacy";
    }

    @GetMapping(path="/contact.html")
    public String getContact(HttpServletRequest request, HttpServletResponse response, Model model) {
        addLoginEndpoints(request, response, model);
        model.addAttribute("siteKey", settings.getGoogleSiteKey());
        return "contact";
    }

    @GetMapping(path="/error.html")
    public String getError(HttpServletRequest request, HttpServletResponse response, Model model) {
        addLoginEndpoints(request, response, model);
        logger.error("Present the error page");
        return "error";
    }

    @GetMapping(path="/error")
    public String getError1(HttpServletRequest request, HttpServletResponse response, Model model) {
        addLoginEndpoints(request, response, model);
        logger.error("Present the error page");
        return "error";
    }

    private void addLoginEndpoints(HttpServletRequest request, HttpServletResponse response, Model model) {
        CsrfToken csrfToken = csrfTokenRepository.loadToken(request);
        if (csrfToken == null) {
            csrfToken = csrfTokenRepository.generateToken(request);
            csrfTokenRepository.saveToken(csrfToken, request, response);
        }
        List<LoginEndpoint> loginEndpoints = authenticationEndpoints.getEndpoints(csrfToken.getToken());
        model.addAttribute("loginEndpoints", loginEndpoints);
        logger.debug("Added the login endpoints");
    }
}
