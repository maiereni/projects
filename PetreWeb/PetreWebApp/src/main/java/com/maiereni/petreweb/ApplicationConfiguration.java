/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.context.ApplicationSettingsReader;
import com.maiereni.utils.archiving.ArchivingProvider;
import com.maiereni.utils.archiving.impl.TarGzArchivingProviderImpl;
import com.maiereni.utils.encryption.EncryptionProvider;
import com.maiereni.utils.encryption.impl.AsymetricEncryptionProviderImpl;
import com.maiereni.utils.encryption.impl.SymetricEncryptionProviderImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.security.CodeSource;
import java.security.Security;
import java.util.UUID;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Petre Maierean
 */
@Configuration
public class ApplicationConfiguration {
    private static final Logger logger = LogManager.getLogger(ApplicationConfiguration.class);

    /**
     * Get the application properties
     * @return
     * @throws Exception
     */
    @Bean
    public Settings getSettings() throws Exception {
        ApplicationSettingsReader applicationSettingsReader = new ApplicationSettingsReader();
        Settings ret = applicationSettingsReader.readProperties();
        String folderName = "tomcat." + UUID.randomUUID().toString().replaceAll("-", "");
        ret.setFoldersToRemove(new Vector<>());
        File fDir = new File(System.getProperty("java.io.tmpdir"), folderName);
        File fDocs = new File(fDir,"doc");
        fDocs = new File(System.getProperty("server.tomcat.docDir",fDocs.getPath()));
        fDocs = copyViewResources(fDocs);
        ret.setDocRoot(fDocs.getPath());
        File fBase = new File(System.getProperty("server.tomcat.baseDir",new File(fDir,"base").getPath()));
        ret.setBaseDir(makeDirs(fBase));
        ret.getFoldersToRemove().add(fDir.getPath());
        logger.debug("The application properties have been read from " + applicationSettingsReader.getfProps().getPath());
        return ret;
    }

    @Bean
    public ExitCodeGenerator getExitCodeGenerator(final Settings settings) {
        return new ExitCodeGenerator() {
            @Override
            public int getExitCode() {
                logger.debug("Clean up");
                if (settings.getFoldersToRemove() != null) {
                    for(String rm: settings.getFoldersToRemove()) {
                        File dir = new File(rm);
                        if (dir.exists()) {
                            try {
                                FileUtils.cleanDirectory(dir);
                                FileUtils.deleteDirectory(dir);
                                logger.debug("Directory removed at " + rm);
                            }
                            catch(Exception e) {
                                logger.error("Could not clean up directory " + rm);
                            }
                        }
                    }
                }
                return 0;
            }
        };
    }

    /**
     * Constructs am Encyption Provider. If the keystore contains a secret key than this method constructs a provider for
     * encryption with a symetric key
     *
     * @param settings
     * @return
     */
    @Bean
    public EncryptionProvider getEncryptionProvider(final Settings settings) throws Exception {
        EncryptionProvider ret = null;
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        try {
            ret = new SymetricEncryptionProviderImpl(
                    settings.getKeystore(),
                    settings.getKeystorePassword(),
                    settings.getKeyPassword(),
                    settings.getAlias());
            logger.debug("Use a symmetrical key encryption");
        }
        catch (Exception e) {
            ret = new AsymetricEncryptionProviderImpl(
                    settings.getKeystore(),
                    settings.getKeystorePassword(),
                    settings.getKeyPassword(),
                    settings.getAlias());
            logger.debug("Use an asymmetrical key encryption");
        }
        return ret;
    }

    /**
     * Constructs an archiving utility class
     * @param settings
     * @return
     * @throws Exception
     */
    @Bean
    public ArchivingProvider getArchivingProvider(final Settings settings) throws Exception {
        return new TarGzArchivingProviderImpl();
    }

    /**
     * Constructs an Object mapper for JSON to Object converstions
     * @return
     */
    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper ret = new ObjectMapper();
        return ret;
    }

    private File copyViewResources(File fDocs) throws Exception {
        File ret = null;
        URI uri = ApplicationConfiguration.class.getResource("/static").toURI();
        if (uri.getScheme().equals("file")) {
            ret = new File(uri.toString().substring(6));
        }
        else if (uri.getScheme().equals("jar")) {
            int ix = uri.toString().indexOf("!");
            String path = uri.toString().substring(ix + 2).replace("!", "");
            logger.debug("Copy all content from " + path);
            copyViewResources(fDocs, path);
            ret = new File(fDocs, path);
        }
        return ret;
    }

    private void copyViewResources(File fDocs,String path) throws Exception {
        CodeSource src = ApplicationConfiguration.class.getProtectionDomain().getCodeSource();
        if (src != null) {
            try (InputStream is = src.getLocation().openStream();
                ZipInputStream zip = new ZipInputStream(is);){
                while(true) {
                    ZipEntry e = zip.getNextEntry();
                    if (e == null)
                        break;
                    String name = e.getName();
                    if (name.startsWith(path)) {
                        File f = new File(fDocs, name);
                        if (e.isDirectory()) {
                            if (!f.isDirectory())
                                if (!f.mkdirs()) {
                                    throw new Exception("Cannot make directory");
                                }
                        }
                        else {
                            byte[] buffer = readCurrentEntry(zip);
                            FileUtils.writeByteArrayToFile(f, buffer);
                        }
                    }
                }
            }
        }
        else {
            logger.error("No source");
        }
    }

    private byte[] readCurrentEntry(ZipInputStream zip) throws Exception {
        return IOUtils.toByteArray(zip);
    }

    private String makeDirs(File fDir) throws Exception {
        if (!fDir.isDirectory())
            if (!fDir.mkdirs())
                throw new Exception("Cannot make directory at " + fDir.getPath());
        return fDir.getPath();
    }
}
