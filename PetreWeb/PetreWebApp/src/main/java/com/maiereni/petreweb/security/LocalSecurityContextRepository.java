/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.security;

import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.persistency.bo.User;
import com.maiereni.petreweb.processor.CookieUserResolver;
import com.maiereni.petreweb.processor.UserResolver;
import com.maiereni.petreweb.processor.bo.UserCookie;
import com.maiereni.petreweb.security.bo.UserProfile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Component;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Petre Maierean
 */
@Component
public class LocalSecurityContextRepository extends HttpSessionSecurityContextRepository implements SecurityContextRepository, ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LogManager.getLogger(LocalSecurityContextRepository.class);

    private Cache<String, UserProfile> cache;

    @Autowired
    CookieUserResolver cookieUserResolver;
    @Autowired
    private Settings settings;
    @Autowired
    private UserResolver userResolver;

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder httpRequestResponseHolder) {
        return getSecurityContext(httpRequestResponseHolder.getRequest());
    }

    @Override
    public void saveContext(SecurityContext securityContext, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            if (authentication.isAuthenticated()) {
                if (authentication instanceof UserProfile) {
                    UserProfile up = (UserProfile) authentication;
                    cache.put(up.getId(), up);
                }
            }
        }
    }

    @Override
    public boolean containsContext(HttpServletRequest httpServletRequest) {
        return getSecurityContext(httpServletRequest).getAuthentication().isAuthenticated();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        CacheManager manager = event.getApplicationContext().getBean(CacheManager.class);
        cache = manager.getCache("userCache");
        if (cache == null) {
            logger.error("The cache 'userCache' could not be found");
        }
    }

    private SecurityContext getSecurityContext(HttpServletRequest request) {
        SecurityContext ret = null;
        UserProfile userProfile = getUserProfile(request);
        if (userProfile.getName().equals(UserProfile.ANONYMOUS)) {
            logger.debug("An anonymous call");
            ret = new SecurityContextImpl(getAnonymous()) ;
        }
        else {
            ret = new SecurityContextImpl(userProfile);
        }
        return ret;
    }

    private UserProfile getUserProfile(HttpServletRequest request) {
        UserProfile ret = new UserProfile();
        ret.setName(UserProfile.ANONYMOUS);
        ret.getAuthorities().add(new SimpleGrantedAuthority(UserProfile.ANONYMOUS));
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for(Cookie cookie: cookies) {
                try {
                    UserCookie uc = cookieUserResolver.getUser(cookie);
                    if (uc != null) {
                        if (cache.containsKey(uc.getUserId())) {
                            ret = cache.get(uc.getUserId());
                            break;
                        } else {
                            User user = userResolver.getUserById(uc.getUserId());
                            if (user != null) {
                                logger.debug("The user has been found for " + uc.getUserId());
                                ret = new UserProfile();
                                ret.setId(user.getUserId());
                                ret.setName(user.getName());
                                List<GrantedAuthority> authorities = new ArrayList<>();
                                user.getUserRoles().forEach(ur -> {
                                    authorities.add(new SimpleGrantedAuthority(ur.getRole().getName()));
                                });
                                ret.setAuthorities(authorities);
                                ret.setAuthenticated(true);
                                cache.put(uc.getUserId(), ret);
                            }
                            else {
                                logger.error("No user has been found for " + uc.getUserId());
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("Failed to get user identity for the cookie", e);
                }
            }
        }
       return ret;
    }

    private AnonymousAuthenticationToken getAnonymous() {
        return new AnonymousAuthenticationToken(UserProfile.ANONYMOUS,UserProfile.ANONYMOUS, AuthorityUtils.createAuthorityList("ROLE_CUSTOM"));
    }
}
