/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.filter.csrf;

import com.maiereni.petreweb.context.Settings;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Component
public class LocalCsrfTokenRepository implements CsrfTokenRepository, ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LogManager.getLogger(LocalCsrfTokenRepository.class);
    private static final String TEMPLATE = "{\"header_name\":\"%s\", \"parameter_name\":\"%s\", \"value\":\"%s\"}";
    private static final String LAST_SAVED_TOKEN = "LAST_SAVED_TOKEN";
    public static final String NONCE_TOKEN = "XNONCE";
    public static final String GOOGLE_STATE = "state";
    public static final String CSRF_TOKEN_NONCE = "nonce";
    private CookieCsrfTokenRepository nested = new CookieCsrfTokenRepository();
    private String parameterName, headerName;
    @Autowired
    Settings settings;
    private Cache<String, String> csrfTokenCache;

    public LocalCsrfTokenRepository() {
        nested = new CookieCsrfTokenRepository();
        CsrfToken def = generateToken(null);
        this.parameterName = def.getParameterName();
        this.headerName = def.getParameterName();
    }

    @Override
    public CsrfToken generateToken(HttpServletRequest request) {
        CsrfToken ret = nested.generateToken(request);
        logger.debug("Csrf was generated: " + toString(ret));
        return ret;
    }

    public CsrfToken getSavedToken(HttpServletRequest request) {
        return (CsrfToken) request.getAttribute(LAST_SAVED_TOKEN);
    }

    /**
     * Add the nonce
     * @param token
     * @param request
     * @param response
     * @return
     */
    public String addNonce(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
        String ret = UUID.randomUUID().toString().replace("-", "");
        if (!(token == null || request == null || response == null)) {
            csrfTokenCache.put(ret, token.getToken());
            addRequestUriForNonce(ret, request);
            Cookie cookie = new Cookie(NONCE_TOKEN, ret);
            cookie.setSecure(request.isSecure());
            cookie.setPath("/");
            cookie.setMaxAge(180);
            cookie.setHttpOnly(true);
            cookie.setDomain(settings.getDomain());
            response.addCookie(cookie);
            logger.debug("The nonce cookie has been added" + ret);
        }
        else {
            removeNonce(request, response);
        }
        return ret;
    }

    /**
     * Add the nonce cookie
     * @param token the CSRF token
     * @param state the state that is passed in the call to initiate the OAuth2 flow
     * @param request the request
     * @param response the response
     */
    public void addNonce(String token, String state, HttpServletRequest request, HttpServletResponse response) {
        if (!(token == null || state == null || request == null || response == null)) {
            csrfTokenCache.put(token, state);
            addRequestUriForNonce(token, request);
            Cookie cookie = new Cookie(NONCE_TOKEN, token);
            cookie.setSecure(request.isSecure());
            cookie.setPath("/");
            cookie.setMaxAge(180);
            cookie.setHttpOnly(true);
            cookie.setDomain(settings.getDomain());
            response.addCookie(cookie);
            logger.debug("The nonce cookie has been added " + state);
        }
    }

    /**
     * Get the request uri that triggered the adding of the nonce token
     * @param request
     * @return
     */
    public String getRequestUriForNonce(HttpServletRequest request) {
        String ret = null;
        Cookie cookie = WebUtils.getCookie(request, NONCE_TOKEN);
        if (cookie != null) {
            String key = getNonceKey(cookie.getValue());
            ret = csrfTokenCache.get(key);
        }
        return ret;
    }

    /**
     * Remove nonce cookie
     * @param request
     * @param response
     * @return
     */
    public String removeNonce(HttpServletRequest request, HttpServletResponse response) {
        String ret = null;
        Cookie cookie = WebUtils.getCookie(request, NONCE_TOKEN);
        if (!(cookie == null || request == null || response == null)) {
            ret = cookie.getValue();
            csrfTokenCache.remove(ret);
            cookie = new Cookie(NONCE_TOKEN, "");
            cookie.setSecure(request.isSecure());
            cookie.setPath("/");
            cookie.setMaxAge(0);
            cookie.setHttpOnly(true);
            cookie.setDomain(settings.getDomain());
            response.addCookie(cookie);
            logger.debug("The nonce cookie has been removed");
        }
        return ret;
    }

    @Override
    public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Csrf was saved: " + toString(token));
        nested.saveToken(token, request, response);
        request.setAttribute(LAST_SAVED_TOKEN, token);
    }

    @Override
    public CsrfToken loadToken(HttpServletRequest request) {
        CsrfToken ret = null;
        if (request != null) {
            ret = nested.loadToken(request);
            Cookie cookie = WebUtils.getCookie(request, NONCE_TOKEN);
            if (cookie != null) {
                String nonce = cookie.getValue();
                String token = csrfTokenCache.get(nonce);
                if (StringUtils.isNotBlank(token)) {
                    ret = new DefaultCsrfToken(GOOGLE_STATE, GOOGLE_STATE, token);
                    logger.debug("The expected state is " + token);
                }
                else {
                    logger.error("No state value in the cache for nonce: " + nonce);
                }
            }
            else {
                logger.error("The nonce cookie was not found in the request");
            }
            logger.debug("Csrf was loaded as: " + toString(ret));
        }
        return ret;
    }

    public void setParameterName(String parameterName) {
        nested.setParameterName(parameterName);
        this.parameterName = parameterName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
        nested.setHeaderName(headerName);
    }

    public void setCookieName(String cookieName) {
        nested.setCookieName(cookieName);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        CacheManager cacheManager = event.getApplicationContext().getBean(CacheManager.class);
        csrfTokenCache = cacheManager.getCache("csrfCache", String.class, String.class);
    }

    private String toString(CsrfToken token) {
        return token == null ? "null" : String.format(TEMPLATE, token.getHeaderName(), token.getParameterName(), token.getToken());
    }
    private void addRequestUriForNonce(String nonce, HttpServletRequest request) {
        String url = request.getRequestURI();
        String key = getNonceKey(nonce);
        csrfTokenCache.put(key, url);
    }

    private String getNonceKey(String nonce) {
        return  "uri-" + nonce;
    }
}
