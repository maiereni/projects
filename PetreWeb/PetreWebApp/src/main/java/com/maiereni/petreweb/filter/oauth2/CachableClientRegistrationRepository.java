/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.filter.oauth2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.petreweb.filter.oauth2.bo.DiscoveryDocument;
import com.maiereni.petreweb.filter.oauth2.bo.OAuth2Registration;
import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.filter.oauth2.bo.LoginEndpoint;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;

import javax.cache.Cache;
import java.util.*;
import java.util.function.Consumer;

/**
 * An implementation of the ClientRegistrationRepository that retrieves information about the OAuth2 Provider at runtime
 * and caches it locally
 *
 * @author Petre Maierean
 */
public class CachableClientRegistrationRepository implements ClientRegistrationRepository,
        Iterable<ClientRegistration>, AuthenticationEndpoints {
    private static final Logger logger = LogManager.getLogger(CachableClientRegistrationRepository.class);
    Cache<String, DiscoveryDocument> discoveryDocuments;
    ObjectMapper objectMapper;
    Settings settings;

    private Map<String, OAuth2Registration> oAuth2RegistrationMap;

    public CachableClientRegistrationRepository(Cache<String, DiscoveryDocument> discoveryDocuments,
                                         ObjectMapper objectMapper,
                                         Map<String, OAuth2Registration> oAuth2RegistrationMap) {
        this.discoveryDocuments = discoveryDocuments;
        this.objectMapper = objectMapper;
        this.settings = settings;
        this.oAuth2RegistrationMap = oAuth2RegistrationMap;
    }

    /**
     * Get the ClientRegistration for a give registrationId
     *
     * @param registrationId
     * @return
     */
    @Override
    public ClientRegistration findByRegistrationId(String registrationId) {
        ClientRegistration ret = null;
        if (StringUtils.isNotBlank(registrationId)) {
            if (oAuth2RegistrationMap.containsKey(registrationId)) {
                logger.debug("Find registration for " + registrationId);
                OAuth2Registration oAuth2Registration = oAuth2RegistrationMap.get(registrationId);
                if (StringUtils.isNotBlank(oAuth2Registration.getDiscoveryUrl())) {
                    try {
                        DiscoveryDocument discoveryDocument = getDiscoveryDocument(registrationId, oAuth2Registration.getDiscoveryUrl());
                        ret = getClientRegistration(oAuth2Registration, discoveryDocument);
                    } catch (Exception e) {
                        logger.error("Could not read a ClientRegistration for " + registrationId, e);
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Get all the available client registration objects
     * @return
     */
    @Override
    public Iterator<ClientRegistration> iterator() {
        List<ClientRegistration> clientRegistrationList = new ArrayList<>();
        for (String providerName : oAuth2RegistrationMap.keySet()) {
            ClientRegistration clientRegistration = findByRegistrationId(providerName);
            if (clientRegistration != null) {
                clientRegistrationList.add(clientRegistration);
            }
        }
        return clientRegistrationList.iterator();
    }

    /**
     * Lists all the endpoints available to the user
     * @param csrf the csrf token to be embedded
     * @return
     */
    @Override
    public List<LoginEndpoint> getEndpoints(String csrf) {
        List<LoginEndpoint> ret = new ArrayList<>();
        for (String providerName : oAuth2RegistrationMap.keySet()) {
            ClientRegistration clientRegistration = findByRegistrationId(providerName);
            LoginEndpoint endpoint = new LoginEndpoint();
            endpoint.setTitle(clientRegistration.getRegistrationId());
            endpoint.setLink("/oauth2/authorization/" + clientRegistration.getRegistrationId() + "?_csrf=" + csrf);
            ret.add(endpoint);
        }
        return ret;
    }

    @Override
    public void forEach(Consumer<? super ClientRegistration> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<ClientRegistration> spliterator() {
        return Iterable.super.spliterator();
    }

    private DiscoveryDocument getDiscoveryDocument(String keyName, String url) throws Exception {
        DiscoveryDocument ret = null;
        if (StringUtils.isNoneBlank(keyName, url)) {
            ret = discoveryDocuments.get(keyName);
            if (ret == null) {
                HttpGet httpGet = new HttpGet(url);
                HttpClientContext context = HttpClientContext.create();
                try (CloseableHttpClient httpClient = HttpClientBuilder.create().build();
                     CloseableHttpResponse response = httpClient.execute(httpGet, context)) {
                    if (response.getStatusLine().getStatusCode() != 200) {
                        String error = String.format("%s: %s", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
                        throw new Exception(error);
                    }
                    String s = EntityUtils.toString(response.getEntity(), "UTF-8");
                    ret = objectMapper.readValue(s, DiscoveryDocument.class);
                    discoveryDocuments.put(keyName, ret);
                    logger.debug("The discovery document has been retrieved successfully from " + url);
                }
            }
        }
        return ret;
    }

    private ClientRegistration getClientRegistration(OAuth2Registration oAuth2Registration, DiscoveryDocument discoveryDocument) {
        return ClientRegistration.withRegistrationId(oAuth2Registration.getProviderName())
                .clientId(oAuth2Registration.getClientId())
                .clientSecret(oAuth2Registration.getClientSecret())
                .clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri(oAuth2Registration.getRedirectUri())
                .scope(discoveryDocument.getScopesSupported())
                .authorizationUri(discoveryDocument.getAuthorizationEndpoint())
                .tokenUri(discoveryDocument.getTokenEndpoint())
                .userInfoUri(discoveryDocument.getUserinfoEndpoint())
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .jwkSetUri(discoveryDocument.getJwksUri())
                .clientName(oAuth2Registration.getProviderName())
                .build();
    }
}
