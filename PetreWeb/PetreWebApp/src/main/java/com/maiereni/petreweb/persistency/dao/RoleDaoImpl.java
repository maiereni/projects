/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.persistency.RoleDao;
import com.maiereni.petreweb.persistency.bo.Role;
import jdk.jshell.Snippet;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.List;

/**
 * Describes the permissions for a given user
 * @author Petre Maierean
 */
@Component
public class RoleDaoImpl extends BaseDaoImpl implements RoleDao {
    private static Logger logger = LogManager.getLogger(RoleDaoImpl.class);

    /**
     * Find a role by name
     *
     * @param name
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public Role findRole(String name) throws Exception {
        Role ret = null;
        if (StringUtils.isNotBlank(name)) {
            logger.debug("Find role by name: " + name);
            ret = doFindRole(name);
        }
        return ret;
    }

    /**
     * Find all the roles
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public List<Role> findRoles() throws Exception {
        logger.debug("Find all roles");
        TypedQuery<Role> query = entityManager.createNamedQuery(Role.FIND_ROLES, Role.class);
        return query.getResultList();
    }

    /**
     * Validates that the role exist
     * @param name
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public boolean isRole(String name) {
        boolean ret = false;
        if (StringUtils.isNotBlank(name)) {
            logger.debug("Verifies if there is a role by name: " + name);
            Query query = entityManager.createNamedQuery(Role.IS_ROLE);
            query.setParameter(Role.NAME, name);
            Object o = query.getSingleResult();
            ret = ((Long) o).intValue() > 0;
            if (ret) {
                logger.debug("The role does exist");
            }
        }
        return ret;
    }

    /**
     * Saves the role
     * @param role
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void save(Role role) throws Exception {
        if (role != null) {
            if (isRole(role.getName())) {
                entityManager.merge(role);
            }
            else {
                //role.setId(generateId());
                role.setCreationDate(Calendar.getInstance());
                entityManager.persist(role);
            }
        }
    }
}
