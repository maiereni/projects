/**
 *
 */
package com.maiereni.petreweb.filter.csrf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

/**
 *
 * @author Petre Maierean
 */
public class CustomCsrfRequestMatcher implements RequestMatcher {
    private static final Logger logger = LogManager.getLogger(CustomCsrfRequestMatcher.class);
    private static final String PUBLIC_STR = "/api/api/contactus";
    private static final String WIZARD_STR = "/api/api/buildComp";
    private static final String PUBLIC_PAGES = "(\\x2fmvc\\x2f)(wizard\\x2f)?(\\w+\\x2ehtml)?";
    private static final String LOGIN_LIST = "/api/login/list";
    private static final String GET_METHOD = "GET";
    @Override
    public boolean matches(HttpServletRequest request) {
        boolean ret = true;
        String sUrl = request.getRequestURI();
        if (sUrl.startsWith(PUBLIC_STR) || sUrl.startsWith(WIZARD_STR)) {
            ret = false;
        }
        else if (request.getMethod().equalsIgnoreCase(GET_METHOD) &&
                sUrl.matches(PUBLIC_PAGES)) {
            ret = false;
        }
        else if (sUrl.indexOf(LOGIN_LIST)>=0) {
            ret = false;
        }
        return ret;
    }
}
