/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.processor;

import com.maiereni.petreweb.StartupProcessor;
import com.maiereni.petreweb.persistency.DerbyProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Imports derby files
 * @author Petre Maierean
 */
@Component
public class DataImportingProcessor implements StartupProcessor {
    private static final Logger logger = LogManager.getLogger(DataImportingProcessor.class);
    @Autowired
    private DerbyProperties derbyProperties;

    @Override
    public void startup() {
        logger.debug("Create Derby database");
    }
}
