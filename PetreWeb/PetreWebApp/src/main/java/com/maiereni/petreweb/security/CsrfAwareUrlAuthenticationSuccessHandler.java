/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.security;

import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.filter.csrf.LocalCsrfTokenRepository;
import com.maiereni.petreweb.filter.oauth2.UserIdentityConverter;
import com.maiereni.petreweb.filter.oauth2.bo.UserIdentity;
import com.maiereni.petreweb.persistency.bo.UserRole;
import com.maiereni.petreweb.processor.CookieUserResolver;
import com.maiereni.petreweb.processor.UserResolver;
import com.maiereni.petreweb.processor.bo.UserCookie;
import com.maiereni.petreweb.security.bo.UserProfile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Petre Maierean
 */
@Component
public class CsrfAwareUrlAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private static final Logger logger = LogManager.getLogger(CsrfAwareUrlAuthenticationSuccessHandler.class);
    private static String HTTPS = "https://";

    @Autowired
    LocalCsrfTokenRepository localCsrfTokenRepository;
    @Autowired
    CookieUserResolver cookieUserResolver;
    @Autowired
    LocalSecurityContextRepository securityContextRepository;
    @Autowired
    private Settings settings;
    @Autowired
    private UserResolver userResolver;
    @Autowired
    private List<UserIdentityConverter> userIdentityConverters;

    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        String path = settings.getErrorPage();
        UserProfile userProfile = saveSecurityContext(request, response);
        if (userProfile != null) {
            setAuthenticationCookie(request, response, userProfile);
            localCsrfTokenRepository.removeNonce(request, response);
            path = settings.getDashboardPage();
        }
        String ret = calculateRedirect(request, path);
        logger.debug("Redirect to " + ret);
        return ret;
    }

    private String calculateRedirect(HttpServletRequest request, String path) {
        CsrfToken csrfToken = localCsrfTokenRepository.getSavedToken(request);
        StringBuffer sb = new StringBuffer();
        sb.append(HTTPS)
          .append(settings.getDomain())
          .append(path)
          .append("?")
          .append(csrfToken.getParameterName())
          .append("=")
          .append(csrfToken.getToken());
        return sb.toString();
    }

    private UserProfile saveSecurityContext(HttpServletRequest request, HttpServletResponse response) {
        UserProfile ret = null;
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            Authentication authentication = context.getAuthentication();
            if (authentication.isAuthenticated()) {
                UserProfile userProfile = new UserProfile();
                userProfile.setAuthenticated(true);
                User user = findUser(authentication);
                userProfile.setId(user.getUsername());
                userProfile.setUser(user);
                securityContextRepository.saveContext(context, request, response);
                ret = userProfile;
                logger.debug("Save the security context");
            }
        }
        return ret;
    }

    private User findUser(Authentication authentication) {
        String name = authentication.getName();
        User ret = new User(name,"",authentication.getAuthorities());
        UserIdentity userIdentity = getUserIdentity(authentication);
        if (userIdentity == null) {
            logger.error("Cannot load the user identity for the authentication object");
        }
        else {
            try {
                ret = getOrCreateUser(userIdentity);
            } catch (Exception e) {
                logger.error("Failed to resolve the user", e);
            }
        }
        return ret;
    }

    private User getOrCreateUser(UserIdentity userIdentity) throws Exception {
        com.maiereni.petreweb.persistency.bo.User user = userResolver.getUser(userIdentity);
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (user == null) {
            user = userResolver.createUser(userIdentity, userIdentity.getSource());
            logger.debug("User has been created");
        }
        else {
            for (UserRole ur : user.getUserRoles()) {
                SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(ur.getRole().getName());
                authorities.add(grantedAuthority);
            }
        }
        logger.debug("The user has been found as " + user.getName());
        return new User(user.getUserId().trim(), "", authorities);
    }

    private UserIdentity getUserIdentity(Authentication authentication) {
        UserIdentity ret = null;
        DefaultOidcUser oidcUser = (DefaultOidcUser) authentication.getPrincipal();
        Map<String, Object> attributes = oidcUser.getAttributes();
        Object sel = attributes.get("iss");
        if (sel != null) {
            String iss = sel.toString();
            for (UserIdentityConverter userIdentityConverter : userIdentityConverters) {
                if (userIdentityConverter.getIssName().equals(iss)) {
                    ret = userIdentityConverter.convert(oidcUser);
                    ret.setSource(iss);
                    break;
                }
            }
        }
        return ret;
    }

    private void setAuthenticationCookie(HttpServletRequest request, HttpServletResponse response, UserProfile userProfile) {
        UserCookie userCookie = new UserCookie();
        userCookie.setUserId(userProfile.getId());
        userCookie.setCreationDate(new Date());
        userCookie.setSecure(request.isSecure());
        try {
            Cookie cookie = cookieUserResolver.getSessionCookie(userCookie);
            response.addCookie(cookie);
        } catch (Exception e) {
            logger.error("Failed to add user cookie", e);
        }
    }
}
