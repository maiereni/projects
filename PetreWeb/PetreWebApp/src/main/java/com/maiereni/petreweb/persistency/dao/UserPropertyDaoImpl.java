/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.persistency.UserPropertyDao;
import com.maiereni.petreweb.persistency.bo.User;
import com.maiereni.petreweb.persistency.bo.UserProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Petre Maierean
 */
@Component
public class UserPropertyDaoImpl extends BaseDaoImpl implements UserPropertyDao {
    private static Logger logger = LogManager.getLogger(UserPropertyDaoImpl.class);

    /**
     * Lists all the properties of a user
     * @param userId
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public List<UserProperty> getUserProperties(String userId) throws Exception {
        User user = doFindUser(userId);
        List<UserProperty> ret = new ArrayList<>();
        ret.addAll(user.getUserProperties());
        return ret;
    }

    /**
     * Add user property record
     * @param userProperty
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void addUserProperty(UserProperty userProperty) throws Exception  {
        if (userProperty != null) {
            if (StringUtils.isBlank(userProperty.getId())) {
                logger.debug("Persist user property");
                //userProperty.setId(generateId());
                userProperty.setCreationDate(Calendar.getInstance());
                entityManager.persist(userProperty);
            }
            else {
                logger.debug("Update user property");
                entityManager.merge(userProperty);
            }
        }
    }

    /**
     * Delete the user property record
     * @param userProperty
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deleteUserProperty(UserProperty userProperty) throws Exception {
        if (userProperty != null) {
            if (userProperty.getId() == null) {
                entityManager.remove(userProperty);
            }
        }
    }
}
