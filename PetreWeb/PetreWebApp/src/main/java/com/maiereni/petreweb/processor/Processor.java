/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor;

/**
 *
 * @author Petre Maierean
 */
public interface Processor {
    String ping();

    /**
     * Validate a request
     * @param ip
     * @param token
     * @return
     * @throws Exception
     */
    boolean validate(String ip, String token) throws Exception;

    /**
     * Save a comment
     * @param clientIp
     * @param email
     * @param comment
     * @throws Exception
     */
    void saveComment(String clientIp, String email, String comment) throws Exception;
}
