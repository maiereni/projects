/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.bo;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="ACCOUNTUSERROLE")
@NamedQueries({
        @NamedQuery(name = UserRole.FIND_USER_ROLE, query = "SELECT count(ur) FROM UserRole ur WHERE ur.user.userId = :" + User.USER_ID + " and ur.role.name = :" + Role.NAME),
        @NamedQuery(name = UserRole.DELETE_USER_ROLE, query = "DELETE FROM UserRole ur WHERE ur.user = (SELECT u FROM User u where u.userId = :" + User.USER_ID + ") and ur.role = (SELECT r FROM Role r WHERE r.name = :" + Role.NAME + ")")
})
public class UserRole implements Serializable {
    public static final String FIND_USER_ROLE = "UserRole.Find_User_Role";
    public static final String DELETE_USER_ROLE = "UserRole.Delete_User_Role";

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "char(36)")
    private String id;
    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;
    @ManyToOne
    @JoinColumn(name = "roleId", nullable = false)
    private Role role;
    @Column
    @Temporal(TemporalType.DATE)
    private Calendar creationDate;
    @Column(name="active")
    @ColumnDefault("false")
    private Boolean active;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
