/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency;

/**
 * Defines a simple API to handle user role associations
 * @author Petre Maierean
 */
public interface UserRoleDao {
    /**
     * Add user role
     * @param userId
     * @param roleName
     * @throws Exception
     */
    void addUserRole(String userId, String roleName) throws Exception;

    /**
     * Remove user role
     * @param userId
     * @param roleName
     * @throws Exception
     */
    void removeRole(String userId, String roleName) throws Exception;

    /**
     * Verifies if the user has role
     * @param userId
     * @param roleName
     * @return
     * @throws Exception
     */
    boolean isRole(String userId, String roleName) throws Exception;
}
