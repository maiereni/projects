/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.filter.oauth2;

import com.maiereni.petreweb.filter.csrf.LocalCsrfTokenRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.cache.Cache;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Petre Maierean
 */
public class HttpCookieOAuth2AuthorizationRequestRepository
        implements AuthorizationRequestRepository<OAuth2AuthorizationRequest> {
    private static final Logger logger = LogManager.getLogger(HttpCookieOAuth2AuthorizationRequestRepository.class);

    private LocalCsrfTokenRepository localCsrfTokenRepository;
    private Cache<String, OAuth2AuthorizationRequest> cache;


    HttpCookieOAuth2AuthorizationRequestRepository(
            LocalCsrfTokenRepository localCsrfTokenRepository,
            Cache<String, OAuth2AuthorizationRequest> cache) {
        this.localCsrfTokenRepository = localCsrfTokenRepository;
        this.cache = cache;
    }

    /**
     * Returns the OAuth2AuthorizationRequest associated to the provided HttpServletRequest or null if not available.
     * @param request
     * @return
     */
    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest(HttpServletRequest request) {
        OAuth2AuthorizationRequest ret = null;
        String key = getKey(request);
        if (key != null) {
            logger.debug("Find OAuth2AuthorizationRequest by key: " + key + " " + this);
            ret = cache.get(key);
        }
        if (ret == null) {
            logger.debug("The authorization request was not found in the cache");
        }
        else {
            String requestURL = UrlUtils.buildFullRequestUrl(request);
            UriComponents requestUri = UriComponentsBuilder.fromUriString(requestURL).build();
            UriComponents redirectUri = UriComponentsBuilder.fromUriString(ret.getRedirectUri()).build();
            if (!requestUri.getScheme().equals(redirectUri.getScheme())) {
                ret = OAuth2AuthorizationRequest.from(ret).redirectUri(requestURL).build();
            }
        }
        return ret;
    }

    /**
     * Persists the OAuth2AuthorizationRequest associating it to the provided HttpServletRequest and/or HttpServletResponse.
     * @param authorizationRequest
     * @param request
     * @param response
     */
    @Override
    public void saveAuthorizationRequest(OAuth2AuthorizationRequest authorizationRequest, HttpServletRequest request, HttpServletResponse response) {
        String key = getKey(request);
        if (!(key == null || authorizationRequest == null)) {
            String state = authorizationRequest.getState();
            localCsrfTokenRepository.addNonce(key, state, request, response);
            cache.put(state, authorizationRequest);
            logger.debug("The authorization request record was saved into the cache for " + state + " " + this);
        }
    }

    /**
     * Removes and returns the OAuth2AuthorizationRequest associated to the provided HttpServletRequest and HttpServletResponse or if not available returns null.
     * @param request
     * @return
     */
    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest request) {
        OAuth2AuthorizationRequest ret = null;
        String key = getKey(request);
        if (key != null) {
            ret = cache.getAndRemove(key);
            logger.debug("The authorization request record was removed from cache for " + key);
        }
        return ret;
    }

    private String getKey(HttpServletRequest request) {
        CsrfToken csrfToken = localCsrfTokenRepository.loadToken(request);
        return csrfToken != null ? csrfToken.getToken() : null;
    }
}
