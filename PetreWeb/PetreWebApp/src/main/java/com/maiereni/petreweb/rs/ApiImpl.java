/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.rs;

import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.filter.oauth2.AuthenticationEndpoints;
import com.maiereni.petreweb.processor.Processor;
import com.maiereni.rest.petreweb.model.*;
import com.maiereni.rest.petreweb.rs.ApiApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *
 * @author Petre Maierean
 */
@Component
public class ApiImpl extends ApiApi {
    private static final Logger logger = LogManager.getLogger(ApiImpl.class);

    @Autowired
    Processor processor;
    @Autowired
    Settings settings;
    @Autowired
    AuthenticationEndpoints authenticationEndpoints;


    @Override
    public Response contactUsConfiguration() {
        logger.debug("Get configuration");
        ContactUsConfiguration config = new ContactUsConfiguration();
        config.setSitekey(settings.getGoogleSiteKey());
        return Response.ok().entity(config).build();
    }

    @Override
    public Response ping() {
        String ret = processor.ping();
        return Response.ok().entity(ret).build();
    }

    @Override
    public Response validate(@Valid ContactUsValidationRequest contactUsValidationRequest) {
        ContactUsValidationResponse resp = new ContactUsValidationResponse();
        resp.setValid(Boolean.FALSE);
        try {
            if (processor.validate(contactUsValidationRequest.getClientIP(), contactUsValidationRequest.getToken())) {
                resp.setValid(Boolean.TRUE);
            }
            else {
                resp.setReason("Failed to validate");
            }
        }
        catch (Exception e) {
            logger.error("Failed to validate", e);
            resp.setReason(e.getMessage());
        }
        return Response.ok().entity(resp).build();
    }

    @Override
    public Response submit(@Valid ContactUsSubmitRequest rq) {
        ContactUsSubmitResponse resp = new ContactUsSubmitResponse();
        try {
            processor.saveComment(rq.getClientIP(), rq.getEmail(), rq.getComment());
            resp.setResponse("Successfully submitted. Thank you");
            resp.setCode("OK");
        }
        catch (Exception e) {
            logger.error("Failed to validate", e);
            resp.setResponse(e.getMessage());
            resp.setCode("Failure");
        }
        return Response.ok().entity(resp).build();
    }

}
