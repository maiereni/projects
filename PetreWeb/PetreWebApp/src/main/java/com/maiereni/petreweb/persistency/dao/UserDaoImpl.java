/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.persistency.UserDao;
import com.maiereni.petreweb.persistency.bo.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Component
public class UserDaoImpl extends BaseDaoImpl implements UserDao {
    private static Logger logger = LogManager.getLogger(UserDaoImpl.class);

    /**
     * Find a user by name
     * @param userId
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public User findUser(String userId) throws Exception {
        User ret = null;
        if (StringUtils.isNotBlank(userId)) {
            logger.debug("Find user for " + userId);
            ret = doFindUser(userId);
        }
        return ret;
    }

    /**
     * Verifies if there is a record in the database to match the user
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean isUser(String userId) throws Exception {
        boolean ret = false;
        if (StringUtils.isNotBlank(userId)) {
            logger.debug("Verifies if there is a user record for " + userId);
            Query query = entityManager.createNamedQuery(User.IS_USER_BY_NAME);
            query.setParameter(User.USER_ID, userId);
            Object o = query.getSingleResult();
            ret = ((Long)o).intValue() > 0;
            if (ret) {
                logger.debug("There is a user record for " + userId);
            }
        }
        return ret;
    }

    /**
     * Find users by email
     * @param email the email to find users for
     * @return a list of users
     * @throws Exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<User> findUsers(String email) throws Exception {
        List<User> ret = null;
        if (StringUtils.isNotBlank(email)) {
            logger.debug("Find user for " + email);
            TypedQuery<User> query = entityManager.createNamedQuery(User.FIND_USER_BY_EMAIL, User.class);
            query.setParameter(User.EMAIL, email);
            ret = query.getResultList();
            logger.debug("Found " + ret.size() + " users");
        }
        return ret;
    }

    /**
     * Save or update a user
     * @param user
     * @throws Exception
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(User user) throws Exception {
        if (user != null) {
            if (isUser(user.getUserId())) {
                entityManager.merge(user);
            }
            else {
                //user.setId(generateId());
                entityManager.persist(user);
            }
        }
    }
}
