/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.filter.oauth2;

import com.maiereni.petreweb.filter.oauth2.bo.UserIdentity;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * A user identity converted specific for the Google OAuth2 responses
 * @author Petre Maierean
 */
@Component
public class GoogleUserIdentityConverter implements UserIdentityConverter {
    private static final String NAME_KEY = "name";
    private static final String EMAIL_KEY = "email";
    private static final String FIRST_NAME_KEY = "family_name";
    private static final String LAST_NAME_KEY = "given_name";
    private static final String PICTURE_URL_KEY = "picture";
    private static final String LOCALE = "locale";
    private static final String ISS_NAME = "https://accounts.google.com";
    @Override
    public String getIssName() {
        return ISS_NAME;
    }

    @Override
    public UserIdentity convert(OidcUser oidcUser) {
        UserIdentity userIdentity = new UserIdentity();
        Map<String, Object> attributes = oidcUser.getAttributes();
        userIdentity.setUserId(getValue(attributes, NAME_KEY));
        userIdentity.setName(getValue(attributes, NAME_KEY));
        userIdentity.setEmail(getValue(attributes, EMAIL_KEY));
        userIdentity.setFamilyName(getValue(attributes, FIRST_NAME_KEY));
        userIdentity.setGivenName(getValue(attributes, LAST_NAME_KEY));
        userIdentity.setPictureUrl(getValue(attributes, PICTURE_URL_KEY));
        userIdentity.setEmailVerified(true);
        userIdentity.setLocale(getValue(attributes, LOCALE));
        return userIdentity;
    }

    private String getValue(Map<String, Object> attributes, String key) {
        Object object = attributes.get(key);
        return object != null? object.toString() : null;
    }
}
