/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.filter.oauth2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.filter.csrf.LocalCsrfTokenRepository;
import com.maiereni.petreweb.filter.oauth2.bo.DiscoveryDocument;
import com.maiereni.petreweb.filter.oauth2.bo.OAuth2Registration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import javax.cache.Cache;
import javax.cache.CacheManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Configuration factory classes for the Oauth2 objects
 * @author Petre Maierean
 */
@Configuration
public class Oauth2BeanFactory {
    private static final Logger logger = LogManager.getLogger(Oauth2BeanFactory.class);

    @Bean
    public ClientRegistrationRepository getCachableClientRegistrationRepository(
            Settings settings,
            ObjectMapper objectMapper,
            CacheManager cacheManager) {
        Cache<String, DiscoveryDocument> discoveryDocumentCache = cacheManager.getCache("oauth2Documents", String.class, DiscoveryDocument.class);
        List<OAuth2Registration> oAuth2RegistrationList = settings.getoAuth2RegistrationList();
        final Map<String, OAuth2Registration> oAuth2RegistrationMap = new HashMap<>();
        oAuth2RegistrationList.forEach( k -> {
            if (k != null) {
                String providerName = k.getProviderName();
                oAuth2RegistrationMap.put(providerName, k);
            }
        });
        return new CachableClientRegistrationRepository(
                discoveryDocumentCache,
                objectMapper,
                oAuth2RegistrationMap);
    }


    @Bean
    public HttpCookieOAuth2AuthorizationRequestRepository getStatusSaver(
            LocalCsrfTokenRepository localCsrfTokenRepository,
            CacheManager cacheManager) {
        Cache<String, OAuth2AuthorizationRequest> cache = cacheManager.getCache("oauth2requests");
        return new HttpCookieOAuth2AuthorizationRequestRepository(localCsrfTokenRepository, cache);
    }

}
