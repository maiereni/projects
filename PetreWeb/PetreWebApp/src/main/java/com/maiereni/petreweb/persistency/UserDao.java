/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency;

import com.maiereni.petreweb.persistency.bo.User;

import java.util.List;

/**
 * @author Petre Maierean
 */
public interface UserDao {
    /**
     * Find user by name
     * @param userId
     * @return
     * @throws Exception
     */
    User findUser(String userId) throws Exception;

    /**
     * Validates is there is a user by name
     * @param userId
     * @return
     * @throws Exception
     */
    boolean isUser(String userId) throws Exception;

    /**
     * Find users by email
     * @param email
     * @return
     * @throws Exception
     */
    List<User> findUsers(String email) throws Exception;

    /**
     * Save user
     * @param user
     * @throws Exception
     */
    void save(User user) throws Exception;
}
