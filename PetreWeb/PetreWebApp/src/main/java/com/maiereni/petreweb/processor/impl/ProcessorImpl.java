/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.processor.Processor;
import com.maiereni.petreweb.processor.bo.GoogleValidationRequest;
import com.maiereni.petreweb.processor.bo.GoogleValidationResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.concurrent.Callable;

/**
 * A processor that conveys the response on a separate thread by the request
 * @author Petre Maierean
 */
@Component
public class ProcessorImpl implements Processor {
    private static final Logger logger = LogManager.getLogger(ProcessorImpl.class);
    public static final String GOOGLE_URL = "https://www.google.com/recaptcha/api/siteverify";

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    Settings settings;
    @Autowired
    ObjectMapper objectMapper;

    @Override
    @Async
    public String ping() {
        String ret = "pong";
        return "pong";
    }

    @Override
    public boolean validate(final String ip, final String token) throws Exception {
        boolean ret = false;
        logger.debug("Make call to validate at Google");
        LinkedMultiValueMap<String, String> rq = new LinkedMultiValueMap<String,String>();
        rq.add("secret", settings.getGoogleSecret());
        rq.add("remoteip", ip);
        rq.add("response", token);
        ResponseEntity<GoogleValidationResponse> response = restTemplate.postForEntity(GOOGLE_URL, rq, GoogleValidationResponse.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            GoogleValidationResponse gr = response.getBody();
            ret = gr.isSuccess();
            if (gr.isSuccess()) {
                logger.debug("Validation has been successful");
            }
            else {
                logger.debug("Error validating " + gr.getErrorCodes());
            }
        }
        else {
            throw new Exception(response.getStatusCode().getReasonPhrase());
        }
        return ret;
    }

    /**
     * Save a comment
     * @param email
     * @param comment
     * @throws Exception
     */
    public void saveComment(String clientIp, String email, String comment) throws Exception {
        String s = new String(Base64Utils.decodeFromString(comment));
        logger.debug("Comment " + clientIp + "," + email + ",\"" + s + "\"");
    }
}
