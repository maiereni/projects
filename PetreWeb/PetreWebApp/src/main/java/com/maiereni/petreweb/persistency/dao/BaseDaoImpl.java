/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.persistency.bo.Role;
import com.maiereni.petreweb.persistency.bo.User;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
public abstract class BaseDaoImpl {

    @PersistenceContext
    protected EntityManager entityManager;

    protected User doFindUser(String userId) throws Exception {
        User ret = null;
        TypedQuery<User> query = entityManager.createNamedQuery(User.FIND_USER_BY_NAME, User.class);
        query.setParameter(User.USER_ID, userId);
        List<User> users = query.getResultList();
        if (users.size() > 0) {
            ret = users.get(0);
        }
        return ret;
    }

    protected Role doFindRole(String name) throws Exception {
        Role ret = null;
        TypedQuery<Role> query = entityManager.createNamedQuery(Role.FIND_ROLE_BY_NAME, Role.class);
        query.setParameter(Role.NAME, name);
        List<Role> roles = query.getResultList();
        if (roles.size() > 0) {
            ret = roles.get(0);
        }
        return ret;
    }

    protected String generateId() {
        String id = UUID.randomUUID().toString().replaceAll("-", "");
        return id;
    }
}
