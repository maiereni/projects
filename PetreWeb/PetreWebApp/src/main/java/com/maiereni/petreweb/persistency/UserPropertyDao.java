/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency;

import com.maiereni.petreweb.persistency.bo.User;
import com.maiereni.petreweb.persistency.bo.UserProperty;

import java.util.List;

/**
 * @author Petre Maierean
 */
public interface UserPropertyDao {
    /**
     * List the properties of a user
     * @param userId
     * @return
     * @throws
     */
    List<UserProperty> getUserProperties(String userId) throws Exception ;

    /**
     * Add a property to the user
     * @param userProperty
     * @throws
     */
    void addUserProperty(UserProperty userProperty) throws Exception ;

    /**
     * Delete the properties of a user
     * @param userProperty
     * @throws
     */
    void deleteUserProperty(UserProperty userProperty) throws Exception ;

}
