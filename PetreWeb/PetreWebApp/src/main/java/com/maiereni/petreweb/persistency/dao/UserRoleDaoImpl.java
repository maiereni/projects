/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.persistency.UserRoleDao;
import com.maiereni.petreweb.persistency.bo.Role;
import com.maiereni.petreweb.persistency.bo.User;
import com.maiereni.petreweb.persistency.bo.UserRole;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Calendar;

/**
 * An implementation of the UserRoleDao
 * @author Petre Maierean
 */
@Component
public class UserRoleDaoImpl extends BaseDaoImpl implements UserRoleDao {
    private static Logger logger = LogManager.getLogger(UserRoleDaoImpl.class);

    /**
     * Adds a role to a user
     * @param userId
     * @param roleName
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void addUserRole(String userId, String roleName) throws Exception {
        if (StringUtils.isNoneBlank(userId, roleName)) {
            if (!isRole(userId, roleName)) {
                logger.debug("Add role '" + roleName + "' to user " + userId);
                User user = doFindUser(userId);
                Role role = doFindRole(roleName);
                UserRole userRole = new UserRole();
                //userRole.setId(generateId());
                userRole.setUser(user);
                userRole.setRole(role);
                userRole.setCreationDate(Calendar.getInstance());
                userRole.setActive(Boolean.TRUE);
                entityManager.persist(userRole);
                user.getUserRoles().add(userRole);
                entityManager.merge(user);
                role.getUserRole().add(userRole);
                entityManager.merge(role);
            }
        }
    }

    /**
     * Removes a role
     * @param userId the userId
     * @param roleName the role name
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void removeRole(String userId, String roleName) throws Exception {
        if (StringUtils.isNoneBlank(userId, roleName)) {
            if (isRole(userId, roleName)) {
                logger.debug("Remove role '" + roleName + "' to user " + userId);
                Query query = entityManager.createNamedQuery(UserRole.DELETE_USER_ROLE);
                query.setParameter(User.USER_ID, userId);
                query.setParameter(Role.NAME, roleName);
                query.executeUpdate();
            }
        }
    }

    /**
     * Verifies if the user has the specified role
     * @param userId
     * @param roleName
     * @return
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public boolean isRole(String userId, String roleName) throws Exception {
        boolean ret = false;
        if (StringUtils.isNoneBlank(userId, roleName)) {
            Query query = entityManager.createNamedQuery(UserRole.FIND_USER_ROLE);
            query.setParameter(User.USER_ID, userId);
            query.setParameter(Role.NAME, roleName);
            Object o = query.getSingleResult();
            ret = ((Long)o).intValue() > 0;
            if (ret) {
                logger.debug("The role '" + roleName + "' has been found to the user " + userId);
            }
        }
        return ret;
    }
}
