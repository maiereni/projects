/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.processor.CookieUserResolver;
import com.maiereni.petreweb.processor.bo.UserCookie;
import com.maiereni.utils.encryption.EncryptionProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;

/**
 * An implementation of the cookie user resolver that uses encryption to
 *
 * @author Petre Maierean
 */
@Component
public class CookieUserResolverImpl implements CookieUserResolver {
    private static final Logger logger = LogManager.getLogger(CookieUserResolverImpl.class);
    @Autowired
    private EncryptionProvider encryptionProvider;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private Settings settings;

    /**
     * Creates a cookie containing the current user id and the login time in an encrypted form
     * @param userCookie
     * @return
     * @throws Exception
     */
    @Override
    public Cookie getSessionCookie(UserCookie userCookie)  throws Exception {
        Cookie ret = null;
        if (userCookie != null) {
            String sUserCookie = objectMapper.writeValueAsString(userCookie);
            String value = encryptionProvider.encrypt(sUserCookie);
            ret = new Cookie(settings.getNameOfSessionCookie(),value);
            ret.setDomain(settings.getDomain());
            ret.setPath("/");
            ret.setHttpOnly(true);
            if (userCookie.isSecure()) {
                ret.setSecure(true);
            }
            ret.setMaxAge(-1);
            logger.debug("Create a session cookie for the user");
        }
        return ret;
    }

    /**
     * Resolves the user name for the cookie provided
     * @param cookie
     * @return
     * @throws Exception
     */
    @Override
    public UserCookie getUser(Cookie cookie) throws Exception {
        UserCookie ret = null;
        if (cookie != null && cookie.getName().equalsIgnoreCase(settings.getNameOfSessionCookie())) {
            String value = cookie.getValue();
            if (StringUtils.isNotBlank(value)) {
                String decrypted = encryptionProvider.decrypt(value);
                ret = objectMapper.readValue(decrypted, UserCookie.class);
            }
        }
        return ret;
    }

}
