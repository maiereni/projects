/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletContextListener;
import java.io.File;

/**
 *
 *
 * @author Petre Maierean
 */
@SpringBootApplication(scanBasePackages = {"com.maiereni.petreweb","com.maiereni.petreweb.security","com.maiereni.rest"})
@EntityScan(basePackages = {"com.maiereni.petreweb.persistency.bo","com.maiereni.rest.wizard.persistency.bo"})
public class ApplicationMain extends SpringBootServletInitializer implements ServletContextListener {
    private static Logger logger;

    public static void main(String[] args) {
        String logConfigFile = System.getProperty("logging.config");
        if (StringUtils.isNotBlank(logConfigFile)) {
            Configurator.initialize("PetreWeb", logConfigFile);
        }
        else {
            logConfigFile = ApplicationMain.class.getResource("/log4j2.xml").getPath();
        }
        Logger logger = LogManager.getLogger(ApplicationMain.class);
        logger.debug("Log configured from " + logConfigFile);
        final LifecycleManager manager = new LifecycleManager();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                manager.doShutdown();
            }
        }));
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ApplicationMain.class);
    }

    public static class LifecycleManager implements Runnable {
        public ConfigurableApplicationContext ctxt;

        LifecycleManager() {
            // as per https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#boot-features-application-exit
            ctxt = SpringApplication.run(ApplicationMain.class);
            Thread th = new Thread(this);
            th.setDaemon(true);
            th.start();
        }

        public void doAfterStartup() {
            String[] beanNames = ctxt.getBeanNamesForType(StartupProcessor.class);
            if (beanNames != null) {
                for(String beanName: beanNames) {
                    StartupProcessor processor = ctxt.getBean(beanName, StartupProcessor.class);
                    processor.startup();
                }
            }
        }

        public void doShutdown() {
            logger.debug("Shut down gracefully");
            String[] beanNames = ctxt.getBeanNamesForType(ShutdownProcessor.class);
            if (beanNames != null) {
                for(String beanName: beanNames) {
                    ShutdownProcessor processor = ctxt.getBean(beanName, ShutdownProcessor.class);
                    processor.preShutdown();
                }
            }
            SpringApplication.exit(ctxt);
            logger.info("Shutting down!!");
        }

        @Override
        public void run() {
            String fd = System.getProperty("user.dir");
            File f = new File(fd, "stop");
            while(!f.exists()) {
                try {
                    Thread.sleep(1000L);
                }
                catch(Exception e) {
                    logger.error("Cannot wait", e);
                }
            }
            if (!f.delete()) {
                f.deleteOnExit();
            }
            doShutdown();
        }
    }
}
