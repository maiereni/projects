/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author Petre Maierean
 */
@Configuration
public class DerbyDatasourceFactory {

    @Bean("dataSource")
    public DataSource getDatasource(DerbyProperties props) throws Exception {
        BasicDataSource ret = new BasicDataSource();
        ret.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        String url = "jdbc:derby:directory:" + props.getPath();
        ret.setUrl(url);
        ret.setUsername(props.getUser());
        ret.setPassword(props.getPassword());
        ret.setInitialSize(1);
        ret.setMaxIdle(60);
        ret.setMinIdle(60);
        return ret;
    }


}
