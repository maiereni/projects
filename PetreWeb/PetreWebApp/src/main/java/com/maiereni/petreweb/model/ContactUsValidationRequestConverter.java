/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.rest.petreweb.model.ContactUsValidationRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.ext.ParamConverter;

/**
 *
 * @author Petre Maierean
 */
public class ContactUsValidationRequestConverter implements ParamConverter<ContactUsValidationRequest> {
    private static final Logger logger = LogManager.getLogger(ContactUsValidationRequestConverter.class);
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public ContactUsValidationRequest fromString(String value) {
        ContactUsValidationRequest ret = null;
        try {
            ret =  objectMapper.readValue(value, ContactUsValidationRequest.class);
        }
        catch (Exception e) {
            logger.error("Failed to convert string to value", e);
        }
        return ret;
    }

    @Override
    public String toString(ContactUsValidationRequest value) {
        String ret = null;
        try {
            ret = objectMapper.writeValueAsString(value);
        }
        catch(Exception e) {
            logger.error("Failed to convert value to string", e);
        }

        return ret;
    }
}
