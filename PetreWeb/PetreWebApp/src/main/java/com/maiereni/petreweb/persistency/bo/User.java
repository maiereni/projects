/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.bo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="ACCOUNTUSER")
@NamedQueries({
        @NamedQuery(name = User.FIND_USER_BY_NAME, query = "SELECT u FROM User u WHERE u.userId = :" + User.USER_ID),
        @NamedQuery(name = User.FIND_USER_BY_EMAIL, query = "SELECT u FROM User u WHERE u.email = :" + User.EMAIL),
        @NamedQuery(name = User.IS_USER_BY_NAME, query = "SELECT count( u ) FROM User u WHERE u.userId = :" + User.USER_ID)
})
public class User implements Serializable {
    public static final String USER_ID = "userId";
    public static final String EMAIL = "email";
    public static final String FIND_USER_BY_NAME = "User.FindByUserId";
    public static final String FIND_USER_BY_EMAIL = "User.FindByEmail";
    public static final String IS_USER_BY_NAME = "User.IsRecordByUserId";
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(36)")
    private String id;
    @Column(columnDefinition = "VARCHAR(32)", nullable = false)
    private String userId;
    @Column(columnDefinition = "VARCHAR(32)", nullable = false)
    private String name;
    @Column(columnDefinition = "VARCHAR(64)", nullable = false)
    private String firstName;
    @Column(columnDefinition = "VARCHAR(64)", nullable = false)
    private String lastName;
    @Column(columnDefinition = "VARCHAR(256)", nullable = false)
    private String email;
    @Column(columnDefinition = "VARCHAR(256)", nullable = true)
    private String pictureUrl;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar creationDate;
    @Column(nullable = true)
    @Temporal(TemporalType.DATE)
    private Calendar lastLoginDate;
    @OneToMany(mappedBy="user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserRole> userRoles;
    @OneToMany(mappedBy="user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserProperty> userProperties;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public Calendar getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Calendar lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public Set<UserProperty> getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(Set<UserProperty> userProperties) {
        this.userProperties = userProperties;
    }
}
