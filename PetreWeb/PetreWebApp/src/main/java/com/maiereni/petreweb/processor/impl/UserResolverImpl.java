/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor.impl;

import com.maiereni.petreweb.filter.oauth2.bo.UserIdentity;
import com.maiereni.petreweb.persistency.RoleDao;
import com.maiereni.petreweb.persistency.UserDao;
import com.maiereni.petreweb.persistency.UserPropertyDao;
import com.maiereni.petreweb.persistency.UserRoleDao;
import com.maiereni.petreweb.persistency.bo.Role;
import com.maiereni.petreweb.persistency.bo.User;
import com.maiereni.petreweb.persistency.bo.UserProperty;
import com.maiereni.petreweb.processor.UserResolver;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A processor class that implements the UserResolver interface
 * @author Petre Maierean
 */
@Component
public class UserResolverImpl implements UserResolver {
    private static Logger logger = LogManager.getLogger(UserResolverImpl.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserRoleDao userRoleDao;
    @Autowired
    private UserPropertyDao userPropertyDao;

    /**
     * Resolve a user
     * @param userIdentity  the identity of the user
     * @return
     * @throws Exception
     */
    @Override
    public User getUser(UserIdentity userIdentity) throws Exception {
        User ret = null;
        if (userIdentity != null) {
            if ( StringUtils.isNoneBlank(userIdentity.getUserId(), userIdentity.getEmail())) {
                ret = getUserById(userIdentity.getUserId());
            }
            else if ( StringUtils.isNoneBlank(userIdentity.getName(), userIdentity.getEmail() )) {
                List<User> users =  userDao.findUsers(userIdentity.getEmail());
                for(User user: users) {
                    if (user.getName().equals(userIdentity.getName())) {
                        ret = user;
                        break;
                    }
                }
            }
        }
        else {
            throw new Exception("The argument is null, or it contains wrong data");
        }
        return ret;
    }

    /**
     * Validates that the user is known
     * @param userIdentity the identity of the user
     * @return
     * @throws Exception
     */
    @Override
    public boolean isUser(UserIdentity userIdentity) throws Exception {
        User user = getUser(userIdentity);
        return user != null;
    }

    /**
     * Creates a user
     * @param userIdentity
     * @param source
     * @return
     * @throws Exception
     */
    @Override
    public User createUser(UserIdentity userIdentity, String source) throws Exception {
        User ret = null;
        if (!isUser(userIdentity)) {
            logger.debug("Create a user for an identity for source " + source);
            ret = new User();
            ret.setUserId(userIdentity.getUserId());
            ret.setName(userIdentity.getName());
            ret.setLastName(userIdentity.getFamilyName());
            ret.setFirstName(userIdentity.getGivenName());
            ret.setEmail(userIdentity.getEmail());
            ret.setCreationDate(Calendar.getInstance());
            ret.setPictureUrl(userIdentity.getPictureUrl());
            userDao.save(ret);
            addRole(userIdentity.getUserId(), Role.DEFAULT_USER);
            ret = getUserById(userIdentity.getUserId());
            UserProperty userProperty = new UserProperty();
            userProperty.setUser(ret);
            userProperty.setName("source");
            userProperty.setType("string");
            userProperty.setValue(source);
            userPropertyDao.addUserProperty(userProperty);
        }
        return ret;
    }

    /**
     * Get a user by name
     * @param userId
     * @return
     * @throws Exception
     */
    @Override
    public User getUserById(String userId) throws Exception {
        User ret = null;
        if (StringUtils.isNotBlank(userId)) {
            ret = userDao.findUser(userId);
        }
        return ret;
    }

    /**
     * Add a role to the user identity
     * @param userIdentity
     * @param roleName
     * @throws Exception
     */
    @Override
    public void addRole(UserIdentity userIdentity, String roleName) throws Exception {
        if (isUser(userIdentity) && StringUtils.isNotBlank(roleName)) {
            addRole(userIdentity.getUserId(), roleName);
        }
    }

    /**
     * Get roles for the user identity
     * @param userIdentity
     * @return
     * @throws Exception
     */
    @Override
    public List<String> getRoles(UserIdentity userIdentity) throws Exception {
        final List<String> ret = new ArrayList<>();
        User user = getUser(userIdentity);
        user.getUserRoles().forEach( ur -> {
            ret.add(ur.getRole().getName());
        });
        return ret;
    }

    private void addRole(String userId, String roleName) throws Exception {
        if (!roleDao.isRole(roleName)) {
            Role role = new Role();
            role.setName(roleName);
            role.setDescription("");
            if (roleName.equalsIgnoreCase(Role.DEFAULT_USER)) {
                role.setDescription(Role.DEFAULT_USER_DESCRIPTION);
            }
            role.setCreationDate(Calendar.getInstance());
            roleDao.save(role);
        }
        userRoleDao.addUserRole(userId, roleName);
    }
}
