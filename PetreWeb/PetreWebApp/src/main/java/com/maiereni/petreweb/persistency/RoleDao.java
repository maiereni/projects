/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency;

import com.maiereni.petreweb.persistency.bo.Role;

import java.util.List;

/**
 * @author Petre Maierean
 */
public interface RoleDao {
    /**
     * Find a role by name
     * @param name
     * @return
     * @throws Exception
     */
    Role findRole(String name) throws Exception;

    /**
     * Find all the roles
     * @return
     * @throws Exception
     */
    List<Role> findRoles() throws Exception;

    /**
     * Validates that the role exist
     * @param name
     * @return
     */
    boolean isRole(String name);

    /**
     * Save role
     * @param role
     * @throws Exception
     */
    void save(Role role) throws Exception;
}
