/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb;

import com.maiereni.petreweb.filter.csrf.CsrfLoggingFilter;
import com.maiereni.petreweb.filter.csrf.CustomCsrfRequestMatcher;
import com.maiereni.petreweb.filter.oauth2.CachableClientRegistrationRepository;
import com.maiereni.petreweb.filter.oauth2.HttpCookieOAuth2AuthorizationRequestRepository;
import com.maiereni.petreweb.security.CustomAuthenticationEntryPoint;
import com.maiereni.petreweb.security.LocalSecurityContextRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.web.OAuth2LoginAuthenticationFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;

import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author Petre Maierean
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true
)
@EnableCaching
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static final Logger logger = LogManager.getLogger(SecurityConfiguration.class);
    @Autowired
    LocalSecurityContextRepository localSecurityContextRepository;
    @Autowired
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
    @Autowired
    CsrfTokenRepository csrfTokenRepository;
    @Autowired
    CachableClientRegistrationRepository cachableClientRegistrationRepository;
    @Autowired
    HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;
    @Autowired
    AuthenticationSuccessHandler authenticationSuccessHandler;

    AuthenticationManagerResolver<HttpServletRequest> resolver() {
        return new org.springframework.security.oauth2.server.resource.authentication.JwtIssuerAuthenticationManagerResolver();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        SimpleUrlAuthenticationFailureHandler authenticationFailureHandler = new SimpleUrlAuthenticationFailureHandler();
        authenticationFailureHandler.setDefaultFailureUrl("/error/authentication.html");
        http.oauth2Login(oauth2Login -> {
                oauth2Login
                    .clientRegistrationRepository(cachableClientRegistrationRepository)
                    .authorizationEndpoint().baseUri("/mvc/authorization")
                    .authorizationRequestRepository(httpCookieOAuth2AuthorizationRequestRepository);
                oauth2Login.loginProcessingUrl("/mvc" + OAuth2LoginAuthenticationFilter.DEFAULT_FILTER_PROCESSES_URI);
                oauth2Login.successHandler(authenticationSuccessHandler);
                oauth2Login.failureHandler(authenticationFailureHandler);
        });
        http.oauth2ResourceServer().authenticationManagerResolver(resolver());

        http.securityContext().securityContextRepository(localSecurityContextRepository);
        http.csrf().requireCsrfProtectionMatcher(new CustomCsrfRequestMatcher())
                .csrfTokenRepository(csrfTokenRepository);

        http.exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint);
        http.addFilterAfter(new CsrfLoggingFilter(), CsrfFilter.class)

                .authorizeRequests()
                .antMatchers("/mvc/error").permitAll()
                .antMatchers("/*.html").permitAll()
                .antMatchers("/wizard/*.html").permitAll()
                .antMatchers("/api/login/list").permitAll()
                .antMatchers("/api/api/contactus/*").permitAll()
                .antMatchers("/api/api/buildComp/*").permitAll()
                .antMatchers("/*/dashboard/*.html").authenticated()
                .antMatchers("/api/api/account/*").authenticated()
                .antMatchers("/api/admin/buildComp/*").authenticated();
    }

}
