/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.mvc.controller;

import com.maiereni.petreweb.context.Settings;
import com.maiereni.petreweb.filter.csrf.LocalCsrfTokenRepository;
import com.maiereni.petreweb.processor.CookieUserResolver;
import com.maiereni.petreweb.processor.UserResolver;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Petre Maierean
 */
@Controller
public class LoginController {
    private static final Logger logger = LogManager.getLogger(LoginController.class);
    private static final String GOOGLE_AUTHENTICATION_URL = "https://accounts.google.com/o/oauth2/v2/auth";
    private static final String CODE = "code";
    private static final String SCOPE = "scope";
    @Autowired
    Settings settings;
    @Autowired
    UserResolver userResolver;
    @Autowired
    CookieUserResolver cookieUserResolver;
    @Autowired
    LocalCsrfTokenRepository csrfTokenRepository;
    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @GetMapping(path="/login.html")
    public String getLogin(HttpServletRequest request, HttpServletResponse response, Model model) {
        CsrfToken csrfToken = csrfTokenRepository.loadToken(request);
        if (csrfToken == null) {
            csrfToken = csrfTokenRepository.generateToken(request);
            csrfTokenRepository.saveToken(csrfToken, request, response);
        }
        String nonce = csrfTokenRepository.addNonce(csrfToken, request, response);
        model.addAttribute("redirect_uri", getRedirectUrl(request, "/login/index.html"));
        model.addAttribute("csrfName", csrfToken.getParameterName());
        model.addAttribute("csrfValue",  csrfToken.getToken());
        model.addAttribute(LocalCsrfTokenRepository.CSRF_TOKEN_NONCE, nonce);
        return "redirectGoogle";
    }

    protected Map<String, String> getTokenizedQuery(HttpServletRequest req) {
        Map<String, String> ret = new HashMap<>();
        String q = req.getQueryString();
        if (StringUtils.isNotBlank(q)) {
            logger.debug("Response is " + q);
            String[] toks = q.split("\\x26");
            for(String tok: toks) {
                String[] toks1 = tok.split("\\x3d");
                if (toks1.length == 2) {
                    ret.put(toks1[0], toks1[1]);
                }
                else {
                    ret.put(toks1[0], "");
                }
            }
        }
        return ret;
    }

    protected boolean isAntiForgeryTokenNotValid(HttpServletRequest req, String tok) {
        return !isAntiForgeryTokenValid(req, tok);
    }

    protected boolean isAntiForgeryTokenValid(HttpServletRequest request, String tok) {
        boolean ret = false;
        CsrfToken csrfToken = csrfTokenRepository.loadToken(request);
        Object o = request.getSession().getAttribute(csrfToken.getParameterName());
        if (o != null && StringUtils.isNotBlank(tok)) {
            String sAntiForgeryToken = o.toString();
            ret = sAntiForgeryToken.equals(tok);
        }
        return ret;
    }

    protected String getRedirectUrl(HttpServletRequest req, String s) {
        StringBuilder url = new StringBuilder();
        url.append("https://").append(req.getServerName());
        url.append(s);
        return url.toString();
    }
}
