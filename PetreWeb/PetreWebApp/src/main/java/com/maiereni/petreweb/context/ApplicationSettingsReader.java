/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.context;

import com.maiereni.petreweb.filter.oauth2.bo.OAuth2Registration;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Petre Maierean
 */
public class ApplicationSettingsReader extends Properties {
    private static final Logger logger = LogManager.getLogger(ApplicationSettingsReader.class);
    /**
     * The secret key used by the application to protect its view pages containing forms with Recaptcha 3
     */
    public static final String GOOGLE_SECRET_KEY = "google.secret.key";
    /**
     * The site key used by the application to protect its view pages containing forms with Recaptcha 3
     */
    public static final String GOOGLE_SITE_KEY = "google.site.key";

    private static final String OAUTH2_REGISTRATION_KEY = "oauth2.client.registration";
    private static final String CLIENT_SECRET = OAUTH2_REGISTRATION_KEY + ".%s.client-secret";
    private static final String CLIENT_ID = OAUTH2_REGISTRATION_KEY + ".%s.client-id";
    private static final String DISCOVERY_DOCUMENT_URL = OAUTH2_REGISTRATION_KEY + ".%s.document-discovery-url";
    private static final String REDIRECT_URI = OAUTH2_REGISTRATION_KEY + ".%s.redirect_uri";

    /**
     * The secret key used by the application to use the OAuth functionality from Google
     */
    public static final String GOOGLE_OAUTH2_SECRET = String.format(CLIENT_SECRET, "google");
    /**
     * The client id used by the application to use the OAuth functionality from Google
     */
    public static final String GOOGLE_OAUTH2_ID = String.format(CLIENT_ID, "google");
    /**
     * The Google OAuth2 Provider document discovery URL
     */
    public static final String GOOGLE_OAUTH2_DOCUMENT_DESCOVERY_URL_KEY = String.format(DISCOVERY_DOCUMENT_URL, "google");
    public static final String GOOGLE_OAUTH2_DOCUMENT_DESCOVERY_URL = "https://accounts.google.com/.well-known/openid-configuration";
    /**
     * The path to the keystore file containing the keypair or secret key used to encrypt the internal cookies or files
     */
    public static final String KEY_STORE = "keyStore";
    /**
     * The password for the keystore file used to encrypt the internal cookies or files
     */
    public static final String KEY_STORE_PASS = "keyStorePassword";
    /**
     * The password for the key pair or the secret contained in the keystore file used to encrypt the internal cookies or files
     */
    public static final String KEY_PASS = "keyPassword";
    /**
     * The alias of the key pair or the secret contained in the keystore file used to encrypt the internal cookies or files
     */
    public static final String KEY_ALIAS = "keyAlias";
    /**
     * The keystore used when establishing an SSL connection with an external server
     */
    public static final String KEY_STORE_VM = "javax.net.ssl." + KEY_STORE;
    /**
     * The password for the keystore used when establishing an SSL connection with an external server
     */
    public static final String KEY_STORE_PASS_VM = "javax.net.ssl." + KEY_STORE_PASS;
    /**
     * The password to the key pair contained in the keystore used when establishing an SSL connection with an external server
     */
    public static final String KEY_PASS_VM = "javax.net.ssl." + KEY_PASS;
    /**
     * The alias to the key pair contained in the keystore used when establishing an SSL connection with an external server
     */
    public static final String KEY_ALIAS_VM = "javax.net.ssl." + KEY_ALIAS;
    /**
     * Domain of the website
     */
    public static final String DOMAIN_NAME = "domain.name";
    /**
     * The name of the session cookie
     */
    public static final String SESSION_COOKIE_NAME = "session.cookie.name";
    /**
     * The default session cookie name
     */
    public static final String DEFAULT_SESSION_COOKIE_NAME = "pwc";
    /**
     * The VM property that points to the file containing properties
     */
    public static final String PROPERTIES_FILE_PATH = "app.properties";
    /**
     * The default configuration file
     */
    public static final String DEFAULT_PROPERTIES_FILE_PATH = "configration.properties";
    private File fProps;

    public ApplicationSettingsReader() throws Exception {
        fProps = getPropertiesFilePath();
        if (!fProps.isFile()) {
            throw new Exception("Could not find file with properties at " + fProps.getPath());
        }
        try(FileInputStream fis = new FileInputStream(fProps)) {
            load(fis);
        }
    }

    public File getfProps() {
        return fProps;
    }

    private File getPropertiesFilePath() {
        String appProperties = System.getProperty(PROPERTIES_FILE_PATH, DEFAULT_PROPERTIES_FILE_PATH);
        return new File(appProperties);
    }

    public Settings readProperties() {
        final Settings ret = new Settings();
        ret.setGoogleSecret(getProperty(ApplicationSettingsReader.GOOGLE_SECRET_KEY));
        ret.setGoogleSiteKey(getProperty(ApplicationSettingsReader.GOOGLE_SITE_KEY));
        ret.setKeystore(getSetting(ApplicationSettingsReader.KEY_STORE_VM, ApplicationSettingsReader.KEY_STORE));
        ret.setKeystorePassword(getSetting(ApplicationSettingsReader.KEY_STORE_PASS_VM, ApplicationSettingsReader.KEY_STORE_PASS));
        ret.setKeyPassword(getSetting(ApplicationSettingsReader.KEY_PASS_VM, ApplicationSettingsReader.KEY_PASS));
        ret.setAlias(getSetting(ApplicationSettingsReader.KEY_ALIAS_VM, ApplicationSettingsReader.KEY_ALIAS));
        ret.setDomain(getSetting(ApplicationSettingsReader.DOMAIN_NAME, ApplicationSettingsReader.DOMAIN_NAME));
        ret.setNameOfSessionCookie(
                getSetting(ApplicationSettingsReader.SESSION_COOKIE_NAME,
                        ApplicationSettingsReader.SESSION_COOKIE_NAME,
                        ApplicationSettingsReader.DEFAULT_SESSION_COOKIE_NAME));
        ret.setGoogleOath2ClientSecret(getProperty(ApplicationSettingsReader.GOOGLE_OAUTH2_SECRET));
        ret.setGoogleOath2ClientId(getProperty(ApplicationSettingsReader.GOOGLE_OAUTH2_ID));
        ret.setoAuth2RegistrationList(loadOAuth2RegistrationList());
        List<String> redirectionUris = getRedirectionUris(ret.getoAuth2RegistrationList(),ret.getDomain());
        ret.setoAuth2RedirectionUris(redirectionUris);
        return ret;
    }

    private List<String> getRedirectionUris(List<OAuth2Registration> auth2Registrations, String domain) {
        final List<String> ret = new ArrayList<>();
        auth2Registrations.forEach( reg -> {
            if (reg != null) {
                logger.error("Read for " + reg.getProviderName());
                String oAuth2RedirectUri = reg.getRedirectUri();
                if (StringUtils.isNotBlank(oAuth2RedirectUri)) {
                    int ix = oAuth2RedirectUri.indexOf(domain);
                    if (ix > 0) {
                        ix = ix + domain.length();
                        oAuth2RedirectUri = "/mvc" + oAuth2RedirectUri.substring(ix);
                    }
                    if (ret.indexOf(oAuth2RedirectUri) == -1) {
                        ret.add(oAuth2RedirectUri);
                    }
                }
            }
        });
        return ret;
    }

    private List<OAuth2Registration> loadOAuth2RegistrationList() {
        final List<OAuth2Registration> ret = new ArrayList<>();
        final List<String> providerNames = new ArrayList<>();
        keySet().forEach((k) -> {
            String key = k.toString();
            if (key.startsWith(OAUTH2_REGISTRATION_KEY)) {
                String[] toks = key.split("\\x2E");
                if (toks.length > 4) {
                    String providerName = toks[3];
                    if (!providerNames.contains(providerName)) {
                        OAuth2Registration oAuth2Registration = getOAuth2Registration(providerName);
                        ret.add(oAuth2Registration);
                        providerNames.add(providerName);
                    }
                }
            }
        });
        return ret;
    }

    private OAuth2Registration getOAuth2Registration(String providerName) {
        OAuth2Registration ret = null;
        String clientId = getProperty(String.format(CLIENT_ID, providerName));
        String clientSecret = getProperty(String.format(CLIENT_SECRET, providerName));
        String discoveryDocumentURL = getProperty(String.format(DISCOVERY_DOCUMENT_URL, providerName));
        String redirectUri = getProperty(String.format(REDIRECT_URI, providerName));
        if (StringUtils.isNoneBlank(clientId, clientSecret, redirectUri)) {
            ret = new OAuth2Registration();
            ret.setProviderName(providerName);
            ret.setClientId(clientId);
            ret.setClientSecret(clientSecret);
            ret.setDiscoveryUrl(discoveryDocumentURL);
            ret.setRedirectUri(redirectUri);
        }
        return ret;
    }

    private String getSetting(String systemKey, String propertyKey) {
        return System.getProperty(systemKey, getProperty(propertyKey));
    }

    private String getSetting(String systemKey, String propertyKey, String defaultValue) {
        return System.getProperty(systemKey, getProperty(propertyKey, defaultValue));
    }
}
