/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.context;

import com.maiereni.petreweb.context.shopify.ShopifyConfigurations;
import com.maiereni.petreweb.filter.oauth2.bo.OAuth2Registration;

import java.io.Serializable;
import java.util.List;

/**
 * @author Petre Maierean
 */
public class Settings implements Serializable {
    public static final String DEFAULT_SESSION_SETTING_NAME = "SITE_USER";
    private static final String GOOGLE_AUTHENTICATION_URL = "https://accounts.google.com/o/oauth2/v2/auth";
    private static final String DEFAULT_DASHBOARD_PAGE = "/dashboard/index.html";
    private static final String DEFAULT_ERROR_PAGE = "/error.html";
    private String docRoot, baseDir, googleSecret, googleSiteKey, googleOath2ClientId, googleOath2ClientSecret,
            keystore, keystorePassword, keyPassword, alias, domain,
            nameOfSessionCookie = DEFAULT_SESSION_SETTING_NAME,
            dashboardPage = DEFAULT_DASHBOARD_PAGE, errorPage = DEFAULT_ERROR_PAGE;
    private String googleAuthenticationUrl = GOOGLE_AUTHENTICATION_URL;
    private List<String> foldersToRemove;
    private List<String> oAuth2RedirectionUris;
    private List<OAuth2Registration> oAuth2RegistrationList;

    public String getDocRoot() {
        return docRoot;
    }

    public void setDocRoot(String docRoot) {
        this.docRoot = docRoot;
    }

    public String getBaseDir() {
        return baseDir;
    }

    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }

    public List<String> getFoldersToRemove() {
        return foldersToRemove;
    }

    public void setFoldersToRemove(List<String> foldersToRemove) {
        this.foldersToRemove = foldersToRemove;
    }

    public String getGoogleSecret() {
        return googleSecret;
    }

    public void setGoogleSecret(String googleSecret) {
        this.googleSecret = googleSecret;
    }

    public String getGoogleSiteKey() {
        return googleSiteKey;
    }

    public void setGoogleSiteKey(String googleSiteKey) {
        this.googleSiteKey = googleSiteKey;
    }

    public String getGoogleOath2ClientId() {
        return googleOath2ClientId;
    }

    public void setGoogleOath2ClientId(String googleOath2ClientId) {
        this.googleOath2ClientId = googleOath2ClientId;
    }

    public String getGoogleOath2ClientSecret() {
        return googleOath2ClientSecret;
    }

    public void setGoogleOath2ClientSecret(String googleOath2ClientSecret) {
        this.googleOath2ClientSecret = googleOath2ClientSecret;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getKeystore() {
        return keystore;
    }

    public void setKeystore(String keystore) {
        this.keystore = keystore;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getKeyPassword() {
        return keyPassword;
    }

    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNameOfSessionCookie() {
        return nameOfSessionCookie;
    }

    public void setNameOfSessionCookie(String nameOfSessionCookie) {
        this.nameOfSessionCookie = nameOfSessionCookie;
    }

    public String getGoogleAuthenticationUrl() {
        return googleAuthenticationUrl;
    }

    public void setGoogleAuthenticationUrl(String googleAuthenticationUrl) {
        this.googleAuthenticationUrl = googleAuthenticationUrl;
    }

    public List<OAuth2Registration> getoAuth2RegistrationList() {
        return oAuth2RegistrationList;
    }

    public void setoAuth2RegistrationList(List<OAuth2Registration> oAuth2RegistrationList) {
        this.oAuth2RegistrationList = oAuth2RegistrationList;
    }

    public List<String> getoAuth2RedirectionUris() {
        return oAuth2RedirectionUris;
    }

    public void setoAuth2RedirectionUris(List<String> oAuth2RedirectionUris) {
        this.oAuth2RedirectionUris = oAuth2RedirectionUris;
    }

    public String getDashboardPage() {
        return dashboardPage;
    }

    public void setDashboardPage(String dashboardPage) {
        this.dashboardPage = dashboardPage;
    }

    public String getErrorPage() {
        return errorPage;
    }

    public void setErrorPage(String errorPage) {
        this.errorPage = errorPage;
    }

    public ShopifyConfigurations getShopifyConfigurations() {
        return shopifyConfigurations;
    }

    public void setShopifyConfigurations(ShopifyConfigurations shopifyConfigurations) {
        this.shopifyConfigurations = shopifyConfigurations;
    }
}
