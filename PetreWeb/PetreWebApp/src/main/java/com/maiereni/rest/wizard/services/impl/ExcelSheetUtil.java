/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.services.impl;

import org.apache.poi.ss.usermodel.*;

import java.util.List;

/**
 * @author Petre Maierean
 */
class ExcelSheetUtil {
    private Sheet sheet;
    private CellStyle cs, cs2;
    private int rowNum = 0;

    ExcelSheetUtil(Workbook wb, String sheetName) {
        sheet = wb.createSheet(sheetName);
        cs = wb.createCellStyle();
        cs2 = wb.createCellStyle();
        DataFormat df = wb.createDataFormat();
        Font f = wb.createFont();
        Font f2 = wb.createFont();
        f.setFontHeightInPoints((short) 12);
        f.setColor( IndexedColors.BLACK.getIndex() );
        f.setBold(true );
        f2.setFontHeightInPoints((short) 10);
        f2.setColor( (short) Font.COLOR_RED );
        f2.setBold(true);
        f2.setStrikeout( true );
        cs.setFont(f);
        cs.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());

        cs2.setFont(f2);
    }

    public void addHeaders(List<String> headerContents) {
        Row row = sheet.createRow(rowNum++);
        int cellNum = 0;
        for(String headerContent : headerContents) {
            Cell cell = row.createCell(cellNum++);
            cell.setCellStyle(cs);
            cell.setCellValue(headerContent);
        }
    }

    public void addRow(List<String> cellContents) {
        Row row = sheet.createRow(rowNum++);
        int cellNum = 0;
        for(String cellContent : cellContents) {
            Cell cell = row.createCell(cellNum++);
            cell.setCellStyle(cs2);
            cell.setCellValue(cellContent);
        }
    }
}
