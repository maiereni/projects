/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.persistency.bo;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="CPU")
@NamedQueries({
    @NamedQuery(name=Processor.FIND_CPU_BY_ID, query=Processor.QUERY_CPU_BY_ID),
    @NamedQuery(name=Processor.FIND_CPU_BY_NAME_AND_FAMILY, query=Processor.QUERY_CPU_BY_NAME_AND_FAMILY),
    @NamedQuery(name=Processor.FIND_CPU_BY_FAMILY, query=Processor.QUERY_CPU_BY_FAMILY)
}
)
public class Processor implements Serializable {
    public static final String FIND_CPU_BY_ID = "Process.findAll";
    public static final String ID = "id";
    public static final String QUERY_CPU_BY_ID = "SELECT c FROM Processor c WHERE c.id=:" + ID;
    public static final String FIND_CPU_BY_NAME_AND_FAMILY = "Processor.findByNameAndFamily";
    public static final String NAME = "name";
    public static final String FAMILY = "familyId";
    public static final String QUERY_CPU_BY_NAME_AND_FAMILY = "SELECT c FROM Processor c WHERE (c.id=: " + ID + ") or (c.name=:" + NAME + " and c.familyId=:" + FAMILY + ")";
    public static final String FIND_CPU_BY_FAMILY = "Processor.findByFamily";
    public static final String QUERY_CPU_BY_FAMILY = "SELECT c FROM Processor c WHERE c.familyId=:" + FAMILY + " and c.available = true order by order asc";
    @Id
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String thumbnail;
    @Column(name="FAMILY_ID")
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID familyId;
    @Column(name="CUST_ID")
    private String custId;
    @Column(columnDefinition = "boolean default true")
    private Boolean available;
    @Column(name="ORDER_NUMBER")
    private Integer order;
    @Column
    private BigDecimal price;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public UUID getFamilyId() {
        return familyId;
    }

    public void setFamilyId(UUID familyId) {
        this.familyId = familyId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
