/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.services.impl;

import com.maiereni.rest.wizard.persistency.dao.SelectorDao;
import com.maiereni.rest.wizard.model.SelectionBean;
import com.maiereni.rest.wizard.model.Selector;
import com.maiereni.rest.wizard.model.SelectorImage;
import com.maiereni.rest.wizard.persistency.bo.Processor;
import com.maiereni.rest.wizard.persistency.bo.ProcessorFamily;
import com.maiereni.rest.wizard.persistency.bo.VideoCard;
import com.maiereni.rest.wizard.persistency.bo.VideoCardFamily;
import com.maiereni.rest.wizard.services.SelectionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Petre Maierean
 */
@Component
public class SelectionServiceImpl implements SelectionService {
    private static Logger logger = LogManager.getLogger(SelectionServiceImpl.class);
    @Autowired
    SelectorDao selectorDao;

    @Override
    public SelectionBean getSelections() throws Exception {
        logger.debug("Get the selections");
        SelectionBean ret = new SelectionBean();
        ret.setMessage("Good");
        ret.setBoards(new ArrayList<>());
        ret.setMemories(new ArrayList<>());
        ret.setProcessors(getCPUFamilies());
        Map<String, List<Selector>> all = getVideoCardFamilies();
        Map<String, Selector> videoCards = new HashMap<>();
        for(String key: all.keySet()) {
            List<Selector> selectors = all.get(key);

            for(Selector selector : selectors) {
                String newKey = key + ":" + selector.getName();
                videoCards.put(newKey, selector);
            }
        }
        ret.setVideoCards(videoCards);
        return ret;
    }

    @Override
    public List<Selector> getCPUFamilies() throws Exception {
        List<ProcessorFamily> families = selectorDao.findCpuFamilies();
        List<Selector> ret = new ArrayList<>();
        for(ProcessorFamily family: families) {
            Selector selector = new Selector();
            selector.setKey(family.getId().toString());
            selector.setName(family.getName());
            ret.add(selector);
        }
        return ret;
    }

    @Override
    public Map<String, List<Selector>> getVideoCardFamilies() throws Exception {
        List<VideoCardFamily> families = selectorDao.findVideoFamilies();
        Map<String, List<Selector>> ret = new LinkedHashMap<String, List<Selector>>();
        for(VideoCardFamily family: families) {
            List<VideoCard> videoCards = selectorDao.getVideoCards(family.getId().toString());
            List<Selector> selectors = new ArrayList<>();
            for(VideoCard videoCard: videoCards) {
                Selector selector = new Selector();
                selector.setKey(videoCard.getId().toString());
                selector.setName(videoCard.getName());
                selector.setDescription(videoCard.getDescription());
                selectors.add(selector);
            }
            ret.put(family.getName(), selectors);
        }
        return ret;
    }

    @Override
    public List<SelectorImage> getCPUs(String familyId) throws Exception {
        List<SelectorImage> ret = new ArrayList<>();
        if (StringUtils.isNotBlank(familyId)) {
            List<Processor> cpus = selectorDao.getCPUs(familyId);
            for(Processor cpu : cpus) {
                SelectorImage c = new SelectorImage();
                c.setName(cpu.getName());
                c.setKey(cpu.getId().toString());
                c.setImage(cpu.getThumbnail());
                c.setDescription(cpu.getDescription());
                ret.add(c);
            }
        }
        return ret;
    }

    @Override
    public List<SelectorImage> getVideoCards(String id) throws Exception {
        List<SelectorImage> ret = new ArrayList<>();
        List<VideoCard> cards = selectorDao.getSimilarVideoCards(id);
        for(VideoCard card: cards) {
            SelectorImage selector = new SelectorImage();
            selector.setKey(card.getId().toString());
            selector.setDescription(card.getDescription());
            selector.setImage(card.getThumbnail());
            selector.setName(card.getName());
            ret.add(selector);
        }
        return ret;
    }

}
