/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.services.impl;

import com.maiereni.rest.wizard.persistency.bo.ComputerPart;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Petre Maierean
 */
public class ExcelWriter implements Closeable {
    private static List<String> HEADERS = Arrays.asList(new String[] {"NAME", "DESCRIPTION", "CODE", "THUMBNAIL", "PRICE"});
    private static NumberFormat NF = NumberFormat.getInstance();
    private File f;
    private Workbook workbook;

    ExcelWriter(File f) {
        this.f = f;
        workbook = new HSSFWorkbook();
    }

    /**
     *
     * @param type
     * @param computerParts
     * @throws Exception
     */
    public void writer(String type, List<ComputerPart> computerParts) throws Exception {
        ExcelSheetUtil excelSheetUtil = new ExcelSheetUtil(workbook, type);
        excelSheetUtil.addHeaders(HEADERS);
        List<String> cellContents = new ArrayList<>();
        for (ComputerPart computerPart : computerParts) {
            cellContents.clear();
            cellContents.add(computerPart.getName());
            cellContents.add(computerPart.getDescription());
            cellContents.add(computerPart.getCode());
            cellContents.add(computerPart.getThumbnail());
            cellContents.add(NF.format(computerPart.getPrice()));
            excelSheetUtil.addRow(cellContents);
        }
    }

    @Override
    public void close() throws IOException {
        try (OutputStream os = new FileOutputStream(f)) {
            workbook.write(os);
        }
    }
}
