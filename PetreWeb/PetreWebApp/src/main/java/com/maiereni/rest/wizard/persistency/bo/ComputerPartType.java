/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.persistency.bo;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Petre Maierean
 */
public enum ComputerPartType {
    TOWER("tower"), CPU("cpu"), VIDEO_CARD("video"), RAM("ram"), HDD("hdd");

    private String type;

    ComputerPartType(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public static ComputerPartType fromString(String type) {
        ComputerPartType ret = null;
        if (StringUtils.isNotBlank(type)) {
            ret = getTypes().get(type);
        }
        return ret;
    }

    public static Map<String, ComputerPartType> getTypes() {
        Map<String, ComputerPartType> ret = new HashMap<>();
        ret.put(TOWER.type, TOWER);
        ret.put(CPU.type, CPU);
        ret.put(VIDEO_CARD.type, VIDEO_CARD);
        ret.put(RAM.type, RAM);
        ret.put(HDD.type, HDD);
        return null;
    }
}
