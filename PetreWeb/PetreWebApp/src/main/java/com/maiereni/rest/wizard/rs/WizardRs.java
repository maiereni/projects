/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.rs;

import com.maiereni.rest.wizard.model.Error;
import com.maiereni.rest.wizard.model.*;
import com.maiereni.rest.wizard.services.SelectionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Petre Maierean
 */
@Component
public class WizardRs extends WizardApi {
    private static Logger logger = LogManager.getLogger(WizardRs.class);
    @Autowired
    private SelectionService selectionService;

    @Override
    public Response apiBuildCompRequestQuotePost(QuoteRequest quoteRequest) {
        return Response.ok().entity("magic!").build();
    }

    @Override
    public Response getBuildComputersSelectors() {
        Response resp = null;
        try {
            SelectionBean selectionBean = selectionService.getSelections();
            resp = Response.ok(selectionBean).build();
        } catch (Exception e) {
            logger.error("Failed to retrieve the selections", e);
            Error error = new Error();
            error.setMessage(e.getMessage());
            error.setCode("error");
            resp = Response.ok(selectionService).build();
        }
        return resp;
    }

    @Override
    public Response getCPUs(String id) {
        Response resp = null;
        try {
            List<SelectorImage> cpus = selectionService.getCPUs(id);
            CPUsResponseBean responseBean = new CPUsResponseBean();
            responseBean.setCpus(cpus);
            resp = Response.ok(responseBean).build();
        } catch (Exception e) {
            logger.error("Failed to retrieve the selections", e);
            Error error = new Error();
            error.setMessage(e.getMessage());
            error.setCode("error");
            resp = Response.ok(selectionService).build();
        }
        return resp;
    }

    @Override
    public Response getVideoCards(String id) {
        Response resp = null;
        try {
            List<SelectorImage> videoCards = selectionService.getVideoCards(id);
            VideoCardResponseBean responseBean = new VideoCardResponseBean();
            responseBean.setCards(videoCards);
            resp = Response.ok(responseBean).build();
        } catch (Exception e) {
            logger.error("Failed to retrieve the selections", e);
            Error error = new Error();
            error.setMessage(e.getMessage());
            error.setCode("error");
            resp = Response.ok(selectionService).build();
        }
        return resp;
    }

    @Override
    public Response updatePrice(UpdateRequest updateRequest) {
        return Response.ok().entity("magic!").build();
    }
}
