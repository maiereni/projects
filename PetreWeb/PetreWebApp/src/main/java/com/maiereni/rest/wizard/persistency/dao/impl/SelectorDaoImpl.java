/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.persistency.dao.impl;

import com.maiereni.rest.wizard.persistency.dao.SelectorDao;
import com.maiereni.rest.wizard.persistency.bo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Repository
public class SelectorDaoImpl implements SelectorDao {
    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<ComputerPart> findComponents(String type) throws Exception {
        return dofindComponents(type);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<ProcessorFamily> findCpuFamilies() throws Exception {
        return doFindCpuFamilies();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<VideoCardFamily> findVideoFamilies() throws Exception {
        TypedQuery<VideoCardFamily> query = entityManager.createNamedQuery(VideoCardFamily.FIND_ACTIVE_FAMILIES, VideoCardFamily.class);
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateCpus(List<Processor> cpus) throws Exception {
        if (cpus != null) {
            for(Processor cpu: cpus) {
                Processor existing = findProcess(cpu);
                if (existing != null) {
                    existing.setOrder(cpu.getOrder());
                    existing.setAvailable(cpu.getAvailable());
                    existing.setDescription(cpu.getDescription());
                    existing.setThumbnail(cpu.getThumbnail());
                }
                else {
                    entityManager.persist(cpu);
                }
            }
            entityManager.flush();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateVideoCards(List<VideoCard> videoCards) throws Exception {
        if (videoCards != null) {
            for(VideoCard videoCard : videoCards) {
                VideoCard existing = findVideoCard(videoCard);
                if (existing != null) {
                    existing.setAvailable(videoCard.getAvailable());
                    existing.setDescription(videoCard.getDescription());
                    existing.setOrder(videoCard.getOrder());
                    existing.setThumbnail(videoCard.getThumbnail());
                }
                else {
                    entityManager.persist(videoCard);
                }
            }
            entityManager.flush();
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Processor> getCPUs(String familyId) throws Exception {
        TypedQuery<Processor> query = entityManager.createNamedQuery(Processor.FIND_CPU_BY_FAMILY, Processor.class);
        query.setParameter(Processor.FAMILY, UUID.fromString(familyId));
        return query.getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<VideoCard> getVideoCards(String familyId) throws Exception {
        return doGetVideoCards(UUID.fromString(familyId));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<VideoCard> getSimilarVideoCards(String id) throws Exception {
        TypedQuery<VideoCard> query = entityManager.createNamedQuery(VideoCard.FIND_BY_ID, VideoCard.class);
        query.setParameter(VideoCard.ID, UUID.fromString(id));
        List<VideoCard> r = query.getResultList();
        if (r.size() > 0) {
            return doGetVideoCards(r.get(0).getFamilyId());
        }
        return new ArrayList<>();
    }

    private List<VideoCard> doGetVideoCards(UUID familyId) throws Exception {
        TypedQuery<VideoCard> query = entityManager.createNamedQuery(VideoCard.FIND_BY_FAMILY_ID, VideoCard.class);
        query.setParameter(VideoCard.FAMILY, familyId);
        return query.getResultList();
    }

    private List<ComputerPart> dofindComponents(final String type) throws Exception {
        TypedQuery<ComputerPart> query = null;
        if (StringUtils.isNotBlank(type)) {
            query = entityManager.createNamedQuery(ComputerPart.FIND_BY_TYPE, ComputerPart.class);
            query.setParameter(ComputerPart.TYPE, type);
        }
        else {
            query = entityManager.createNamedQuery(ComputerPart.FIND_ALL, ComputerPart.class);
        }
        return query.getResultList();
    }

    private List<ProcessorFamily> doFindCpuFamilies() throws Exception {
        TypedQuery<ProcessorFamily> query = entityManager.createNamedQuery(ProcessorFamily.FIND_ALL, ProcessorFamily.class);
        return query.getResultList();
    }

    private Processor findProcess(final Processor c) {
        TypedQuery<Processor> query = entityManager.createNamedQuery(Processor.FIND_CPU_BY_NAME_AND_FAMILY, Processor.class);
        query.setParameter("id", c.getId() != null? c.getId(): UUID.randomUUID());
        query.setParameter("name", c.getName());
        query.setParameter("family", c.getFamilyId());
        Processor ret = null;
        try {
            List<Processor> r = query.getResultList();
            if (r.size() == 1) {
                ret = r.get(0);
            }
        }
        catch(NoResultException ex) {

        }
        return ret;
    }

    private VideoCard findVideoCard(final VideoCard v) {
        VideoCard ret = null;
        TypedQuery<VideoCard> query = entityManager.createNamedQuery(VideoCard.FIND_BY_NAME_AND_FAMILY, VideoCard.class);
        query.setParameter("id", v.getId() != null? v.getId() : UUID.randomUUID());
        query.setParameter("name", v.getName());
        query.setParameter("family", v.getFamilyId());
        try {
            List<VideoCard> r = query.getResultList();
            if (r.size() == 1) {
                ret = r.get(0);
            }
        }
        catch(NoResultException ex) {

        }
        return ret;
    }
}
