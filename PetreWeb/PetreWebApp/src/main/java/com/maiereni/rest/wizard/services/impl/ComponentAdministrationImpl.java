/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.services.impl;

import com.maiereni.rest.wizard.model.UploadBean;
import com.maiereni.rest.wizard.persistency.bo.ComputerPart;
import com.maiereni.rest.wizard.persistency.bo.ComputerPartType;
import com.maiereni.rest.wizard.persistency.dao.SelectorDao;
import com.maiereni.rest.wizard.services.ComponentAdministration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author Petre Maierean
 */
@Component
public class ComponentAdministrationImpl implements ComponentAdministration {
    private static Logger logger = LogManager.getLogger(ComponentAdministrationImpl.class);
    private static final Map<String, ComputerPartType> COMPUTER_PART_TYPE_MAP = ComputerPartType.getTypes();

    @Autowired
    SelectorDao selectorDao;

    /**
     * Download the components in a file
     *
     * @return
     * @throws Exception
     */
    @Override
    public File downloadComponents() throws Exception {
        File f = File.createTempFile("components", ".xlsx");
        try(ExcelWriter excelWriter = new ExcelWriter(f)) {
            for (String type : COMPUTER_PART_TYPE_MAP.keySet()) {
                List<ComputerPart> computerParts = selectorDao.findComponents(type);
                excelWriter.writer(type, computerParts);
            }
        }
        return f;
    }

    /**
     * Import components from a file
     *
     * @param buffer
     * @throws Exception
     */
    @Override
    public UploadBean uploadComponents(byte[] buffer) throws Exception {
        UploadBean ret = new UploadBean();

        ret.setCode("success");
        return ret;
    }
}
