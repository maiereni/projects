/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.persistency.bo;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="COMPONENTS")
@NamedQueries({
    @NamedQuery(name=ComputerPart.FIND_ALL, query=ComputerPart.QUERY_ALL_COMPONENT),
    @NamedQuery(name=ComputerPart.FIND_BY_TYPE, query=ComputerPart.QUERY_COMPONENT_BY_TYPE)
    }
)
public class ComputerPart implements Serializable {
    public static final String FIND_ALL = "ComputerPart.findAll";
    public static final String QUERY_ALL_COMPONENT = "SELECT c FROM ComputerPart c ORDER BY c.name DESC";
    public static final String FIND_BY_TYPE = "ComputerPart.findByType";
    public static final String TYPE = "type";
    public static final String QUERY_COMPONENT_BY_TYPE = "SELECT c FROM ComputerPart c WHERE c.type=:"+ TYPE +" ORDER BY c.name DESC";
    @Id
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column
    private String name;
    @Column(name="IDENTITY_ID")
    private Integer identityId;
    @Column
    private String description;
    @Column
    private String code;
    @Column
    private String thumbnail;
    @Column(name="COMPONENT_TYPE")
    private String type;
    @Column
    private BigDecimal price;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdentityId() {
        return identityId;
    }

    public void setIdentityId(Integer identityId) {
        this.identityId = identityId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
