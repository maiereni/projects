/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.persistency.bo;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="CPU_FAMILY")
@NamedQueries({
        @NamedQuery(name=ProcessorFamily.FIND_ALL, query=ProcessorFamily.QUERY_ACTIVE_CPU_FAMILIES)
    }
)
public class ProcessorFamily implements Serializable {
    public static final String FIND_ALL = "Processor.findAll";
    public static final String QUERY_ACTIVE_CPU_FAMILIES = "SELECT f FROM ProcessorFamily f where f.available = true order by f.orderNumber asc";
    @Id
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column
    private String name;
    @Column(columnDefinition = "boolean default true")
    private Boolean available;
    @Column(name="ORDER_NUMBER")
    private Integer orderNumber;

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name.trim();
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

}
