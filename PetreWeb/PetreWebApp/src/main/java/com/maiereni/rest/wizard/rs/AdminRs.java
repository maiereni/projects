/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.rs;

import com.maiereni.rest.wizard.model.UploadBean;
import com.maiereni.rest.wizard.services.ComponentAdministration;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.InputStream;

/**
 * @author Petre Maierean
 */
@Component
public class AdminRs extends AdminApi {
    private static Logger logger = LogManager.getLogger(AdminRs.class);

    @Autowired
    ComponentAdministration componentAdministration;

    @Override
    public Response download() {
        Response res = null;
        File f = null;
        try {
            f = componentAdministration.downloadComponents();
            Response.ResponseBuilder builder = Response.ok(f);
            builder.header("content-disposition", "attachment; filename=components.xlsx");
            res = builder.build();
        }
        catch(Exception e) {
            logger.error("Failed to download the content of the components", e);
            com.maiereni.rest.wizard.model.Error er = new com.maiereni.rest.wizard.model.Error();
            er.setCode("error");
            er.setMessage("Failure to download the content. " + e.getMessage());
            res = Response.status(404).entity(er).build();
        }
        finally {
            if (f != null) {
                if (!f.delete()) {
                    f.deleteOnExit();
                }
            }
        }
        return res;
    }

    @Override
    public Response upload(InputStream is) {
        UploadBean uploadBean = null;
        try {
            byte[] buffer = IOUtils.toByteArray(is);
            uploadBean = componentAdministration.uploadComponents(buffer);
        }
        catch(Exception e) {
            logger.error("Failed to upload the content of the components", e);
            uploadBean = new UploadBean();
            uploadBean.setCode("error");
            uploadBean.setMessage("Could not upload the content. " + e.getMessage());
        }
        return Response.ok().entity(uploadBean).build();
    }
}
