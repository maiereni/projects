/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.services.impl;

import com.maiereni.rest.wizard.model.SelectorImage;
import com.maiereni.rest.wizard.persistency.bo.ComputerPart;
import com.maiereni.rest.wizard.persistency.bo.Processor;
import com.maiereni.rest.wizard.persistency.bo.ProcessorFamily;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Petre Maierean
 */
class CpuTranslator {
    private List<ProcessorFamily> families;
    CpuTranslator(List<ProcessorFamily> families) {
        this.families = families;
    }

    /**
     * Get first level selectors
     * @return
     */
    public List<SelectorImage> getSelectors() {
        List<SelectorImage> ret = new ArrayList<>();
        for(ProcessorFamily family: families) {
            SelectorImage selector = new SelectorImage();
            selector.setKey(family.getId().toString());
            selector.setName(family.getName());
        }
        return ret;
    }

    /**
     * Convert from part to Cpu
     * @param part
     * @return
     */
    public Processor translate(final ComputerPart part) throws Exception{
        Processor ret = null;
        if (part != null) {
            String name = part.getName();
            if (name.startsWith("InTEL")) {
                name = name.replace("InTEL", "Intel");
            }
            ProcessorFamily cpuFamily = findFamily(name);
            if (cpuFamily != null) {
                ret = new Processor();
                ret.setId(part.getId());
                ret.setFamilyId(cpuFamily.getId());
                ret.setDescription(getDescription(cpuFamily, name));
                ret.setName(getName(cpuFamily, name));
                ret.setAvailable(true);
                ret.setOrder(0);
                ret.setCustId(part.getCode());
                ret.setThumbnail(part.getThumbnail());
            }
        }
        return ret;
    }

    private String getDescription(final ProcessorFamily family, final String name) {
        String n = getName(family, name);
        int i = name.indexOf(n) + n.length();
        String s = name.substring(0, i) + ":" + name.substring(i).trim();
        return StringEscapeUtils.escapeHtml3(s);
    }

    private String getName(final ProcessorFamily family, final String name) {
        String ret = name.substring(family.getName().length()).trim();
        int ix = ret.indexOf(" ");
        if (ix > 0) {
            ret = ret.substring(0, ix).trim();
            if (ret.startsWith("-")) {
                ret = ret.substring(1);
            }
        }
        return ret;
    }

    private ProcessorFamily findFamily(final String name) throws Exception {
        ProcessorFamily ret = null;
        for(ProcessorFamily family: families) {
            if (name.startsWith(family.getName())) {
                ret = family;
                break;
            }
        }
        return ret;
    }


}
