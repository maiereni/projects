/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.rest.wizard.services;

import com.maiereni.rest.wizard.model.SelectionBean;
import com.maiereni.rest.wizard.model.Selector;
import com.maiereni.rest.wizard.model.SelectorImage;

import java.util.List;
import java.util.Map;

/**
 * @author Petre Maierean
 */
public interface SelectionService {
    /**
     * Gets the selections for the form
     * @return
     * @throws Exception
     */
    SelectionBean getSelections() throws Exception;

    /**
     * Get the CPU Families
     * @return
     * @throws Exception
     */
    List<Selector> getCPUFamilies() throws Exception;

    /**
     * Get Video Card families
     * @return
     * @throws Exception
     */
    Map<String, List<Selector>> getVideoCardFamilies() throws Exception;

    /**
     * Find CPUs for a family id
     * @param familyId
     * @return
     * @throws Exception
     */
    List<SelectorImage> getCPUs(String familyId) throws Exception;

    /**
     * Find the Video cars associated with a family Id
     * @param familyId
     * @return
     * @throws Exception
     */
    List<SelectorImage> getVideoCards(String familyId) throws Exception;
}
