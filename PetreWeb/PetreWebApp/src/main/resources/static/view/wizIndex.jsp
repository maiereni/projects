<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Build a computer</title>
    <link rel="stylesheet" href="/css/design.css">
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
</head>
<body>
<section class="my-content">
    <div class="container pt-4">
        <build-your-computer config="/api/buildComp">Build your computer</build-your-computer>
        <script src="/wizard/main.js"></script>
    </div>
</section>

</body>
</html>
