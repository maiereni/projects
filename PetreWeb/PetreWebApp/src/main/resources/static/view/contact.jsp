<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Peter's server application</title>
</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>

    <section class="my-content" id="contact-us">
        Loading
    </section>

<jsp:include page="footer.jsp"></jsp:include>
<script src="https://www.google.com/recaptcha/api.js?render=<%= request.getAttribute("siteKey") %>"></script>
<script defer src="./js/contactus.js"></script>
</body>
</html>
