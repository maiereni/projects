<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Peter's server application</title>
    <link rel="stylesheet" href="css/design.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>

<section class="my-content">
    <div class="container pt-4">
        <p class="lead">The page is not available</p>
    </div>
</section>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
