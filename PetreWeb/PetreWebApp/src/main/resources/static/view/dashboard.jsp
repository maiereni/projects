<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SampleAngularApp</title>
    <base href="/">
    <link rel="stylesheet" href="css/design.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="nav.jsp"></jsp:include>
    <section class="my-content">
        <div class="container pt-4">
            <h1 class="cover-heading">Hello: ${ name }</h1>
            <p>I wander how it will will show</p>
        </div>
    </section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
