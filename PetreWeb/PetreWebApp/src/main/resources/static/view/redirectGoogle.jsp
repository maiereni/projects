<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.maiereni.petreweb.context.Settings" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.maiereni.petreweb.filter.csrf.LocalCsrfTokenRepository" %>
<%
    Settings settings = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext()).getBean(Settings.class);
%>
<!DOCTYPE html>
<html lang="en">
<body onload="document.forms[0].submit()">
    <form action="<%=settings.getGoogleAuthenticationUrl()%>" method="get">
        <input type="hidden" name="client_id" value="<%=settings.getGoogleOath2ClientId()%>"></input>
        <input type="hidden" name="redirect_uri" value="<%=(String) request.getAttribute("redirect_uri")%>"></input>
        <input type="hidden" name="response_type" value="code"></input>
        <input type="hidden" name="scope" value="https://www.googleapis.com/auth/userinfo.profile"></input>
        <input type="hidden" name="state" value="<%=(String) request.getAttribute("csrfValue")%>"></input>
        <input type="hidden" name="nonce" value="<%=(String) request.getAttribute(LocalCsrfTokenRepository.CSRF_TOKEN_NONCE)%>"></input>
        <input type="hidden" name="login_hint" value="jsmith@gmail.com"></input>
        <input type="hidden" name="hd" value="<%= request.getServerName() %>"></input>
        <input type="hidden" name="access_type" value="online"></input>
    </form>
</body>
</html>
