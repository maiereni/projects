<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1"><a class="nav-link" href="#">Wix Blogging</a></li>
            <li class="nav-item mx-0 mx-lg-1"><a class="nav-link" href="#">Cryptocurrencies</a></li>
            <li class="nav-item mx-0 mx-lg-1"><a class="nav-link" href="#">Stocks and investments</a></li>
<sec:authorize access="isAnonymous()">
            <li class="nav-item mx-0 mx-lg-1 dropdown" id="loginDropdown">
                 <a class="nav-link dropdown-toggle my-login" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Login</a>
                 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<c:if test="${ loginEndpoints != null }">
    <c:forEach items="${ loginEndpoints }" var="item">
                    <li><a class="dropdown-item" href="${ item.link }">${ item.title }</a></li>
    </c:forEach>
</c:if>
                 </ul>
            </li>
</sec:authorize>
<sec:authorize access="!isAnonymous()">
            <li class="nav-item mx-0 mx-lg-1"><a class="nav-item my-logout">Logout</a></li>
</sec:authorize>
        </ul>
    </div>
</nav>


<header class="masthead bg-primary text-white text-center">
    <div class="container">
        <div id="example">Server side applications</div>
    </div>
</header>
