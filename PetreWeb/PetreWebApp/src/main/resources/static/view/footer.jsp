<footer class="footer" id="mainFooter">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <p class="navbar-text">&#169; 2021 Maiereni Software and Consulting Inc</p>
            </div>
            <div class="col-6">
                <nav class="navbar navbar-expand-lg">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link" href="/index.html">Home</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link" href="/contact.html">Contact</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link" href="/privacy.html">Privacy Policy</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>
