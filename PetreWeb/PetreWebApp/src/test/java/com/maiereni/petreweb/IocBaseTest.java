/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.InputStream;

/**
 * @author Petre Maierean
 */
public class IocBaseTest {
    private ApplicationContext applicationContext;

    public IocBaseTest(String resource) {
        try {
            applicationContext = new ClassPathXmlApplicationContext(resource);
        }
        finally {
            Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownHook()));
        }
    }

    public <T> T getBean(Class<T> beanClass) {
        return applicationContext.getBean(beanClass);
    }

    public <T> T getBean(String name, Class<T> beanClass) {
        return applicationContext.getBean(name, beanClass);
    }

    private class ShutdownHook implements Runnable {
        @Override
        public void run() {
            try {
                String[] beanNames = applicationContext.getBeanNamesForType(ShutdownProcessor.class);
                for(String beanName : beanNames) {
                    ShutdownProcessor shutdownProcessor = applicationContext.getBean(beanName, ShutdownProcessor.class);
                    shutdownProcessor.preShutdown();
                }
            }
            catch (Exception e) {

            }
        }
    }
}
