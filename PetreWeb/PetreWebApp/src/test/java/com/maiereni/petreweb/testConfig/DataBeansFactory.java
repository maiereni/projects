/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.testConfig;

import com.maiereni.petreweb.ShutdownProcessor;
import com.maiereni.petreweb.persistency.DerbyProperties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Configuration
@EnableTransactionManagement
public class DataBeansFactory extends EncryptionFactory {
    private static final Logger logger = LogManager.getLogger(DataBeansFactory.class);

    @Bean("derbyPath")
    public File getDerbyPath() throws Exception {
        File ret = new File(FileUtils.getTempDirectoryPath(), UUID.randomUUID().toString());
        if (!ret.mkdirs()) {
            throw new Exception("Could not create temporary folder at " + ret.getPath());
        }
        logger.debug("The Derby database properties point to folder " + ret.getPath());
        return ret;
    }

    @Bean("props")
    public DerbyProperties getProperties(File derbyPath) throws Exception {
        DerbyProperties ret = new DerbyProperties();
        ret.setPath(derbyPath.getPath() + ";create=true");
        ret.setPassword("admin");
        ret.setUser("admin");
        return ret;
    }

    @Bean("dataSource")
    public DataSource getDatasource(DerbyProperties props) throws Exception {
        BasicDataSource ret = new BasicDataSource();
        ret.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        String url = "jdbc:derby:directory:" + props.getPath();
        ret.setUrl(url);
        ret.setUsername(props.getUser());
        ret.setPassword(props.getPassword());
        ret.setInitialSize(1);
        ret.setMaxIdle(60);
        ret.setMinIdle(60);
        logger.debug("The data source has been created");
        return ret;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) throws Exception {
        LocalContainerEntityManagerFactoryBean ret = new LocalContainerEntityManagerFactoryBean ();
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        ret.setPersistenceUnitName("entityManager");
        ret.setJpaVendorAdapter(vendorAdapter);
        ret.setPackagesToScan("com.maiereni.petreweb.persistency.bo");
        ret.setDataSource(dataSource);
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.DerbyTenFiveDialect");
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "true");
        ret.setJpaProperties(properties);
        logger.debug("The entity manager has been create");
        return ret;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        logger.debug("The transaction manager has been create");
        return transactionManager;
    }

    @Bean("removeTemporaryDerbyDirectory")
    public ShutdownProcessor getShutdownProcessor(File derbyPath) {
        return new ShutdownProcessor() {
            @Override
            public void preShutdown() {
                if (derbyPath.isDirectory()) {
                    try {
                        FileUtils.deleteDirectory(derbyPath);
                        logger.debug("The derby path has been removed");
                    }
                    catch (Exception e) {
                        logger.error("Failed to remove directory at " + derbyPath.getPath());
                    }
                }
            }
        };
    }
}
