/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.IocBaseTest;
import com.maiereni.petreweb.persistency.RoleDao;
import com.maiereni.petreweb.persistency.UserDao;
import com.maiereni.petreweb.persistency.UserRoleDao;
import com.maiereni.petreweb.persistency.bo.Role;
import com.maiereni.petreweb.persistency.bo.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Unit test class to validate the UserRoleDao
 * @author Petre Maierean
 */
public class TestUserRoleDao extends IocBaseTest {
    private static final Logger logger = LogManager.getLogger(TestUserRoleDao.class);
    private RoleDao roleDao;
    private UserDao userDao;
    private UserRoleDao userRoleDao;

    public TestUserRoleDao() {
        super("/test.xml");
    }

    @Before
    public void setUp() {
        userDao = getBean(UserDao.class);
        roleDao = getBean(RoleDao.class);
        userRoleDao = getBean(UserRoleDao.class);
    }

    @Test
    public void testAddUserRole() {
        final String userId = UUID.randomUUID().toString().replaceAll("-", "");
        final String roleName1 = UUID.randomUUID().toString().replaceAll("-", "");
        final String roleName2 = UUID.randomUUID().toString().replaceAll("-", "");
        try {
            User user = TestUserDao.createUserBean(userId, "test@test.com", "Test", "Test");
            userDao.save(user);
            assertNotNull(user.getId());
            Role role1 = TestRoleDao.createRoleBean(roleName1, "This is my test role1");
            roleDao.save(role1);
            assertNotNull(role1.getId());
            Role role2 = TestRoleDao.createRoleBean(roleName2, "This is my test role1");
            roleDao.save(role2);
            assertNotNull(role1.getId());
            assertTrue(!userRoleDao.isRole(userId, roleName1));
            assertTrue(!userRoleDao.isRole(userId, roleName2));
            userRoleDao.addUserRole(userId, roleName1);
            assertTrue(userRoleDao.isRole(userId, roleName1));
            assertTrue(!userRoleDao.isRole(userId, roleName2));
            userRoleDao.addUserRole(userId, roleName2);
            userRoleDao.removeRole(userId, roleName1);
            assertTrue(!userRoleDao.isRole(userId, roleName1));
            assertTrue(userRoleDao.isRole(userId, roleName2));
        }
        catch (Exception e) {
            logger.error("Failed to create user role", e);
            fail();
        }
    }
}
