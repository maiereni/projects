/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.filter.csrf;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Petre Maierean
 */
public class TestCustomCsrfRequestMatcher {
    private CustomCsrfRequestMatcher matcher = new CustomCsrfRequestMatcher();

    @Test
    public void testMatchRoot() {
        HttpServletRequest request = getRequest("GET", "/mvc/");
        assertTrue(!matcher.matches(request), "expected to match");
    }

    @Test
    public void testMatchIndex() {
        HttpServletRequest request = getRequest("GET", "/mvc/index.html");
        assertTrue(!matcher.matches(request), "expected to match");
    }

    @Test
    public void testNotMatch() {
        HttpServletRequest request = getRequest("GET", "/mvc/dash/index.html");
        assertTrue(matcher.matches(request), "expected to match");
    }


    private HttpServletRequest getRequest(String method, String url) {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn(url);
        Mockito.when(request.getMethod()).thenReturn(method);
        return request;
    }
}
