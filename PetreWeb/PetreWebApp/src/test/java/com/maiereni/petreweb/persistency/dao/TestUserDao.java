/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;
import com.maiereni.petreweb.IocBaseTest;
import com.maiereni.petreweb.persistency.UserDao;
import com.maiereni.petreweb.persistency.bo.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * @author Petre Maierean
 */
public class TestUserDao extends IocBaseTest {
    private static final Logger logger = LogManager.getLogger(TestUserDao.class);
    private UserDao userDao;

    public TestUserDao() {
        super("/test.xml");
    }

    @Before
    public void setUp() {
        userDao = getBean(UserDao.class);
    }

    @Test
    public void testAddSimpleUser() {
        final String userId = UUID.randomUUID().toString().replaceAll("-", "");
        final String email = "test@test.com";
        try {
            assertTrue(!userDao.isUser(userId));
            User user = createUserBean(userId, email, "Test", "Test");
            userDao.save(user);
            assertTrue(userDao.isUser(userId));
            User user1 = userDao.findUser(userId);
            assertNotNull("Expected to find user", user1);
            List<User> users = userDao.findUsers(email);
            assertTrue("Expected to find at least one such user", users.size() > 0);
            user1.setLastName("Simple");
            userDao.save(user1);
            User user2 = userDao.findUser(userId);
            assertEquals("", user1.getLastName(), user2.getLastName());
        }
        catch(Exception e) {
            logger.error("Failed to run the test", e);
            fail("Could not add user");
        }
    }

    @Test
    public void testNoUser() {
        final String userId = UUID.randomUUID().toString().replaceAll("-", "");
        try {
            assertTrue(!userDao.isUser(userId));
            assertNull(userDao.findUser(userId));
        }
        catch(Exception e) {
            logger.error("Failed to run the test", e);
            fail("Could not add user");
        }
    }

    protected static final User createUserBean(String userId, String email, String fName, String lName) {
        User user = new User();
        user.setUserId(userId);
        user.setCreationDate(Calendar.getInstance());
        user.setEmail(email);
        user.setFirstName(fName);
        user.setLastName(lName);
        return user;
    }
}
