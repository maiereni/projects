/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.testConfig;

import com.maiereni.petreweb.ShutdownProcessor;
import com.maiereni.utils.encryption.impl.SymetricEncryptionProviderImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.InputStream;

/**
 * @author Petre Maierean
 */
public class EncryptionFactory {

    @Bean("secretKeyFile")
    public File getSecretKey() throws Exception {
        try (InputStream is = getClass().getResourceAsStream("/secret.p12")) {
            if (is == null) {
                throw new Exception("Could not find the resource at /secret.p12");
            }
            File f = File.createTempFile("test", "p12");
            byte[] buffer = IOUtils.toByteArray(is);
            FileUtils.writeByteArrayToFile(f, buffer);
            return f;
        }
    }

    @Bean
    public SymetricEncryptionProviderImpl getEncryptionProvider(File secretKeyFile) throws Exception {
        return new SymetricEncryptionProviderImpl(secretKeyFile.getPath(), "changeit", "testMe", "testme");
    }

    @Bean("removeSecretKey")
    public ShutdownProcessor getShutdownProcessor(File secretKeyFile) {
        return new ShutdownProcessor() {
            @Override
            public void preShutdown() {
                if (secretKeyFile.exists())
                    if (!secretKeyFile.delete())
                        secretKeyFile.deleteOnExit();
            }
        };
    }
}
