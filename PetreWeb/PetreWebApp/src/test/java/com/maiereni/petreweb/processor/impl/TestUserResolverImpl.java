/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor.impl;

import com.maiereni.petreweb.IocBaseTest;
import com.maiereni.petreweb.filter.oauth2.bo.UserIdentity;
import com.maiereni.petreweb.persistency.bo.Role;
import com.maiereni.petreweb.persistency.bo.User;
import com.maiereni.petreweb.processor.UserResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;


/**
 * Unit test class for the UserResolver
 * @author Petre Maierean
 */
public class TestUserResolverImpl extends IocBaseTest {
    private static final Logger logger = LogManager.getLogger(TestUserResolverImpl.class);
    private UserResolver userResolver;
    public static final String USER_ID = UUID.randomUUID().toString().replaceAll("-", "");
    public static final String ROLE_ID = UUID.randomUUID().toString().replaceAll("-", "");
    public TestUserResolverImpl() {
        super("/test.xml");
    }

    @Before
    public void setUp() {
        userResolver = getBean(UserResolver.class);
    }

    @Test
    public void testProcessor() {
        try {
            UserIdentity userIdentity = createTestUser(USER_ID);
            assertTrue("Unexpected case", !userResolver.isUser(userIdentity));
            User user = userResolver.createUser(userIdentity, "test");
            assertNotNull("The user is not expected to be null", user);
            assertNotNull("The role of the user is not null", user.getUserRoles());
            List<String> roles = userResolver.getRoles(userIdentity);
            assertNotNull("The user has at least one role", roles);
            assertTrue("The user has at least one role", roles.size() == 1);
            assertEquals("Expected user role", Role.DEFAULT_USER, roles.get(0));
            userResolver.addRole(userIdentity, ROLE_ID);
            roles = userResolver.getRoles(userIdentity);
            assertTrue("The user has at least one role", roles.size() == 2);
        }
        catch (Exception e) {
            logger.error("Failed to test", e);
            fail("Failed to test");
        }
    }

    private UserIdentity createTestUser(String userId) {
        UserIdentity ret = new UserIdentity();
        ret.setUserId(userId);
        ret.setPictureUrl("https:/test.com/pic.png");
        ret.setName("test");
        ret.setGivenName("Test");
        ret.setFamilyName("Test");
        ret.setEmail("test@test.com");
        return ret;
    }
}
