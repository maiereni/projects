/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.processor.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import com.maiereni.petreweb.IocBaseTest;
import com.maiereni.petreweb.processor.bo.UserCookie;
import org.junit.Test;

import javax.servlet.http.Cookie;
import java.util.Date;
import org.junit.Before;
/**
 * Unit test class for the CookieUserResolverImpl class
 *
 * @author Petre Maierean
 */
public class TestCookieUserResolverImpl {
    private final static IocBaseTest base = new IocBaseTest("/test.xml");
    private CookieUserResolverImpl cookieUserResolver;

    @Before
    public void setUp() {
        cookieUserResolver = base.getBean(CookieUserResolverImpl.class);
    }

    @Test
    public void testGetCookie() {
        try {
            final String testUserName = "petre.test";
            UserCookie userCookie = new UserCookie();
            userCookie.setUserId(testUserName);
            userCookie.setCreationDate(new Date());
            Cookie cookie = cookieUserResolver.getSessionCookie(userCookie);
            assertNotNull(cookie);
            UserCookie uc = cookieUserResolver.getUser(cookie);
            assertNotNull(uc);
            assertEquals("Expected to match", testUserName, uc.getUserId());
        }
        catch(Exception e) {
            e.printStackTrace();
            fail("Failed to test");
        }
    }
    @Test
    public void testNegative() {
        try {
            assertNull(cookieUserResolver.getUser(null));
            assertNull(cookieUserResolver.getSessionCookie(null));
        }
        catch(Exception e) {
            e.printStackTrace();
            fail("Failed to test");
        }
    }
}
