/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.persistency.dao;

import com.maiereni.petreweb.IocBaseTest;
import com.maiereni.petreweb.persistency.RoleDao;
import com.maiereni.petreweb.persistency.bo.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.UUID;

import static org.junit.Assert.*;
/**
 * @author Petre Maierean
 */
public class TestRoleDao extends IocBaseTest {
    private static final Logger logger = LogManager.getLogger(TestRoleDao.class);
    private RoleDao roleDao;

    public TestRoleDao() {
        super("/test.xml");
    }

    @Before
    public void setUp() {
        roleDao = getBean(RoleDao.class);
    }

    @Test
    public void testRoleAdding() {
        final String name = UUID.randomUUID().toString().replaceAll("-", "");
        try {
            assertNull(roleDao.findRole(name));
            Role role = createRoleBean(name, "This is a very simple role");
            roleDao.save(role);
            assertNotNull(role.getId());
            Role role1 = roleDao.findRole(name);
            assertNotNull(role1);
            assertTrue(roleDao.isRole(name));
            role1.setDescription("Another changed description");
            roleDao.save(role1);
            Role role2 = roleDao.findRole(name);
            assertEquals(role1.getDescription(), role2.getDescription());
        }
        catch(Exception e) {
            logger.error("Failed to process", e);
            fail("Failed to process");
        }
    }

    protected static Role createRoleBean(String name, String description) {
        Role role = new Role();
        role.setName(name);
        role.setCreationDate(Calendar.getInstance());
        role.setDescription(description);
        return role;
    }
}
