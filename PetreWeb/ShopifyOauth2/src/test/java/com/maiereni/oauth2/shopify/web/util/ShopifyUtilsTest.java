/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.util;

import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;

/**
 * @author Petre Maierean
 */
public class ShopifyUtilsTest {
    private ShopifyUtils shopifyUtils = new ShopifyUtils();

    @Test
    public void testNullArguments() {
        assertTrue(!shopifyUtils.isShopifyAdmin(null));
    }

    @Test
    public void testWithNoReferrer() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        assertTrue(!shopifyUtils.isShopifyAdmin(request));
    }

    @Test
    public void testWithReferrerOther() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getHeader(ShopifyUtils.REFERER)).thenReturn("https://wwww.test.store/abra");
        assertTrue(!shopifyUtils.isShopifyAdmin(request));
    }

    @Test
    public void testWithReferrerAdmin() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getHeader(ShopifyUtils.REFERER)).thenReturn("https://123computerbuilder.myshopify.com/admin/apps/build-your-computer");
        assertTrue(shopifyUtils.isShopifyAdmin(request));
    }

    @Test
    public void testGetStoreNameWithNull() {
        assertNull(shopifyUtils.getStoreName(null));
    }

    @Test
    public void testGetStoreNameNoReferrer() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        assertNull(shopifyUtils.getStoreName(request));
    }

    @Test
    public void testGetStoreNameByParameter() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getParameter(ShopifyUtils.SHOP)).thenReturn("123computerbuilder.myshopify.com");
        assertEquals("123computerbuilder", shopifyUtils.getStoreName(request));
    }

    @Test
    public void testGetStoreNameByOtherParameter() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getParameter(ShopifyUtils.SHOP)).thenReturn("abc.test.com");
        assertNull(shopifyUtils.getStoreName(request));
    }

}
