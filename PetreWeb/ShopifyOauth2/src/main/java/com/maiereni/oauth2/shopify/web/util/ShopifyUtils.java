/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Petre Maierean
 */
public class ShopifyUtils {
    public static final String ADMIN_LINK_REGEX = "(https|http)\\:\\/\\/[a-zA-Z0-9][a-zA-Z0-9\\-]*\\.myshopify\\.com\\/.*";
    public static final String STORE_PARAM_REGEX = "[a-zA-Z0-9][a-zA-Z0-9\\-]*\\.myshopify\\.com";
    public static final String REFERER = "referer";
    public static final String SHOP = "shop";
    /**
     * Checks if the httpServletRequest is from a Shipfy store
     * @param httpServletRequest
     * @return
     */
    public boolean isShopifyAdmin(HttpServletRequest httpServletRequest) {
        boolean ret = false;
        if (httpServletRequest != null) {
            String referrer = httpServletRequest.getHeader(REFERER);
            if (StringUtils.isNotBlank(referrer)){
                ret = referrer.matches(ADMIN_LINK_REGEX);
            } else {
                String shop = httpServletRequest.getParameter(SHOP);
                if (StringUtils.isNotBlank(shop)) {
                    ret = shop.matches(STORE_PARAM_REGEX);
                }
            }
        }
        return ret;
    }

    /**
     * Get the Store name
     * @param httpServletRequest
     * @return
     */
    public String getStoreName(HttpServletRequest httpServletRequest) {
        String ret = null;
        if (isShopifyAdmin(httpServletRequest)) {
            String referrer = httpServletRequest.getHeader(REFERER);
            if (StringUtils.isNotBlank(referrer)) {
                int ix = referrer.indexOf("//");
                int iy = referrer.indexOf(".", ix + 2);
                ret = referrer.substring(ix + 2, iy);
            }
            else {
                String shop = httpServletRequest.getParameter(SHOP);
                if (StringUtils.isNotBlank(shop)) {
                    if (shop.matches(STORE_PARAM_REGEX)) {
                        int ix = shop.indexOf(".myshopify.com");
                        if (ix > 0) {
                            ret = shop.substring(0, ix);
                        }
                        else {
                            ret = shop;
                        }
                    }
                }
            }
        }
        return ret;
    }
}
