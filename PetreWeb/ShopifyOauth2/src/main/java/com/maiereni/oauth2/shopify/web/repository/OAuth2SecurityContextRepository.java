/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.repository;

import com.maiereni.oauth2.shopify.web.bo.UserProfile;
import com.maiereni.oauth2.shopify.web.util.ShopifyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;

/**
 * A security context repository implementation that provides a bridge between the OAuth2 Authorized Client Repository
 * and the Spring Security architecture
 *
 * @author Petre Maierean
 */
public class OAuth2SecurityContextRepository implements SecurityContextRepository {
    private static final Logger logger = LogManager.getLogger(OAuth2SecurityContextRepository.class);
    private OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository;
    private ShopifyUtils shopifyUtils = new ShopifyUtils();

    public OAuth2SecurityContextRepository(OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository) {
        this.oAuth2AuthorizedClientRepository = oAuth2AuthorizedClientRepository;
    }

    /**
     * Load the security context from the oAuth2AuthorizedClientRepository
     *
     * @param httpRequestResponseHolder
     * @return
     */
    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder httpRequestResponseHolder) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2AuthorizedClient client = oAuth2AuthorizedClientRepository.loadAuthorizedClient(
                "shopify",
                authentication,
                httpRequestResponseHolder.getRequest());
        SecurityContext ret = null;
        if (client == null) {
            ret = new SecurityContextImpl(getAnonymous()) ;
        }
        else {
            logger.debug("Load context for an authenticated user");
            UserProfile userProfile =
                    getUserProfile(httpRequestResponseHolder.getRequest(), client);
            ret = new SecurityContextImpl(userProfile);
        }
        return ret;
    }

    @Override
    public void saveContext(
            SecurityContext securityContext,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse) {
        logger.debug("Save context");

    }

    /**
     * Checks if the security context is valid
     * @param httpServletRequest
     * @return
     */
    @Override
    public boolean containsContext(HttpServletRequest httpServletRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2AuthorizedClient client = oAuth2AuthorizedClientRepository.loadAuthorizedClient(
                "shopify",
                authentication,
                httpServletRequest);

        return !(client == null || isExpired(client));
    }

    private UserProfile getUserProfile(HttpServletRequest request, OAuth2AuthorizedClient client) {
        UserProfile ret = new UserProfile();
        ret.setName(client.getPrincipalName());
        ret.getAuthorities().add(new SimpleGrantedAuthority(UserProfile.ANONYMOUS));
        if (!(client == null || isExpired(client))) {
            String storeName = shopifyUtils.getStoreName(request);
            ret.setId(storeName);
            ret.setoAuth2AuthorizedClient(client);
            ret.setAuthenticated(true);
            if (client.getPrincipalName().equals(UserProfile.STORE_ADMIN)) {
                ret.getAuthorities().add(new SimpleGrantedAuthority(UserProfile.STORE_ADMIN));
            }
            else {
                ret.getAuthorities().add(new SimpleGrantedAuthority(UserProfile.STORE_USER));
            }
        }
        return ret;
    }

    protected boolean isExpired(OAuth2AuthorizedClient client) {
        boolean ret = false;
        if (client.getAccessToken() != null) {
            if (client.getAccessToken().getExpiresAt() != null) {
                ret = client.getAccessToken().getExpiresAt().isAfter(Instant.now());
            }
        } else if (client.getRefreshToken() != null) {
            if (client.getRefreshToken().getExpiresAt() != null) {
                ret = client.getRefreshToken().getExpiresAt().isAfter(Instant.now());
            }
        }
        return ret;
    }

    private AnonymousAuthenticationToken getAnonymous() {
        return new AnonymousAuthenticationToken(UserProfile.ANONYMOUS,UserProfile.ANONYMOUS,
                AuthorityUtils.createAuthorityList("ROLE_CUSTOM"));
    }
}
