/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.bo;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Petre Maierean
 */
public class UserProfile implements Serializable, Authentication {
    public static final String ANONYMOUS = "anonymous";
    public static final String STORE_ADMIN = "storeAdmin";
    public static final String STORE_USER = "storeUser";
    private String name, id;
    private List<GrantedAuthority> authorities = new ArrayList<>();
    private User user;
    private boolean authenticated;
    private OAuth2AuthorizedClient oAuth2AuthorizedClient;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.user = new User(name, "", authorities);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }
    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities.clear();
        this.authorities.addAll(authorities);
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public Object getCredentials() {
        return user;
    }

    @Override
    public Object getDetails() {
        return id;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
        this.authenticated = authenticated;
    }

    public OAuth2AuthorizedClient getoAuth2AuthorizedClient() {
        return oAuth2AuthorizedClient;
    }

    public void setoAuth2AuthorizedClient(OAuth2AuthorizedClient oAuth2AuthorizedClient) {
        this.oAuth2AuthorizedClient = oAuth2AuthorizedClient;
    }
}
