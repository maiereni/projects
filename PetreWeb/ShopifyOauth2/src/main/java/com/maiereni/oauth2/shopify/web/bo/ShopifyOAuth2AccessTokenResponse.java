/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.bo;

import java.io.Serializable;

/**
 * @author Petre Maierean
 */
public class ShopifyOAuth2AccessTokenResponse implements Serializable {
    private String access_token, scope, expires_in, associated_user_scope;
    private AssociatedUser associated_user;

    public AssociatedUser getAssociated_user() {
        return associated_user;
    }

    public void setAssociated_user(AssociatedUser associated_user) {
        this.associated_user = associated_user;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getAssociated_user_scope() {
        return associated_user_scope;
    }

    public void setAssociated_user_scope(String associated_user_scope) {
        this.associated_user_scope = associated_user_scope;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
