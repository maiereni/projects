/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.oauth2.shopify.web.OAuth2InstalationRequestResolver;
import com.maiereni.oauth2.shopify.web.bo.ShopifyOAuth2AuthorizationToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * @author Petre Maierean
 */
public class OAuth2AuthorizedClientRepositoryImpl implements OAuth2AuthorizedClientRepository {
    private final Log logger = LogFactory.getLog(OAuth2AuthorizedClientRepositoryImpl.class);
    private static final String DEFAULT_AUTHORIZED_CLIENTS_ATTR_NAME = HttpSessionOAuth2AuthorizedClientRepository.class
            .getName() + ".AUTHORIZED_CLIENTS";

    private final String sessionAttributeName = DEFAULT_AUTHORIZED_CLIENTS_ATTR_NAME;
    private ClientRegistrationRepository clientRegistrationRepository;
    private String authHeaderName;
    private OAuth2ObjectsDao oAuth2ObjectsDao;
    private ObjectMapper objectMapper;

    public OAuth2AuthorizedClientRepositoryImpl(
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2ObjectsDao oAuth2ObjectsDao,
            ObjectMapper objectMapper,
            String authHeaderName) {
        this.clientRegistrationRepository = clientRegistrationRepository;
        this.oAuth2ObjectsDao = oAuth2ObjectsDao;
        this.objectMapper = objectMapper;
        this.authHeaderName = authHeaderName;
    }

    @Override
    public <T extends OAuth2AuthorizedClient> T loadAuthorizedClient(
            String clientRegistrationId,
            Authentication principal,
            HttpServletRequest request) {
        Assert.hasText(clientRegistrationId, "clientRegistrationId cannot be empty");
        Assert.notNull(request, "request cannot be null");
        T ret = (T) getAuthorizedClients(request).get(clientRegistrationId);
        if (ret == null) {
            String header = getSelector(request);
            logger.debug("The selector is " + header);
            if (StringUtils.isNotBlank(header)) {
                String s = oAuth2ObjectsDao.getOAuth2AuthorizedClient(header);
                if (StringUtils.isNotBlank(s)) {
                    try {
                        ShopifyOAuth2AuthorizationToken auth2AuthorizationToken =
                                objectMapper.readValue(s, ShopifyOAuth2AuthorizationToken.class);
                        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(
                                auth2AuthorizationToken.getClientRegistrationId());
                        OAuth2AuthorizedClient authorizedClient = new OAuth2AuthorizedClient(
                                clientRegistration,
                                auth2AuthorizationToken.getPrincipalName(),
                                auth2AuthorizationToken.getAccessToken(),
                                auth2AuthorizationToken.getRefreshToken());
                        ret = (T) authorizedClient;
                        logger.debug("Loaded the oauth2AuthorizedClient from the backend");
                        Map<String, OAuth2AuthorizedClient> authorizedClients = getAuthorizedClients(request);
                        authorizedClients.put(clientRegistrationId, authorizedClient);
                        request.getSession().setAttribute(this.sessionAttributeName, authorizedClients);
                    } catch (Exception e) {
                        logger.error("Could not read the OAuth2AuthorizationRequest from the string", e);
                    }
                }
            }
        }
        else {
            logger.debug("Found an authorization client from the session");
        }
        return ret;
    }

    protected String getSelector(HttpServletRequest httpServletRequest) {
        String ret = null;
        if (httpServletRequest != null) {
            ret = httpServletRequest.getParameter(OAuth2ParameterNames.CODE);
            if (StringUtils.isBlank(ret)) {
                ret = httpServletRequest.getHeader(authHeaderName);
                if (StringUtils.isBlank(ret)) {
                    ret = (String) httpServletRequest.getAttribute(authHeaderName);
                }
            }
        }
        return ret;
    }

    @Override
    public void saveAuthorizedClient(
            OAuth2AuthorizedClient authorizedClient,
            Authentication principal,
            HttpServletRequest request,
            HttpServletResponse response) {
        Assert.notNull(authorizedClient, "authorizedClient cannot be null");
        Assert.notNull(request, "request cannot be null");
        Assert.notNull(response, "response cannot be null");
        Map<String, OAuth2AuthorizedClient> authorizedClients = getAuthorizedClients(request);
        authorizedClients.put(authorizedClient.getClientRegistration().getRegistrationId(), authorizedClient);
        request.getSession().setAttribute(sessionAttributeName, authorizedClients);
        logger.debug("Save the authorizedClients object in the session");
        String shopName = request.getParameter(OAuth2InstalationRequestResolver.SHOP_ATTRIBUTE_NAME);
        String state = request.getParameter(OAuth2ParameterNames.STATE);
        ShopifyOAuth2AuthorizationToken auth2AuthorizationToken = convert(authorizedClient, principal, shopName);
        String header = request.getParameter(OAuth2ParameterNames.CODE);
        if (StringUtils.isBlank(header)) {
            header = UUID.randomUUID().toString();
        }
        response.setHeader(authHeaderName, header);
        request.setAttribute(authHeaderName, header);
        try {
            String content = getContent(authorizedClient, shopName);
            logger.debug("Save content for " + header);
            Calendar expiryDate = getExpiryDate(auth2AuthorizationToken);
            oAuth2ObjectsDao.setOAuth2AuthorizedClient(header, state, shopName, content, expiryDate);
        }
        catch(Exception e) {
            logger.error("Failed to persist", e);
        }
    }

    private String getContent(OAuth2AuthorizedClient authorizedClient, String shopName) throws Exception{
        ShopifyOAuth2AuthorizationToken shopifyOAuth2AuthorizationToken = new ShopifyOAuth2AuthorizationToken();
        shopifyOAuth2AuthorizationToken.setAccessToken(authorizedClient.getAccessToken());
        shopifyOAuth2AuthorizationToken.setRefreshToken(authorizedClient.getRefreshToken());
        shopifyOAuth2AuthorizationToken.setPrincipalName(authorizedClient.getPrincipalName());
        shopifyOAuth2AuthorizationToken.setClientRegistrationId(authorizedClient.getClientRegistration().getRegistrationId());
        shopifyOAuth2AuthorizationToken.setShopName(shopName);
        return objectMapper.writeValueAsString(shopifyOAuth2AuthorizationToken);
    }

    private Calendar getExpiryDate(ShopifyOAuth2AuthorizationToken auth2AuthorizationToken) {
        Calendar ret = null;
        if (auth2AuthorizationToken.getAccessToken() != null) {
            Instant instant = auth2AuthorizationToken.getAccessToken().getExpiresAt();
            if (instant != null) {
                ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
                ret = GregorianCalendar.from(zdt);
            }
        }
        else if (auth2AuthorizationToken.getRefreshToken() != null) {
            Instant instant = auth2AuthorizationToken.getRefreshToken().getExpiresAt();
            if (instant != null) {
                ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
                ret = GregorianCalendar.from(zdt);
            }
        }
        return ret;
    }

    private ShopifyOAuth2AuthorizationToken convert(
            OAuth2AuthorizedClient authorizedClient,
            Authentication principal,
            String shopName) {
        String clientRegistrationId = authorizedClient.getClientRegistration().getRegistrationId();
        ShopifyOAuth2AuthorizationToken ret = new ShopifyOAuth2AuthorizationToken();
        ret.setPrincipalName(principal != null? principal.getName() : "anonymousUser");
        ret.setShopName(shopName);
        ret.setClientRegistrationId(clientRegistrationId);
        ret.setAccessToken(authorizedClient.getAccessToken());
        ret.setRefreshToken(authorizedClient.getRefreshToken());
        return ret;
    }

    @Override
    public void removeAuthorizedClient(String clientRegistrationId, Authentication principal, HttpServletRequest request, HttpServletResponse response) {
        Assert.hasText(clientRegistrationId, "clientRegistrationId cannot be empty");
        Assert.notNull(request, "request cannot be null");
        Map<String, OAuth2AuthorizedClient> authorizedClients = this.getAuthorizedClients(request);
        if (!authorizedClients.isEmpty()) {
            if (authorizedClients.remove(clientRegistrationId) != null) {
                if (!authorizedClients.isEmpty()) {
                    request.getSession().setAttribute(this.sessionAttributeName, authorizedClients);
                }
                else {
                    request.getSession().removeAttribute(this.sessionAttributeName);
                }
            }
        }
    }


    private Map<String, OAuth2AuthorizedClient> getAuthorizedClients(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Map<String, OAuth2AuthorizedClient> authorizedClients = (session != null)
                ? (Map<String, OAuth2AuthorizedClient>) session.getAttribute(this.sessionAttributeName) : null;
        if (authorizedClients == null) {
            authorizedClients = new HashMap<>();
        }
        return authorizedClients;
    }
}
