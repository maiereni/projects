/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.oauth2.util.TokenUtil;
import com.maiereni.oauth2.util.repository.AbstractOAuth2AuthorizationRequestRepositoryImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

/**
 * @author Petre Maierean
 */
public class OAuth2AuthenticationRequestRepository extends AbstractOAuth2AuthorizationRequestRepositoryImpl {
    private final Log logger = LogFactory.getLog(OAuth2AuthenticationRequestRepository.class);
    private static final String SHOP = "shop";

    OAuth2ObjectsDao oAuth2ObjectsDao;
    ObjectMapper objectMapper;
    public OAuth2AuthenticationRequestRepository(OAuth2ObjectsDao oAuth2ObjectsDao, ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.oAuth2ObjectsDao = oAuth2ObjectsDao;
    }

    @Override
    public void setAuthorizationRequest(String state, String originalRequest, String type, OAuth2AuthorizationRequest request) {
        if (StringUtils.isNotBlank(state) && request != null) {
            try {
                String store = TokenUtil.getQueryToken(SHOP, originalRequest);
                logger.debug("Save authorization request with state " + state + " for store" + store);
                String s = objectMapper.writeValueAsString(request);
                oAuth2ObjectsDao.saveOrUpdate(state, store, s);
            }
            catch (Exception e) {
                logger.error("Failed to persist the authorization request", e);
            }
        }
    }

    @Override
    public OAuth2AuthorizationRequest getAuthorizationRequest(String state) {
        OAuth2AuthorizationRequest ret = null;
        Oauth2Object object = oAuth2ObjectsDao.findObject(state);
        if (object != null) {
            String s = object.getContent();
            try {
                ret = objectMapper.readValue(s, OAuth2AuthorizationRequest.class);
            }
            catch(Exception e) {
                logger.error("Could not read the OAuth2AuthorizationRequest from the string", e);
            }
        }
        return ret;
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(String state) {
        OAuth2AuthorizationRequest ret = getAuthorizationRequest(state);
        if (ret != null) {
            oAuth2ObjectsDao.deleteObject(state);
            logger.debug("The oauth2 authorization request for " + state + " has been removed");
        }
        return ret;
    }
}
