/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.intercept.RunAsManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import java.util.Collection;
import java.util.List;

/**
 * @author Petre Maierean
 */
public class RunAsManagerShopify implements RunAsManager {
    private static final Log logger = LogFactory.getLog(RunAsManagerShopify.class);

    @Override
    public Authentication buildRunAs(Authentication authentication, Object object, Collection<ConfigAttribute> attributes) {
        return authentication != null && authentication.isAuthenticated() ? authentication : null;
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return false;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    public static void setRunAsManager(ApplicationContext applicationContext) throws BeansException {
        try {
            FilterChainProxy filterChainProxy = applicationContext.getBean(FilterChainProxy.class);
            List<SecurityFilterChain> list = filterChainProxy.getFilterChains();
            list.stream()
                    .flatMap(chain -> chain.getFilters().stream())
                    .forEach(filter -> {
                        if (filter instanceof FilterSecurityInterceptor) {
                            FilterSecurityInterceptor filterSecurityInterceptor = (FilterSecurityInterceptor) filter;
                            RunAsManagerShopify runAsManager = new RunAsManagerShopify();
                            filterSecurityInterceptor.setRunAsManager(runAsManager);
                            logger.debug("The run as manager was set");
                        }
                    });
        }
        catch(Exception e) {
            logger.error("Could not configure the FilterSecurity interceptor", e);
        }
    }
}
