/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.filter;

import com.maiereni.oauth2.shopify.web.OAuth2InstalationRequestResolver;
import com.maiereni.oauth2.shopify.web.bo.UserProfile;
import com.maiereni.oauth2.shopify.web.repository.OAuth2SecurityContextRepository;
import com.maiereni.oauth2.shopify.web.util.ShopifyUtils;
import com.maiereni.oauth2.shopify.web.validation.ShopifyVerificationStrategyImpl;
import com.maiereni.oauth2.util.OAuth2AuthorizationRequestRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * A filter that processes the authorization response from Shopify
 * Described at https://shopify.dev/apps/auth/oauth#the-oauth-flow step 4
 *
 * @author Petre Maierean
 */
public class

ShopifyOAuth2AuthorizationCodeGrantFilter extends GenericFilterBean  {
    private static final Log logger = LogFactory.getLog(ShopifyOAuth2AuthorizationCodeGrantFilter.class);
    private ShopifyVerificationStrategyImpl shopifyVerificationStrategy;
    private OAuth2AuthorizationRequestRepository oauth2AuthorizationRequestRepository;
    private OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository;
    private AccessDeniedHandler accessDeniedHandler;
    private ShopifyOAuth2AccessTokenResponseClient shopifyOAuth2AccessTokenResponseClient;
    private ClientRegistration clientRegistration;
    private OAuth2SecurityContextRepository oAuth2SecurityContextRepository;
    private ShopifyUtils shopifyUtils = new ShopifyUtils();

    public ShopifyOAuth2AuthorizationCodeGrantFilter(
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthorizationRequestRepository oauth2AuthorizationRequestRepository,
            OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository,
            OAuth2SecurityContextRepository oAuth2SecurityContextRepository,
            AccessDeniedHandler accessDeniedHandler) throws Exception {
        this.oauth2AuthorizationRequestRepository = oauth2AuthorizationRequestRepository;
        this.oAuth2AuthorizedClientRepository = oAuth2AuthorizedClientRepository;
        this.accessDeniedHandler = accessDeniedHandler;
        this.shopifyVerificationStrategy = new ShopifyVerificationStrategyImpl(clientRegistrationRepository);
        this.clientRegistration =
                clientRegistrationRepository.findByRegistrationId("shopify");
        if (clientRegistration == null) {
            throw new Exception("The client " + "shopify");
        }
        this.shopifyOAuth2AccessTokenResponseClient = new ShopifyOAuth2AccessTokenResponseClient(clientRegistration);
        this.oAuth2SecurityContextRepository = oAuth2SecurityContextRepository;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpRequestResponseHolder httpRequestResponseHolder = new HttpRequestResponseHolder(req, resp);
        if(!matchesAuthorizationResponse(req)) {
            chain.doFilter(request, response);
        }
        else {
            try {
                String shopName = validateRequest(req);
                String code = req.getParameter(OAuth2ParameterNames.CODE);
                if (StringUtils.isBlank(code)) {
                    throw new AccessDeniedException("Missing code");
                }
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                OAuth2AuthorizedClient oauth2AuthorizedClient =
                        oAuth2AuthorizedClientRepository.loadAuthorizedClient(clientRegistration.getRegistrationId(), auth, req);
                if (oauth2AuthorizedClient == null) {
                    logger.debug("Try to resolve from the restful call with " + code);
                    String defaultPrincipalName = UserProfile.STORE_USER;
                    if (shopifyUtils.isShopifyAdmin(req)) {
                        defaultPrincipalName = UserProfile.STORE_ADMIN;
                    }
                    oauth2AuthorizedClient =
                            shopifyOAuth2AccessTokenResponseClient.getShopifyOAuth2AccessTokenResponse(code, shopName, defaultPrincipalName);
                }
                oAuth2AuthorizedClientRepository.saveAuthorizedClient(oauth2AuthorizedClient, auth, req, resp);
                logger.debug("Found the authorization client");
                if (oAuth2SecurityContextRepository.containsContext(req)) {
                    SecurityContextHolder.clearContext();
                    SecurityContext securityContext = oAuth2SecurityContextRepository.loadContext(httpRequestResponseHolder);
                    SecurityContextHolder.setContext(securityContext);
                    logger.debug("Create a new security context for the client");
                }
                chain.doFilter(request, response);
            } catch (AccessDeniedException e) {
                logger.error("Access deny", e);
            } catch (Exception e) {
                logger.error("Failed to contact the endpoint", e);
            }
        }
        return;
    }

    private String validateRequest(HttpServletRequest httpServletRequest) throws AccessDeniedException {
        String ret = null;
 /*       if (!shopifyVerificationStrategy.isShopifyRequest(httpServletRequest)) {
            throw new AccessDeniedException("This request must come from Shopify");
        }
        else {
        }
*/
        String shopName = httpServletRequest.getParameter(OAuth2InstalationRequestResolver.SHOP_ATTRIBUTE_NAME);
        String state = httpServletRequest.getParameter(OAuth2ParameterNames.STATE);
        if (StringUtils.isAnyBlank(shopName, state)) {
            throw new AccessDeniedException("This request must contain both host and the nonce");
        }
        OAuth2AuthorizationRequest oAuth2AuthorizationRequest =
                oauth2AuthorizationRequestRepository.getAuthorizationRequest(httpServletRequest);
        if (oAuth2AuthorizationRequest == null) {
            throw new AccessDeniedException("Cannot find the original request");
        }
        String actualShopName = (String)oAuth2AuthorizationRequest.getAdditionalParameters()
                .get(OAuth2InstalationRequestResolver.SHOP_ATTRIBUTE_NAME);
        if (!actualShopName.equals(shopName)) {
            throw new AccessDeniedException("Cannot find the original request");
        }
        ret = shopName;
        return ret;
    }

    private boolean matchesAuthorizationResponse(HttpServletRequest request) {
        MultiValueMap<String, String> params = OAuth2AuthorizationResponseUtils.toMultiMap(request.getParameterMap());
        if (!OAuth2AuthorizationResponseUtils.isAuthorizationResponse(params)) {
            return false;
        }
        OAuth2AuthorizationRequest authorizationRequest = oauth2AuthorizationRequestRepository
                .getAuthorizationRequest(request);
        if (authorizationRequest == null) {
            return false;
        }
        // Compare redirect_uri
        UriComponents requestUri = UriComponentsBuilder.fromUriString(UrlUtils.buildFullRequestUrl(request)).build();
        UriComponents redirectUri = UriComponentsBuilder.fromUriString(authorizationRequest.getRedirectUri()).build();
        Set<Map.Entry<String, List<String>>> requestUriParameters = new LinkedHashSet<>(
                requestUri.getQueryParams().entrySet());
        Set<Map.Entry<String, List<String>>> redirectUriParameters = new LinkedHashSet<>(
                redirectUri.getQueryParams().entrySet());
        // Remove the additional request parameters (if any) from the authorization
        // response (request)
        // before doing an exact comparison with the authorizationRequest.getRedirectUri()
        // parameters (if any)
        requestUriParameters.retainAll(redirectUriParameters);
        if (Objects.equals(requestUri.getUserInfo(), redirectUri.getUserInfo())
                && Objects.equals(requestUri.getHost(), redirectUri.getHost())
                && Objects.equals(requestUri.getPort(), redirectUri.getPort())
                && Objects.equals(requestUriParameters.toString(), redirectUriParameters.toString())) {
            return true;
        }
        return false;
    }

}
