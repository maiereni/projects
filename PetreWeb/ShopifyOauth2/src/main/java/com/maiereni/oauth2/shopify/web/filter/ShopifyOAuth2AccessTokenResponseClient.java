/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.filter;

import com.maiereni.oauth2.shopify.web.bo.ShopifyOAuth2AccessTokenResponse;
import com.maiereni.oauth2.util.client.BaseSSLClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;

import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class retrieves the Autentication
 *
 * @author Petre Maierean
 */
public class ShopifyOAuth2AccessTokenResponseClient extends BaseSSLClient {
    private static final Log logger = LogFactory.getLog(ShopifyOAuth2AccessTokenResponseClient.class);
    private static final String URL_TEMPLATE = "https://%s/admin/oauth/access_token";

    private ClientRegistration clientRegistration;
    public ShopifyOAuth2AccessTokenResponseClient(ClientRegistration clientRegistration)
            throws Exception {
        this.clientRegistration = clientRegistration;
    }

    public OAuth2AuthorizedClient getShopifyOAuth2AccessTokenResponse(String code, String shopName
            , String defaultPrincipalName)
            throws Exception {
        Map<String, String> props = new HashMap<>();
        props.put(OAuth2ParameterNames.CODE, code);
        String clientId = clientRegistration.getClientId();
        props.put(OAuth2ParameterNames.CLIENT_ID, clientId);
        String clientSecret = clientRegistration.getClientSecret();
        props.put(OAuth2ParameterNames.CLIENT_SECRET, clientSecret);
        String url = String.format(URL_TEMPLATE, shopName);
        logger.debug("Connect to shopify to get the access token from " + url + " for " + code);
        ShopifyOAuth2AccessTokenResponse res = postRequest(url, props,
                ParameterizedTypeReference.forType(ShopifyOAuth2AccessTokenResponse.class));
        logger.debug("The response has been received");
        String principalName = getPrincipalName(res, defaultPrincipalName);
        return new OAuth2AuthorizedClient(
                clientRegistration, principalName,  getAccessToken(res), null
        );
    }


    private OAuth2AccessToken getAccessToken(ShopifyOAuth2AccessTokenResponse response) {
        Instant instant = Instant.now();
        Instant expires = null;
        Set<String> scopes = null;
        if (StringUtils.isNotBlank(response.getExpires_in())) {
            expires = instant.plusSeconds(Long.parseLong(response.getExpires_in()));
        }
        if (StringUtils.isNotBlank(response.getScope())) {
            String[] toks = response.getScope().split(",");
            scopes = new HashSet<>();
            for(String tok: toks) {
                scopes.add(tok);
            }
        }
        return new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER,
                response.getAccess_token(), instant, expires, scopes);
    }

    private String getPrincipalName(ShopifyOAuth2AccessTokenResponse response, String defaultPrincipalName) {
        String ret = defaultPrincipalName;
        if (response.getAssociated_user() != null) {
            ret = response.getAccess_token();
        }

        return ret;
    }

}
