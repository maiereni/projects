/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web.validation;

import com.maiereni.oauth2.util.HmacUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Verified the origin of the request to ensure that it is from Shopify
 *
 * @author Petre Maierean
 */
public class ShopifyVerificationStrategyImpl implements ShopifyVerificationStrategy {
    private static final Log logger = LogFactory.getLog(ShopifyVerificationStrategyImpl.class);
    public static final String NONCE_PARAMETER = OAuth2ParameterNames.STATE;
    public static final String HMAC_PARAMETER = "hmac";
    public static final String HMAC_HEADER = "X-Shopify-Hmac-SHA256";
    public static final String SHOPIFY = "shopify";

    private ClientRegistrationRepository clientRegistrationRepository;

    public ShopifyVerificationStrategyImpl(
            ClientRegistrationRepository clientRegistrationRepository) {
        this.clientRegistrationRepository = clientRegistrationRepository;
    }

    /**
     * Validates that this is a valid request from Shopify
     * @param request
     * @return
     */
    @Override
    public boolean isShopifyRequest(HttpServletRequest request) {
        boolean ret = false;
        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(SHOPIFY);
        if (clientRegistration != null) {
            Map<String, String[]> params = request.getParameterMap();
            if (params != null && params.containsKey(HMAC_PARAMETER)) {
                String[] hmacs = params.get(HMAC_PARAMETER);
                if (hmacs.length == 1 && StringUtils.isNotBlank(hmacs[0])) {
                    String hmac = hmacs[0];
                    String processedQuery = getRequestQueryWithoutHmac(request, hmac);
                    if (StringUtils.isNotBlank(processedQuery)) {
                        String secret = clientRegistration.getClientSecret();
                        try {
                            String shaOfQuery = HmacUtil.getHmacSHA256(secret, processedQuery);
                            if (shaOfQuery.equals(hmac)) {
                                ret = true;
                            }
                            else {
                                logger.error("Could not verify the Hmac of the request");
                            }
                        }
                        catch (Exception e) {
                            logger.error("Failed to hash the string ", e);
                        }
                    }
                }
            }
        }
        return ret;
    }

    private String getRequestQueryWithoutHmac(HttpServletRequest request, String hmac) {
        String queryString = request.getQueryString();
        String hmacQueryStringPiece = HMAC_PARAMETER + "=" + hmac + "&";
        String ret = queryString.replaceFirst(Pattern.quote(hmacQueryStringPiece), "");
        if(ret.equals(queryString)) {
            ret = queryString.replaceFirst(Pattern.quote("&" + hmacQueryStringPiece), "");
            if(ret.equals(queryString)) {
                ret = null;
            }
        }
        return ret;
    }
}
