/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.shopify.web;

import com.maiereni.oauth2.shopify.web.validation.ShopifyVerificationStrategyImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.maiereni.oauth2.util.OAuth2AuthorizationRequestRepository;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Petre Maierean
 */
public class OAuth2InstalationRequestResolver extends DefaultRedirectStrategy implements OAuth2AuthorizationRequestResolver {
    private static final Log logger = LogFactory.getLog(OAuth2InstalationRequestResolver.class);
    public static final String REGISTRATION_ID = "registrationId";
    public static final String SHOP_ATTRIBUTE_NAME = "shop";
    public static final String REDIRECT = "REDIRECT";
    private static final String STATE = OAuth2ParameterNames.STATE;
    private static final String SCOPE = OAuth2ParameterNames.SCOPE;
    private static final String REDIRECT_URI = OAuth2ParameterNames.REDIRECT_URI;
    private static final String CLIENT_ID = OAuth2ParameterNames.CLIENT_ID;
    private static final String I_FRAME_AUTHENTICATION_URI_KEY = "iFrameString";
    private static final String I_FRAME_REDIRECT_URI = "/oauth/authorize";
    private static final String PARENT_AUTHENTICATION_URI_KEY = "parentString";

    private ClientRegistrationRepository clientRegistrationRepository;
    private AntPathRequestMatcher installPathRequestMatcher;
    private OAuth2AuthorizationRequestRepository oauth2AuthorizationRequestRepository;
    private ShopifyVerificationStrategyImpl shopifyVerificationStrategy;
    private final StringKeyGenerator stateGenerator = new Base64StringKeyGenerator(Base64.getUrlEncoder());
    private String loginUri;

    public OAuth2InstalationRequestResolver(
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthorizationRequestRepository oauth2AuthorizationRequestRepository,
            AntPathRequestMatcher installPathRequestMatcher,
            String loginUri) {
        this.clientRegistrationRepository = clientRegistrationRepository;
        this.oauth2AuthorizationRequestRepository = oauth2AuthorizationRequestRepository;
        this.installPathRequestMatcher = installPathRequestMatcher;
        this.loginUri = loginUri;
        this.shopifyVerificationStrategy = new ShopifyVerificationStrategyImpl(clientRegistrationRepository);
    }

    /**
     * Resolves an OAuth2AuthorizationRequest for shopify
     * @param request
     * @return
     */
    @Override
    public OAuth2AuthorizationRequest resolve(HttpServletRequest request) {
        return resolve(request, "shopify");
    }

    /**
     * Resolves an OAuth2AuthorizationRequest for shopify
     * @param request
     * @param registrationId
     * @return
     */
    @Override
    public OAuth2AuthorizationRequest resolve(HttpServletRequest request, String registrationId) {
        if(isAuthenticated(request) || !installPathRequestMatcher.matches(request)) {
            return null;
        }
        if(StringUtils.isBlank(registrationId)) {
            throw new IllegalArgumentException("Registration id is required");
        }
        OAuth2AuthorizationRequest ret = null;
        if (!shopifyVerificationStrategy.isShopifyRequest(request)) {
            logger.debug("Redirects to the login page");
            ret = redirectToLogin();
        }
        else {
            String shopName = request.getParameter(SHOP_ATTRIBUTE_NAME);
            if (StringUtils.isBlank(shopName)) {
                logger.debug("Redirects to the login page");
                ret = redirectToLogin();
            } else {
                ret = getRedirectionRequest(request, registrationId, shopName);
                saveRedirectAuthenticationUris(request, ret);
                oauth2AuthorizationRequestRepository.saveAuthorizationRequest(request, ret, "installation");
            }
        }
        return ret;
    }

    protected void saveRedirectAuthenticationUris(HttpServletRequest request, OAuth2AuthorizationRequest authorizationRequest) {
        String redirectUrl = calculateRedirectUrl(request.getContextPath(), authorizationRequest.getAuthorizationUri());
        redirectUrl = addRedirectParams(redirectUrl, authorizationRequest);

        String parentFrameRedirectUrl = calculateRedirectUrl(request.getContextPath(), authorizationRequest.getAuthorizationUri());
        String iFrameRedirectUri = addRedirectParams(I_FRAME_REDIRECT_URI, authorizationRequest);

        request.setAttribute(I_FRAME_AUTHENTICATION_URI_KEY, iFrameRedirectUri);
        request.setAttribute(REDIRECT_URI, redirectUrl);
        logger.debug("Redirect uri: " + redirectUrl);
        logger.debug("IFrame redirect uri: " + iFrameRedirectUri);
    }

    private String addRedirectParams(String uri, OAuth2AuthorizationRequest authorizationRequest) {
        LinkedMultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add(CLIENT_ID, authorizationRequest.getClientId());
        String esc = StringEscapeUtils.escapeHtml4(authorizationRequest.getRedirectUri());
        logger.debug("Escape: " + esc);
        queryParams.add(REDIRECT_URI, esc);
        queryParams.add(SCOPE, concatenateListIntoCommaString(new ArrayList<>(authorizationRequest.getScopes())));
        queryParams.add(STATE, authorizationRequest.getState());

        String re = UriComponentsBuilder
                .fromUriString(uri)
                .queryParams(queryParams)
                .build()
                .toString();

        return re;

    }

    protected String concatenateListIntoCommaString(List<String> pieces) {
        StringBuilder builder = new StringBuilder();

        if(pieces == null || pieces.size() < 1) {
            throw new RuntimeException("The provided List must contain at least one element");
        }
        pieces.stream()
                .forEach(e -> {
                    builder.append(e);
                    builder.append(",");
                });

        return builder.substring(0, builder.length() - 1);
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        if (SecurityContextHolder.getContext().getAuthentication() instanceof OAuth2AuthenticationToken) {
            return true;
        }
        return false;
    }

    protected OAuth2AuthorizationRequest redirectToLogin() {
        if(SecurityContextHolder.getContext().getAuthentication() != null) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }

        OAuth2AuthorizationRequest request = OAuth2AuthorizationRequest.implicit()
                .authorizationUri(REDIRECT)
                .authorizationRequestUri(loginUri) // the redirect uri
                .clientId(REDIRECT)
                .redirectUri(REDIRECT)
                .build();

        return request;


    }

    protected OAuth2AuthorizationRequest getRedirectionRequest(HttpServletRequest request, String registrationId, String shopName) {
        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(registrationId);
        if (clientRegistration == null) {
            throw new IllegalArgumentException("Invalid Client Registration: " + registrationId);
        }
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(OAuth2ParameterNames.REGISTRATION_ID, clientRegistration.getRegistrationId());

        // only the Authorization code grant is accepted
        OAuth2AuthorizationRequest.Builder builder;
        if (AuthorizationGrantType.AUTHORIZATION_CODE.equals(clientRegistration.getAuthorizationGrantType())) {
            builder = OAuth2AuthorizationRequest.authorizationCode();
        } else {
            throw new IllegalArgumentException("Invalid Authorization Grant Type ("  +
                    clientRegistration.getAuthorizationGrantType().getValue() +
                    ") for Client Registration: " + clientRegistration.getRegistrationId());
        }

        String redirectUriStr = expandRedirectUri(request, clientRegistration);

        if(logger.isDebugEnabled()) {
            logger.debug("Constructed the redirect uri string: " + redirectUriStr);
        }

        Map<String, Object> additionalParameters = new HashMap<>();
        additionalParameters.put(SHOP_ATTRIBUTE_NAME, shopName);
        String authorizationUriTemplate = clientRegistration.getProviderDetails().getAuthorizationUri();
        String state = stateGenerator.generateKey();
        return builder
                .clientId(clientRegistration.getClientId())
                .authorizationUri(generateAuthorizationUri(request, authorizationUriTemplate, shopName))
                .redirectUri(redirectUriStr)
                .scopes(clientRegistration.getScopes())
                .state(state)
                .attributes(attributes)
                .additionalParameters(additionalParameters)
                .build();
    }

    String expandRedirectUri(HttpServletRequest request, ClientRegistration clientRegistration) {
        // Supported URI variables -> baseUrl, registrationId
        // EX: "{baseUrl}/oauth2/code/{registrationId}"
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("registrationId", clientRegistration.getRegistrationId());
        String baseUrl = UriComponentsBuilder.fromHttpUrl(UrlUtils.buildFullRequestUrl(request))
                .replaceQuery(null)
                .replacePath(request.getContextPath())
                .build()
                .toUriString();
        uriVariables.put("baseUrl", baseUrl);

        return UriComponentsBuilder.fromUriString(clientRegistration.getRedirectUriTemplate())
                .buildAndExpand(uriVariables)
                .toUriString();
    }

    private String generateAuthorizationUri(HttpServletRequest request, String authorizationUriTemplate, String shopName) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put(SHOP_ATTRIBUTE_NAME, shopName);

        String authorizationUri = UriComponentsBuilder
                .fromHttpUrl(authorizationUriTemplate)
                .buildAndExpand(uriVariables)
                .toUriString();

        logger.debug("Generated authorization uri: " + authorizationUri);
        return authorizationUri;
    }
}
