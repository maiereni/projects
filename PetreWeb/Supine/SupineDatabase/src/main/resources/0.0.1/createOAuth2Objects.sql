DROP TABLE public.oauth2_objects;

CREATE TABLE public.oauth2_objects
(
    id uuid NOT NULL,
    key character varying(64) COLLATE pg_catalog."default" NOT NULL,
    type character varying(32) COLLATE pg_catalog."default" NOT NULL,
    content character varying(2048) COLLATE pg_catalog."default" NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    state character varying(64) COLLATE pg_catalog."default" NOT NULL,
    store character varying(128) COLLATE pg_catalog."default" NOT NULL,
    response_date timestamp without time zone,
    expiration_date timestamp without time zone,
    comments character varying(512) COLLATE pg_catalog."default",
    CONSTRAINT oauth2_objects_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.oauth2_objects
    OWNER to postgres;
