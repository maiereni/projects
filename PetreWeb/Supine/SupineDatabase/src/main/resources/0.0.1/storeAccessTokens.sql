CREATE TABLE public.store_access_tokens
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    storedomain character varying(50) COLLATE pg_catalog."default" NOT NULL,
    tokentype character varying(50) COLLATE pg_catalog."default" NOT NULL,
    tokenvalue character varying(100) COLLATE pg_catalog."default" NOT NULL,
    salt character varying(100) COLLATE pg_catalog."default" NOT NULL,
    issuedat bigint NOT NULL,
    expiresat bigint NOT NULL,
    scopes character varying(200) COLLATE pg_catalog."default" NOT NULL
)
TABLESPACE pg_default;

ALTER TABLE public.store_access_tokens
    OWNER to postgres;

