/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.db;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.util.StringUtil;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Petre Maierean
 */
public class SupineDatabaseUpdater {
    private static final Logger logger = LogManager.getLogger(SupineDatabaseUpdater.class);

    public static void main(String[] args) {
        try {
            Properties props = getConnectionProperties();
            DataSource dataSource = getDatasource(props);
            applyChanges(dataSource);
        }
        catch (Exception e) {
            logger.error("Failed to update", e);
        }
    }

    private static DataSource getDatasource(Properties props) throws Exception {
        logger.debug("Create data source");
        BasicDataSource ret = new BasicDataSource();
        ret.setDriverClassName(props.getProperty("spring.datasource.driver-class-name", "org.postgresql.Driver"));
        ret.setUrl(props.getProperty("spring.datasource.url"));
        ret.setUsername(props.getProperty("spring.datasource.username"));
        ret.setPassword(props.getProperty("spring.datasource.password"));
        ret.setInitialSize(1);
        ret.setMaxIdle(60);
        ret.setMinIdle(60);
        return ret;
    }

    private static void applyChanges(DataSource dataSource) throws Exception {
        logger.debug("Apply changes");
        try (java.sql.Connection connection = dataSource.getConnection();) {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            Liquibase liquibase = new Liquibase("/changelog.xml", new ClassLoaderResourceAccessor(), database);
            liquibase.update(new Contexts(), new LabelExpression());
        }
    }

    private static Properties getConnectionProperties() throws Exception {
        String propFile = System.getProperty("app.config");
        if (StringUtil.isEmpty(propFile)) {
            throw new Exception("Expected vm property 'app.config'");
        }
        Properties ret = new Properties();
        try (FileInputStream fis = new FileInputStream(propFile)) {
            ret.load(fis);
        }
        return ret;
    }
}
