<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Peter's server application</title>
    <link rel="stylesheet" href="../css/design.css">
    <link rel="stylesheet" href="/supine/njs/styles.css">
</head>
<body>
    <div id="spinner-border" class="spinner-border" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    <script>
        function open() {
            document.getElementById('spinner-border').hidden = true;
        }
    </script>
    <app-root>Loading ...</app-root>
    <script src="/supine/njs/runtime.js"></script>
    <script src="/supine/njs/polyfills.js"></script>
    <script src="/supine/njs/styles.js"></script>
    <script src="/supine/njs/vendor.js"></script>
    <script src="/supine/njs/main.js"></script>
</body>
</html>
