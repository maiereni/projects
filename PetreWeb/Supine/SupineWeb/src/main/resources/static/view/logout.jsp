<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Peter's server application</title>
    <link rel="stylesheet" href="../css/design.css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>

<section class="my-content">
    <div class="container pt-4">
        <h1 class="cover-heading">Logout Page</h1>
        <p class="lead">Please login</p>
    </div>
</section>

</body>
</html>
