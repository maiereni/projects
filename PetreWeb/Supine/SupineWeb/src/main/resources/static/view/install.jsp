<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%
    pageContext.setAttribute("redirect_uri", request.getAttribute("redirect_uri"));
    pageContext.setAttribute("iframe_redirect_uri", request.getAttribute("iFrameString"));
%>
<!DOCTYPE html>
<head lang="en">
    <meta charset="UTF-8">
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script>
    	var redirectUri = '${redirect_uri}';
    	var iFrameString = '${iframe_redirect_uri}';
    	// If the current window is the 'parent', change the URL by setting location.href parentRedirectUri
    	if (window.top === window.self) {
    		console.log("In parent: " + redirectUri);
    		if(redirectUri){
    			window.location.assign(redirectUri);
    		}
    	} else {
    		// If the current window is the 'child', change the parent's URL with ShopifyApp.redirect
    		console.log("In child: " + iFrameString);
    		// if there's no redirect, it's because the store has been installed, but it doesn't exist
    		// in the app database. Logging in from the browser will allow the app to store the token.
    		console.log("If you are seeing this, please log in directly from your browser, not the embedded app.")
    		// ShopifyApp.redirect(iFrameString);
            window.location.assign(redirectUri);
    	}
    </script>
    <title>TEST</title>
  </head>
  <body>
    <div>
        <sec:authorize access="!isAnonymous()">
            <p>This is the configuration page</p>
        </sec:authorize>
        <sec:authorize access="isAnonymous()">
            <p>There has been a problem logging in from the embedded app. Please log in directly from your browser</p>
        </sec:authorize>
    </div>
  </body>
</html>
