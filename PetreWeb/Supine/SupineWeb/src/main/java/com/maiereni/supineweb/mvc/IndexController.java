/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.mvc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Petre Maierean
 */
@Controller
public class IndexController {
    private final Log logger = LogFactory.getLog(IndexController.class);

    @GetMapping(path="/index.html")
    public String getIndex(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "index";
    }

    @GetMapping(path="/configuration.html")
    public String getConfiguration(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "configuration";
    }

    @GetMapping(path="/error")
    public String getError(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.debug("Error");
        return "error";
    }
    @GetMapping(path="/error.html")
    public String getError1(HttpServletRequest request, HttpServletResponse response, Model model) {
        return getError(request, response, model);
    }
}
