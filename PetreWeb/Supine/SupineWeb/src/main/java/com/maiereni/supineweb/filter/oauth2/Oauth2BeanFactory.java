/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.filter.oauth2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.oauth2.shopify.web.OAuth2AuthorizationEndpointCustomizer;
import com.maiereni.oauth2.shopify.web.OAuth2InstalationRequestResolver;
import com.maiereni.oauth2.shopify.web.repository.OAuth2AuthenticationRequestRepository;
import com.maiereni.oauth2.shopify.web.repository.OAuth2AuthorizedClientRepositoryImpl;
import com.maiereni.oauth2.shopify.web.repository.OAuth2ObjectsDao;
import com.maiereni.oauth2.shopify.web.repository.OAuth2SecurityContextRepository;
import com.maiereni.oauth2.util.ObjectMapperUtil;
import com.maiereni.oauth2.util.PropertyLoaderStrategy;
import com.maiereni.oauth2.util.repository.DefaultClientRegistrationRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.cache.Cache;
import javax.cache.CacheManager;

/**
 * @author Petre Maierean
 */
@Configuration
public class Oauth2BeanFactory {
    private static final Logger logger = LogManager.getLogger(Oauth2BeanFactory.class);
    public static final String CONFIGURATION_FILE_PATH_ARG = "oauth2.config.file";
    /**
     * Constructs an Object mapper for JSON to Object converstions
     * @return
     */
    @Bean
    public ObjectMapper getObjectMapper() {
        return ObjectMapperUtil.getObjectMapper();
    }

    /**
     * Get the configuration loader strategy
     * @return
     * @throws Exception
     */
    @Bean
    public PropertyLoaderStrategy getPropertyLoaderStrategy() throws Exception {
        String configFile = System.getProperty(CONFIGURATION_FILE_PATH_ARG);
        if (StringUtils.isBlank(configFile)) {
            throw new Exception("Expected command line argument " + CONFIGURATION_FILE_PATH_ARG);
        }
        logger.debug("Use the OAUTH2 configuration file at " + configFile);
        return new FilePropertyLoaderStrategyImpl(configFile);
    }

    /**
     * Constructs the ClientRegistrationRepository
     * @param objectMapper
     * @param cacheManager
     * @return
     * @throws Exception
     */
    @Bean
    public ClientRegistrationRepository getClientRegistrationRepository(
            ObjectMapper objectMapper,
            CacheManager cacheManager,
            PropertyLoaderStrategy propertyLoaderStrategy)
            throws Exception {
        Cache<String, ClientRegistration> clientRegistrationCache = cacheManager.getCache("clientRegistration", String.class, ClientRegistration.class);
        return new DefaultClientRegistrationRepository(clientRegistrationCache, objectMapper, propertyLoaderStrategy);
    }

    /**
     * Creates an Oauth2AuthenticationRequestRepository
     * @param clientRegistrationRepository
     * @param objectMapper
     * @param oAuth2ObjectsDao
     * @return
     */
    @Bean
    public OAuth2AuthenticationRequestRepository getOauth2AuthenticationRequestRepository(
            ClientRegistrationRepository clientRegistrationRepository,
            ObjectMapper objectMapper,
            OAuth2ObjectsDao oAuth2ObjectsDao) {
        return new OAuth2AuthenticationRequestRepository(oAuth2ObjectsDao, objectMapper);
    }

    /**
     *
     * @param objectMapper
     * @param oAuth2ObjectsDao
     * @return
     */
    @Bean
    public OAuth2AuthorizedClientRepositoryImpl getOAuth2AuthorizedClientRepositoryImpl(
            ClientRegistrationRepository clientRegistrationRepository,
            ObjectMapper objectMapper,
            OAuth2ObjectsDao oAuth2ObjectsDao) {
        return new OAuth2AuthorizedClientRepositoryImpl(
                clientRegistrationRepository,
                oAuth2ObjectsDao,
                objectMapper,
                "supine");
    }

    @Bean
    public OAuth2SecurityContextRepository getOAuth2SecurityContextRepository(
            OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository) {
        return new OAuth2SecurityContextRepository(oAuth2AuthorizedClientRepository);
    }

    /**
     * Constructor of the ShopifyOAuth2AuthorizationRequestResolver
     * @param clientRegistrationRepository
     * @param oauth2AuthenticationRequestRepository
     * @return
     * @throws Exception
     */
    @Bean
    public OAuth2InstalationRequestResolver getShopifyOauth2AuthorizationRequestResolver(
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthenticationRequestRepository oauth2AuthenticationRequestRepository)
            throws Exception {
        AntPathRequestMatcher matcher = new AntPathRequestMatcher("/**/shopify/install.html");
        return new OAuth2InstalationRequestResolver(
                clientRegistrationRepository,
                oauth2AuthenticationRequestRepository,
                matcher,
                "/supine/login.html");
    }

    @Bean
    public OAuth2AuthorizationEndpointCustomizer getOAuth2AuthorizationEndpointCustomizer(
            OAuth2AuthenticationRequestRepository oauth2AuthenticationRequestRepository,
            OAuth2InstalationRequestResolver oauth2InstalationRequestResolver) {
        return new OAuth2AuthorizationEndpointCustomizer(
                oauth2AuthenticationRequestRepository,
                oauth2InstalationRequestResolver,
                "https://mylocaldomain.com/supine/login");
    }

}
