/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb;

import com.maiereni.supineweb.rs.SupineApi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;
import java.util.logging.Level;

/**
 * @author Petre Maierean
 */
@Component
@ApplicationPath("api")
public class RestConfiguration extends ResourceConfig {
    private static final Logger logger = LogManager.getLogger(RestConfiguration.class);

    public RestConfiguration() {
        logger.debug("Create ResourceConfiguration");
        register(JacksonFeature.class);
        register(SupineApi.class);

        java.util.logging.Logger jLogger = java.util.logging.Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME);
        jLogger.setLevel(java.util.logging.Level.ALL);
        LoggingFeature logging = new LoggingFeature(jLogger, Level.FINEST, LoggingFeature.Verbosity.PAYLOAD_ANY, 1024*50);
        register(logging);
        property(LoggingFeature.LOGGING_FEATURE_VERBOSITY, LoggingFeature.Verbosity.PAYLOAD_ANY);
    }

}
