/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb;

import com.maiereni.oauth2.shopify.web.OAuth2AuthorizationEndpointCustomizer;
import com.maiereni.oauth2.shopify.web.filter.ShopifyOAuth2AuthorizationCodeGrantFilter;
import com.maiereni.oauth2.shopify.web.repository.OAuth2AuthenticationRequestRepository;
import com.maiereni.oauth2.shopify.web.repository.OAuth2SecurityContextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ChannelSecurityConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2ClientConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

/**
 * @author Petre Maierean
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true
)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    OAuth2AuthenticationRequestRepository oauth2AuthenticationRequestRepository;
    @Autowired
    OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository;
    @Autowired
    ClientRegistrationRepository clientRegistrationRepository;
    @Autowired
    OAuth2AuthorizationEndpointCustomizer oAuth2AuthorizationEndpointCustomizer;
    @Autowired
    OAuth2SecurityContextRepository oAuth2SecurityContextRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        ChannelSecurityConfigurer<HttpSecurity>.ChannelRequestMatcherRegistry registry = http.requiresChannel();
        registry.antMatchers("/**").requiresInsecure();
        http.securityContext().securityContextRepository(oAuth2SecurityContextRepository);
        LogoutConfigurer<HttpSecurity> logoutConfigurer =  http.logout();

        http.sessionManagement()
                .sessionFixation()
                .none();

        OAuth2LoginConfigurer<HttpSecurity> loginConfigurer =
                http.authorizeRequests()
                .antMatchers("/api/api/ping").permitAll()
                .antMatchers("/mvc/shopify/install.html").permitAll()
                .antMatchers("/mvc/shopify/login.html").permitAll()
                .antMatchers("/mvc/shopify/logout.html").permitAll()
                .antMatchers("/mvc/index.html").permitAll()
                .antMatchers("/mvc/error").permitAll()
                .anyRequest().authenticated().and()
                .oauth2Login();


        OAuth2ClientConfigurer<HttpSecurity> clientConfigurer = http.oauth2Client();
        loginConfigurer.authorizedClientRepository(oAuth2AuthorizedClientRepository);
        loginConfigurer.clientRegistrationRepository(clientRegistrationRepository);
        loginConfigurer.authorizationEndpoint(oAuth2AuthorizationEndpointCustomizer);
        loginConfigurer.loginProcessingUrl("https://mylocaldomain.com/supine/shopify/login.html");
        clientConfigurer.authorizedClientRepository(oAuth2AuthorizedClientRepository);

        http.addFilterBefore(getShopifyOAuth2AuthorizationCodeGrantFilter(), OAuth2AuthorizationRequestRedirectFilter.class);

        HeadersConfigurer<?> configurer = new HeadersConfigurer<>();
        configurer = http.getConfigurer(configurer.getClass());

        if (configurer == null) {
            throw new RuntimeException("HeadersConfigurer is required");
        }
        configurer.frameOptions().disable();

    }

    public ShopifyOAuth2AuthorizationCodeGrantFilter getShopifyOAuth2AuthorizationCodeGrantFilter()
            throws Exception {
        AccessDeniedHandlerImpl accessDeniedHandler = new AccessDeniedHandlerImpl();
        accessDeniedHandler.setErrorPage("/error.html");
        return new ShopifyOAuth2AuthorizationCodeGrantFilter(
                clientRegistrationRepository,
                oauth2AuthenticationRequestRepository,
                oAuth2AuthorizedClientRepository,
                oAuth2SecurityContextRepository,
                accessDeniedHandler);
    }

}
