/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.mvc;

import com.maiereni.oauth2.shopify.web.OAuth2InstalationRequestResolver;
import com.maiereni.oauth2.shopify.web.bo.UserProfile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Petre Maierean
 */
@Controller
@RequestMapping("shopify")
public class ShopifyController {
    private final Log logger = LogFactory.getLog(ShopifyController.class);

    @Autowired
    OAuth2InstalationRequestResolver shopifyOAuth2InstalationRequestResolver;

    @GetMapping(path="/install.html")
    public String getOauth2Initiation(HttpServletRequest request, HttpServletResponse response, Model model) {
        String ret = "install";

        OAuth2AuthorizationRequest oAuth2AuthorizationRequest =
                shopifyOAuth2InstalationRequestResolver.resolve(request, "shopify");
        if (oAuth2AuthorizationRequest == null) {
            ret = "configuration";
        }
        else if (oAuth2AuthorizationRequest.getAuthorizationUri().equals(OAuth2InstalationRequestResolver.REDIRECT)) {
            ret = login(request, response, model);
        }
        logger.debug("Shopify install");
        return ret;
    }

    @GetMapping(path="/login.html")
    public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
        String ret = "login";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            logger.debug("An authenticated call");
            return "configuration";
        }
        else {
            logger.debug("Login Shopify");
        }
        return ret;
    }

    @GetMapping(path="/logout.html")
    public String logout(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.debug("Logout Shopify");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            SecurityContextHolder.clearContext();
        }
        return "logout";
    }

    @GetMapping(path="/authorize.html")
    public String getAuthorizedPage1(HttpServletRequest request, HttpServletResponse response, Model model) {
        return getAuthorizedPage(request, response, model);
    }

    @GetMapping(path="/authorize")
    public String getAuthorizedPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        String ret = "user";
        try {
            UserProfile authentication = (UserProfile)
                    SecurityContextHolder.getContext().getAuthentication();
            if (authentication.getName().equals(UserProfile.STORE_ADMIN)) {
                ret = "configuration";
            }
        }
        catch (Exception e) {
            logger.error("Failed to detect the proper role", e);
        }
        logger.debug("The main page is for " + ret);
        return ret;
    }
}
