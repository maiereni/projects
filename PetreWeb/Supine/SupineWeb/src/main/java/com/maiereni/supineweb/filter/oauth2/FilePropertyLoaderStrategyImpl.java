/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.filter.oauth2;

import com.maiereni.oauth2.shopify.web.validation.ShopifyVerificationStrategyImpl;
import com.maiereni.oauth2.util.config.Oauth2Properties;
import com.maiereni.oauth2.util.repository.BasePropertyLoaderStrategy;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * An implementation of the PropertyLoaderStrategy which gets the information about Shopify client from a file
 *
 * @author Petre Maierean
 */
public class FilePropertyLoaderStrategyImpl extends BasePropertyLoaderStrategy {
    private static final Logger logger = LogManager.getLogger(FilePropertyLoaderStrategyImpl.class);
    public static final String CONFIGURATION_FILE_KEY = "oauth2.config.path";
    public static final String SHOPIFY = ShopifyVerificationStrategyImpl.SHOPIFY;
    private File configurationFile;

    public FilePropertyLoaderStrategyImpl() throws Exception {
        this(null);
    }

    public FilePropertyLoaderStrategyImpl(String sFileName) throws Exception {
        if (StringUtils.isBlank(sFileName)) {
            String s = System.getProperty(CONFIGURATION_FILE_KEY, "/opt/local/config/supineWeb/oauth2.properties");
            configurationFile = new File(s);
        }
        else {
            configurationFile = new File(sFileName);
        }
        if (!configurationFile.exists()) {
            generateBlankProperties(sFileName);
            throw new Exception("Could not find the configuration file at " + configurationFile.getPath() + " generated a blank one");
        }
        logger.debug("Configuration file at " + configurationFile.getPath());
    }

    @Override
    public Oauth2Properties getProperties(String registrationId) throws Exception {
        Properties props = new Properties();
        try(FileInputStream fis = new FileInputStream(configurationFile)) {
            if (fis == null) {
                throw new Exception("Cannot find the properties file at " + configurationFile.getPath());
            }
            props.load(fis);
        }
        return convert(props);
    }


    @Override
    public boolean isKnown(String registrationId) {
        return StringUtils.isNotBlank(registrationId) && registrationId.equals(SHOPIFY);
    }
}
