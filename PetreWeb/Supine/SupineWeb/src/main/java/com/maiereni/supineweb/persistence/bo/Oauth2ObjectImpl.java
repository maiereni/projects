/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.persistence.bo;

import com.maiereni.oauth2.shopify.web.repository.Oauth2Object;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Entity
@Table(name="oauth2_objects", schema = "public")
@NamedQueries({
  @NamedQuery(name= Oauth2ObjectImpl.FIND_OBJECT, query="select o from Oauth2ObjectImpl o where o.state = :state and o.type = :type"),
  @NamedQuery(name= Oauth2ObjectImpl.FIND_OBJECT_BY_KEY, query="select o from Oauth2ObjectImpl o where o.key = :key")
})
public class Oauth2ObjectImpl extends Oauth2Object implements Serializable {
    public static final String FIND_OBJECT = "Oauth2Object.find";
    public static final String FIND_OBJECT_BY_KEY = "Oauth2Object.findByKey";
    @Id
    @Type(type="pg-uuid")
    private UUID id;
    @Column(name="key")
    private String key;
    @Column(name="comments")
    private String comments;
    @Column(name="content")
    private String content;
    @Column(name="state")
    private String state;
    @Column(name="store")
    private String store;
    @Column(name="type")
    private String type;
    @Column(name="creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar creationDate;
    @Column(name="response_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar responseDate;
    @Column(name="expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar expirationDate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public Calendar getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Calendar responseDate) {
        this.responseDate = responseDate;
    }

    public Calendar getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }
}
