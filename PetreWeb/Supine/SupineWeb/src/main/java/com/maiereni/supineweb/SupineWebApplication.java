package com.maiereni.supineweb;

import com.maiereni.supineweb.bo.SupineSettings;
import org.apache.commons.dbcp2.BasicDataSourceFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.security.CodeSource;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@SpringBootApplication
@EnableCaching
//@ComponentScan(basePackages={"com.maiereni.supineweb"})
public class SupineWebApplication {
    private static final Logger logger = LogManager.getLogger(SupineWebApplication.class);

    @Bean
    public SupineSettings getSettings() throws Exception {
        SupineSettings ret = new SupineSettings();
        String tmpDir = Files.createTempDirectory("supine").toFile().getAbsolutePath();
        File fDoc = copyViewResources(getDir(tmpDir, "docs"));
        ret.setDocRoot(fDoc.getPath());
        ret.setBaseDir(getDir(tmpDir, "base").toString());
        return ret;
    }

    @Bean("datasource")
    public DataSource getDatasource(final DataSourceProperties dataSourceProperties) throws Exception {
        Properties props = new Properties();
        props.setProperty("driverClassName",dataSourceProperties.getDriverClassName());
        props.setProperty("url", dataSourceProperties.getUrl());
        props.setProperty("username", dataSourceProperties.getUsername());
        props.setProperty("password", dataSourceProperties.getPassword());
        DataSource ret = BasicDataSourceFactory.createDataSource(props);
        logger.debug("The datasource has been created ");
        return ret;
    }

    private static SupineSettings supineSettings;

    public static void main(String[] args) {
        logger.debug("Start application");
        ApplicationContext applicationContext = SpringApplication.run(SupineWebApplication.class, args);
        logger.debug("Application has been started");
        supineSettings = applicationContext.getBean(SupineSettings.class);
        Runtime.getRuntime().addShutdownHook(new Thread(new ShutdownCleanup()));
        // RunAsManagerShopify.setRunAsManager(applicationContext);
    }

    private static void listFilters(ApplicationContext applicationContext) {
        try {
            final StringBuffer sb = new StringBuffer();
            FilterChainProxy filterChainProxy = applicationContext.getBean(FilterChainProxy.class);
            List<SecurityFilterChain> list = filterChainProxy.getFilterChains();
            list.stream()
                    .flatMap(chain -> chain.getFilters().stream())
                    .forEach(filter -> sb.append(filter.getClass()).append("\r\n"));
            logger.debug("Filters:\r\n" + sb.toString());
        }
        catch (Exception e) {
            logger.error("Does not have a datasource configured", e);
        }
    }

    private File getDir(String tmpDir, String subpath) throws Exception {
        File fDir = new File(tmpDir, subpath);
        if (!fDir.mkdirs()) {
            throw new Exception("Could not create folder " + fDir.getPath());
        }
        return fDir;
    }

    private File copyViewResources(File fDocs) throws Exception {
        File ret = null;
        URI uri = SupineWebApplication.class.getResource("/static").toURI();
        if (uri.getScheme().equals("file")) {
            ret = new File(uri.toString().substring(6));
        }
        else if (uri.getScheme().equals("jar")) {
            int ix = uri.toString().indexOf("!");
            String path = uri.toString().substring(ix + 2).replace("!", "");
            logger.debug("Copy all content from " + path);
            copyViewResources(fDocs, path);
            ret = new File(fDocs, path);
        }
        return ret;
    }

    private void copyViewResources(File fDocs,String path) throws Exception {
        CodeSource src = SupineWebApplication.class.getProtectionDomain().getCodeSource();
        if (src != null) {
            try (InputStream is = src.getLocation().openStream();
                 ZipInputStream zip = new ZipInputStream(is);){
                while(true) {
                    ZipEntry e = zip.getNextEntry();
                    if (e == null)
                        break;
                    String name = e.getName();
                    if (name.startsWith(path)) {
                        File f = new File(fDocs, name);
                        if (e.isDirectory()) {
                            if (!f.isDirectory())
                                if (!f.mkdirs()) {
                                    throw new Exception("Cannot make directory");
                                }
                        }
                        else {
                            byte[] buffer = readCurrentEntry(zip);
                            FileUtils.writeByteArrayToFile(f, buffer);
                        }
                    }
                }
            }
        }
        else {
            logger.error("No source");
        }
    }

    private byte[] readCurrentEntry(ZipInputStream zip) throws Exception {
        return IOUtils.toByteArray(zip);
    }

    private String makeDirs(File fDir) throws Exception {
        if (!fDir.isDirectory())
            if (!fDir.mkdirs())
                throw new Exception("Cannot make directory at " + fDir.getPath());
        return fDir.getPath();
    }

    private static class ShutdownCleanup implements Runnable {
        @Override
        public void run() {
            if (supineSettings != null) {
                File fDir = new File(supineSettings.getBaseDir()).getParentFile();
                try {
                    logger.debug("Cleanup temporary folder");
                    FileUtils.deleteDirectory(fDir);
                }
                catch (Exception e) {
                    logger.error("Failed to clean up directory " + fDir.getPath(), e);
                }
            }
        }
    }
}
