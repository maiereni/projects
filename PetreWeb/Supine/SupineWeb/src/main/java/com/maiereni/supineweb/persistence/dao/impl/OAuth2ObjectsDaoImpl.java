/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.supineweb.persistence.dao.impl;

import com.maiereni.supineweb.persistence.bo.Oauth2ObjectImpl;
import com.maiereni.oauth2.shopify.web.repository.OAuth2ObjectsDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * @author Petre Maierean
 */
@Repository
public class OAuth2ObjectsDaoImpl implements OAuth2ObjectsDao {
    @PersistenceContext
    protected EntityManager entityManager;

    @Transactional
    @Override
    public String getOAuth2AuthorizedClient(String key) {
        String ret = null;
        if (StringUtils.isNotBlank(key)) {
            TypedQuery<Oauth2ObjectImpl> query =
                    entityManager.createNamedQuery(Oauth2ObjectImpl.FIND_OBJECT_BY_KEY, Oauth2ObjectImpl.class);
            query.setParameter("key", key);
            List<Oauth2ObjectImpl> res = query.getResultList();
            if (res.size() > 0) {
                ret = res.get(0).getContent();
            }
        }
        return ret;
    }

    @Transactional
    @Override
    public void setOAuth2AuthorizedClient(String selector, String state, String store, String content, Calendar expiryDate) {
        if (StringUtils.isNoneBlank(selector, content)) {
            Oauth2ObjectImpl o = new Oauth2ObjectImpl();
            o.setId(UUID.randomUUID());
            o.setComments("An authorization");
            o.setType("authorize");
            o.setStore(store);
            o.setKey(selector);
            o.setState(state);
            o.setExpirationDate(expiryDate);
            o.setContent(content);
            o.setCreationDate(Calendar.getInstance());
            entityManager.persist(o);
        }
    }

    @Transactional
    @Override
    public void deleteOAuth2AuthorizedClient(String selector) {
        if (StringUtils.isNotBlank(selector)) {
            Query q = entityManager.createQuery("DELETE FROM Oauth2ObjectImpl o where o.key = :key and o.type = 'authorize'");
            q.setParameter("key", selector);
            q.executeUpdate();
        }
    }

    @Transactional
    @Override
    public void saveOrUpdate(String state, String store, String content) {
        Oauth2ObjectImpl o = doFindObject(state, "install");
        if (o != null) {
            o.setContent(content);
            o.setStore(store);
            o.setResponseDate(Calendar.getInstance());
            entityManager.merge(o);
        }
        else {
            o = new Oauth2ObjectImpl();
            o.setId(UUID.randomUUID());
            o.setComments("");
            o.setKey(state);
            o.setType("install");
            o.setStore(store);
            o.setState(state);
            o.setContent(content);
            o.setCreationDate(Calendar.getInstance());
            entityManager.persist(o);
        }
    }

    @Transactional
    @Override
    public Oauth2ObjectImpl findObject(String state) {
        return doFindObject(state, "install");
    }

    private Oauth2ObjectImpl doFindObject(String key, String type) {
        TypedQuery<Oauth2ObjectImpl> query = entityManager.createNamedQuery(Oauth2ObjectImpl.FIND_OBJECT, Oauth2ObjectImpl.class);
        query.setParameter("state", key);
        query.setParameter("type", type);
        List<Oauth2ObjectImpl> res = query.getResultList();
        Oauth2ObjectImpl ret = null;
        if (res.size() > 0) {
            ret = res.get(0);
        }
        return ret;
    }

    @Transactional
    @Override
    public void deleteObject(String state) {
        if (StringUtils.isNotBlank(state)) {
            Query q = entityManager.createQuery("DELETE FROM Oauth2ObjectImpl o where o.state = :state and o.type = 'install'");
            q.setParameter("state", state);
            q.executeUpdate();
        }
    }
}
