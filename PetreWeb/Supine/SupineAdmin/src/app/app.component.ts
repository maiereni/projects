import {Component, OnInit} from '@angular/core';

declare var open: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'SupineAdmin';

  ngOnInit(): void {
    try {
      open();
    }
    catch(e) {

    }
  }

}
