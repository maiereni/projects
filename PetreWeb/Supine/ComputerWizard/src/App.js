import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import StepNav from './Elements/StepNav';
import Panels from './Elements/Panels';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step : "tab1",
      cpu : 'Intel',
      selCPU : '',
      selVideo : '',
      selMemory : '',
      selHDD : '',
      firstName : '',
      lastName : '',
      email : '',
      phone : '',
      processors: [],
      videoCards: [],
      others1: [],
      others2: []
    };
  }

  changeStep = (s) => {
    this.setState({
      step: s
    });
  }

  changeCPU = (s) => {
    this.setState({
      cpu: s
    });
  }

  changeSelectedCPU = (s) => {
    this.setState ( {
      selCPU: s
    });
  };

  changeSelectedVideo = (s) => {
    this.setState ( {
      selVideo: s
    });
  };

  changeSelectedMemory = (s) => {
    this.setState ({
      selMemory: s
    })
  };

  changeSelectedHDD = (s) => {
    this.setState( {
      selHDD: s
    });
  };

  setFirstName = (s) => {
    this.setState( {
      firstName: s
    })
  }

  setLastName = (s) => {
    this.setState( {
      lastName: s
    })
  }

  isNameError = (s) => {
    let r = true;
    if (/^[a-zA-Z]{0,10}$/.test(s)) {
      r = false;
    }
    return r;
  }

  setEmail = (s) => {
    this.setState( {
      email: s
    })
  }

  isEmailError = () => {
    let r = true;
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)) {
      r = false;
    }
    return r;
  }

  setPhone = (s) => {
    this.setState( {
      phone: s
    })
  }

  isPhomeError = () => {
    let r = true;
    if (/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(this.state.phone)) {
      r = false;
    }
    return r;
  }

  getProcessors = function() {
    let processors = [];
    this.state.processors.forEach( (p, ix) => {
      let name = p.name;
      if (name.toString().startsWith(this.state.cpu)) {
        let selected = false;
        if ((this.state.selCPU === '' && ix === 0) || (this.state.selCPU === p.key)) {
          selected = true;
        }
        let description = p.description;
        if (description === '') {
          description = p.name;
        }
        let props = {key: p.key, name: p.name, description: description, thumbnail: p.image, selected: selected};
        processors.push(props);
      }
    });
    return processors;
  }

  getVideoCards = function() {
    let cards = [];
    this.state.videoCards.forEach( (c, ix ) => {
      let selected = false;
      if ((this.state.selVideo === '' && ix === 0) || ( this.state.selVideo === c.key )) {
        selected = true;
      }
      let description = c.description;
      if (description === '') {
        description = c.name;
      }
      let props = {key: c.key, name: c.name, description: description, thumbnail: c.image, selected: selected};
      cards.push(props);
    })
    return cards;
  };

  render() {
    let canSubmit = !(this.isEmailError() || this.isPhomeError());
    return (
      <div className="container-fluid wzd2">
        <div className="row pb-5">
          <div className="col-12">
            <h1 className="wzd1">Build your computer</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <StepNav step={ this.state.step }/>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <Panels step={ this.state.step }
                    changeStep={ this.changeStep }
                    cpus={ this.getProcessors() }
                    cards={ this.getVideoCards() }
                    memories={ this.state.memories }
                    changeCPU={ this.changeCPU }
                    changeSelectedCPU = { this.changeSelectedCPU }
                    changeSelectedVideo = { this.changeSelectedVideo }
                    changeSelectedMemory = { this.changeSelectedMemory }
                    changeSelectedHDD = { this.changeSelectedHDD }
                    setFirstName = { this.setFirstName }
                    setLastName = { this.setLastName }
                    setEmail = { this.setEmail }
                    setPhone = { this.setPhone }
                    canSubmit = { canSubmit }
                    fnClass = { this.isNameError( this.state.firstName ) ? 'text-danger' : 'text-secondary' }
                    lnClass = { this.isNameError( this.state.lastName ) ? 'text-danger' : 'text-secondary' }
                    emClass = { this.isEmailError() ? 'text-danger' : 'text-secondary' }
                    pnClass = { this.isPhomeError() ? 'text-danger' : 'text-secondary' }
                    sitekey = { this.props.sitekey ? this.props.sitekey : 'unknown' }
            >
            </Panels>
          </div>
        </div>
      </div>
    );
  }

  // Here I do a server call
  componentDidMount() {
    console.log(this.props);
    const backendUrl = this.props.config + '/selectors';
    axios.get(backendUrl, {
      crossDomain: true
    }).then(r => {
      this.setState({
        processors: r.data.processors,
        videoCards: r.data.videoCards,
        memories: r.data.memories
      });
    }).catch(reason => {
      console.log('error: ' + reason);
    });
  }
}

export default App;
