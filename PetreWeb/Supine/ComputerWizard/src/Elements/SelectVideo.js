import React from 'react';

const selectVideo = (props) => {
  let nextStep = function() {
    props.nextStep("tab3");
  };
  let prevStep = function() {
    props.nextStep('tab1');
  };
  let cls = 'tab-pane fade';
  if (props.step === 'tab2') {
    cls = cls + ' show active';
  }
  let opts = props.cards.map( (p) => {
    return (
      <option value={ p.key } key={ p.key } defaultValue={ p.selected }>{ p.name }</option>
    )
  });

  let changeSelected = (e) => {
    let name = e.target.value;
    props.changeSelectedVideo(name);
  };

  let description = '';
  props.cards.forEach( p => {
    if (p.selected) {
      description = (
        <div className="description">
          <p>{ p.description }</p>
          <img src={ p.thumbnail } alt={ p.name }></img>
        </div>
      );
    }
  });

  return (
    <div className={ cls } id="v-pills-video" role="tabpanel" aria-labelledby="v-pills-video-tab">
      <div className="row pb-4 pt-4">
        <div className="col-10">
          <h2>Select a video card</h2>
        </div>
      </div>
      <div className="row pb-4">
        <div className="col-10">
          <select className="form-control" id="video" onChange={ changeSelected }>{ opts }</select>
        </div>
      </div>
      <div className="row pb-4">
        <div className="col-12">{ description }</div>
      </div>
      <div className="row">
        <div className="col-12 pt-4 float-right">
          <button type="button" className="btn btn-link" onClick={ prevStep }>Back</button>
          <button id="next2" className="offset-7 btn btn-primary" onClick={ nextStep }>Next</button>
        </div>
      </div>
    </div>
  );
};

export default selectVideo;
