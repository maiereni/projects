import React from 'react';

const selectCPU = (props) => {
  var nextStep = function() {
    props.nextStep("tab2");
  };
  let cls = 'tab-pane fade';
  if (props.step === 'tab1') {
    cls = cls + ' show active';
  }
  let proc = props.cpus.map( (p) => {
    return (
      <option value={ p.key } key={ p.key } defaultValue={ p.selected }>{ p.name }</option>
    )
  });

  let changeSelected = (e) => {
    let name = e.target.value;
    props.changeSelectedCPU(name);
  };

  let description = '';
  props.cpus.forEach( p => {
    if (p.selected) {
      description = (
        <div className="description">
          <p>{ p.description }</p>
          <img src={ p.thumbnail } alt={ p.name }></img>
        </div>
      );
    }
  });

  return (
    <div className={ cls } id="v-pills-cpu" role="tabpanel" aria-labelledby="v-pills-cpu-tab">
      <div className="row pb-4 pt-4">
        <div className="col-10">
          <h2>Select the CPU </h2>
        </div>
      </div>
      <div className="row pb-4">
        <div className="col-12">
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" name="cpu" id="cpu1" value="Intel" defaultChecked="true" onClick={ () => props.changeCPU('Intel') }/>
            <label className="form-check-label" htmlFor="cpu1">Intel&nbsp;</label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" name="cpu" id="cpu2" value="AMD" onClick={ () => props.changeCPU('AMD') }/>
            <label className="form-check-label" htmlFor="cpu2">AMD&nbsp;</label>
          </div>
        </div>
      </div>
      <div className="row pb-4">
        <div className="col-10">
          <div className="form-group">
            <select className="form-control" id="cpu3" onChange={ changeSelected }>{ proc }</select>
          </div>
        </div>
      </div>
      <div className="row pb-4">
        <div className="col-12">{ description }</div>
      </div>
      <div className="row">
        <div className="col-12 pt-4 float-right">
          <button id="next1" className="offset-9 btn btn-primary" onClick={nextStep}>Next</button>
        </div>
      </div>
    </div>
  );
};

export default selectCPU;
