import React from 'react';
import { Nav } from 'react-bootstrap';

const stepNav = (prop) => {
  return (
      <Nav variant="tabs">
        <Nav.Item>
          <Nav.Link eventKey="tab1" active={ prop.step === 'tab1' } onSelect={ null }>CPU</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="tab2" active={ prop.step === 'tab2' } onSelect={ null }>Video</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="tab3" active={ prop.step === 'tab3' } onSelect={ null }>Preferences</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="tab4" active={ prop.step === 'tab4' } onSelect={ null }>Quote</Nav.Link>
        </Nav.Item>
      </Nav>
  );
};

export default stepNav;
