import React, { Component } from 'react';
import SelectCPU from './SelectCPU'
import SelectVideo from './SelectVideo'
import OtherPrefs from './OtherPrefs'
import RequestQuot from './RequestQuote'

class Panels extends Component {
  render() {
    return (
      <div className="tab-content" id="v-pills-tabContent">
        <SelectCPU
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          cpus={ this.props.cpus }
          changeCPU={ this.props.changeCPU }
          changeSelectedCPU={ this.props.changeSelectedCPU }
        />
        <SelectVideo
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          cards={ this.props.cards }
          changeSelectedVideo = {this.props.changeSelectedVideo }
        />
        <OtherPrefs
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          others1={ this.props.others1 }
          changeSelectedMemory={ this.props.changeSelectedMemory }
          changeSelectedHDD = { this.props.changeSelectedHDD }
        />
        <RequestQuot
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          setFirstName = { this.props.setFirstName }
          setLastName = { this.props.setLastName }
          setEmail = { this.props.setEmail }
          setPhone = { this.props.setPhone }
          canSubmit = { this.props.canSubmit }
          fnClass = { this.props.fnClass }
          lnClass = { this.props.lnClass }
          emClass = { this.props.emClass }
          pnClass = { this.props.pnClass }
          sitekey = { this.props.sitekey }
        />
      </div>
    );
  }
}

export default Panels;
