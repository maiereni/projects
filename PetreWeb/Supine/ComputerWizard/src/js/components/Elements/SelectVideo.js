import React, { Component } from 'react';
import create_UUID from './Utils'

class SelectVideo extends Component {

  nextStep = () => {
    this.props.nextStep("tab3");
  }

  prevStep = () =>  {
    this.props.nextStep('tab1');
  }

  changeSelectedMaker = (e) => {
    let name = e.target.value;
    this.props.changeSelectedMaker(name);
  }

  changeSelectedGRU = (e) => {
    let name = e.target.value;
    this.props.changeSelectedVideo(name);
  }

  changeSelectedVideoCard = (e) => {
    let name = e.target.value;
    this.props.changeSelectedVideoCard(name);
  }

  render() {
    let cls = 'tab-pane fade';
    if (this.props.step === 'tab2') {
      cls = cls + ' show active';
    }

    let opts3 = this.props.videoCards.map( v => <option value={ v.key } key={ v.key } defaultValue={ v.selected }>{ v.name }</option> );
    let opts2 = '';
    this.props.cards.forEach((c) => {
        if (c.selected) {
          opts2 = c.videoCards.map( (p, ix) => {
            let selected = false;
            if (ix === 1) {
              selected = true;
            }
            return <option value={ p.key } key={ p.key } defaultValue={ selected }>{p.name}</option>;
          });
        }
      }
    );
    let opts = this.props.cards.map( (p) => {
      if (p.selected) {
        let selected = true;
        return <option value={ p.key } key={ p.key } defaultValue={ selected }>{ p.name }</option>
      }
      else {
        return <option value={ p.key } key={ p.key }>{ p.name }</option>
      }
    });
    let description = '', img = '', title = '';
    this.props.videoCards.forEach( p => {
      if (p.selected) {
        img = p.thumbnail;
        title = p.description;
        let ar = p.description.split(':');
        let n = 0;
        description = ar.map( (item, ix) => {
          n = n + 1;
          let u = p.key + '-' + n;
          if (n == 1) {
            return <h2 key={u}>{ item }</h2>;
          }
          else {
            return <p key={u}>{ item }</p>;
          }
        });
      }
    });
    return (
      <div className={ cls } id="v-pills-video" role="tabpanel" aria-labelledby="v-pills-video-tab">
        <div className="row pb-4 panel-body">
          <div className="col-1">&nbsp;</div>
          <div className="col-10">
            <div className="row pb-4 pt-8">
              <div className="col-12 form-group">
                <label htmlFor="maker">Select a Maker</label>
                <select className="form-control" id="maker" onChange={ this.changeSelectedMaker }>{ opts }</select>
              </div>
            </div>
            <div className="row pb-4 pt-8">
              <div className="col-12 form-group">
                <label htmlFor="videoCard">Select a Video Card: </label>
                <select className="form-control" id="videoCard" onChange={ this.changeSelectedVideoCard }>{ opts3 }</select>
              </div>
            </div>
            <div className="row pb-4">
              <div className="col-12 sel-desc">{ description }</div>
            </div>
          </div>
          <div className="col-1">&nbsp;</div>
        </div>
        <div className="row">
          <div className="col-12 pt-4">
            <button type="button" className="btn btn-link ml-2" onClick={ this.prevStep }>Back</button>
            <button id="next2" className="btn btn-primary float-right mr-4" onClick={ this.nextStep }>Next</button>
          </div>
        </div>
      </div>
    );
  }
};

export default SelectVideo;
