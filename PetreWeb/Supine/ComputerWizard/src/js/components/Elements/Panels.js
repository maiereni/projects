import React, { Component } from 'react';
import SelectCPU from './SelectCPU'
import SelectVideo from './SelectVideo'
import OtherPrefs from './OtherPrefs'
import RequestQuot from './RequestQuote'

class Panels extends Component {
  render() {
    return (
      <div className="tab-content" id="v-pills-tabContent">
        <SelectCPU
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          cpuFamilies={ this.props.cpuFamilies }
          cpus={ this.props.cpus }
          changeCPU={ this.props.changeCPU }
          changeSelectedFamily={ this.props.changeSelectedFamily }
          changeSelectedCPU={ this.props.changeSelectedCPU }
        />
        <SelectVideo
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          cards={ this.props.cards }
          videoCards={ this.props.videoCards }
          changeSelectedMaker = { this.props.changeSelectedMaker }
          changeSelectedVideo = { this.props.changeSelectedVideo }
          changeSelectedVideoCard = { this.props.changeSelectedVideoCard }
        />
        <OtherPrefs
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          others1={ this.props.others1 }
          changeSelectedMemory={ this.props.changeSelectedMemory }
          changeSelectedHDD = { this.props.changeSelectedHDD }
        />
        <RequestQuot
          step={ this.props.step }
          nextStep={ this.props.changeStep }
          setFirstName = { this.props.setFirstName }
          setLastName = { this.props.setLastName }
          setEmail = { this.props.setEmail }
          setPhone = { this.props.setPhone }
          canSubmit = { this.props.canSubmit }
          fnClass = { this.props.fnClass }
          lnClass = { this.props.lnClass }
          emClass = { this.props.emClass }
          pnClass = { this.props.pnClass }
          cmtClass = { this.props.cmtClass }
          sitekey = { this.props.sitekey }
          setComment = { this.props.setComment }
          comments = { this.props.comments }
          doSubmit = { this.props.doSubmit }
          isSubmitting = { this.props.isSubmitting }
        />
      </div>
    );
  }
}

export default Panels;
