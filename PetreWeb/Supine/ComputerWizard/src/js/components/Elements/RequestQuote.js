import React, { Component } from 'react';
import { ReCaptcha } from 'react-recaptcha-v3'

class RequestQuote extends Component  {
  prevStep = () => {
    this.props.nextStep('tab3');
  };
  changeFN = (e) => {
    let fn = e.target.value;
    this.props.setFirstName( fn );
  };
  changeLN = (e) => {
    let ln = e.target.value;
    this.props.setLastName( ln );
  };
  changeEmail = (e) => {
    let email = e.target.value;
    this.props.setEmail(email);
  };
  changePhone = (e) => {
    let phone = e.target.value;
    this.props.setPhone(phone);
  };

  changeText = (e) => {
    let text = e.target.value;
    this.props.setComment(text);
  };

  doSubmit = (e) => {
    if (this.props.canSubmit) {
      this.props.doSubmit();
    }
  }

  verifyCallback = (recaptchaToken) => {
    // Here you will get the final recaptchaToken!!!
    console.log(recaptchaToken, "<= your recaptcha token")
  }

  updateToken = () => {
    // you will get a new token in verifyCallback
    this.recaptcha.execute();
  }

  render() {
    let cls = 'tab-pane fade';
    if (this.props.step === 'tab4') {
      cls = cls + ' show active';
    }
    let textChars = "Up to 250 characters";
    if (this.props.comments !== '') {
      let comments = this.props.comments;
      textChars = "Typed in " + comments.length + " of 250 characters";
    }
    let buttonText = 'Submit';
    if (this.props.isSubmitting) {
      buttonText = 'Submitting';
    }
    return (
      <div className={ cls } id="v-pills-submit" role="tabpanel" aria-labelledby="v-pills-submit-tab">
        <div className="row pb-4 panel-body">
          <div className="col-1">&nbsp;</div>
          <div className="col-10">
            <div className="row">
              <div className="col-5">
                <label htmlFor="fname" className={ this.props.fnClass }>First Name</label>
                <input className="form-control"  type="text" name="fname" id="fname" onChange={ this.changeFN }/>
                <small id="firstNameHelp" >0 to 20 chars</small>
              </div>
              <div className="col-2">
                &nbsp;
              </div>
              <div className="col-5">
                <label htmlFor="lname" className={ this.props.lnClass }>Last Name</label>
                <input className="form-control"  type="text" name="lname" id="lname" onChange={ this.changeLN }/>
                <small id="lastNameHelp">0 to 20 chars</small>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <label htmlFor="email" className={ this.props.emClass }>Email (required)</label>
                <input type="email" className="form-control" id="email" aria-describedby="emailHelp" onChange={ this.changeEmail }/>
                <small id="emailHelp">Will not be share</small>
              </div>
            </div>
            <div className="row">
              <div className="col-7">
                <label htmlFor="phone" className={ this.props.pnClass }>Phone (required)</label>
                <input type="text" className="form-control" id="phone" aria-describedby="phoneHelp" onChange={ this.changePhone }/>
                <small id="phoneHelp">Will not be share</small>
              </div>
            </div>
            <div className="row pb-2">
              <div className="col-12">
                <label htmlFor="comments" className={ this.props.cmtClass }>Comment (required)</label>
                <textarea className="form-control" id="comments" rows="4" aria-describedby="textHelp" onChange={ this.changeText } value={this.props.comments }/>
                <small id="textHelp">{ textChars }</small>
              </div>
            </div>
          </div>
          <div className="col-1">&nbsp;</div>
        </div>
        <div className="row pb-4">
          <div className="col-12">
            <ReCaptcha
              ref={ref => this.recaptcha = ref}
              sitekey={ this.props.sitekey }
              action='submit'
              verifyCallback={ this.verifyCallback }
            />
          </div>
        </div>
        <div className="row">
          <div className="col-12 pt-4">
            <button type="button" className="btn btn-link ml-2" onClick={ this.prevStep }>Back</button>
            <button id="submit" className="btn btn-primary float-right mr-4" disabled={ ! this.props.canSubmit } onClick={ this.doSubmit }>{ buttonText }</button>
          </div>
        </div>
      </div>
    );
  }

};

export default RequestQuote;
