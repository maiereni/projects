import React, { Component } from 'react';
import create_UUID from './Utils';

class SelectCPU extends Component {

  changeSelected = (e) => {
    let name = e.target.value;
    this.props.changeSelectedCPU(name);
  };

  changeSelectedFamily = (e) => {
    let name = e.target.value;
    this.props.changeSelectedFamily(name);
  };

  nextStep = () => {
    this.props.nextStep("tab2");
  };

  render() {
    let cls = 'tab-pane fade';
    if (this.props.step === 'tab1') {
      cls = cls + ' show active';
    }
    let procFam = this.props.cpuFamilies.map( (p) => {
      return (
        <option value={ p.key } key={ p.key } defaultValue={ p.selected }>{ p.name }</option>
      )
    });
    let proc = this.props.cpus.map( (p) => {
      return (
        <option value={ p.key } key={ p.key } defaultValue={ p.selected }>{ p.name }</option>
      )
    });

    let description = '', img = '', title = '';
    this.props.cpus.forEach( p => {
      if (p.selected) {
        img = p.thumbnail;
        title = p.description;
        let ar = p.description.split(':');
        let n = 0;
        description = ar.map( (item, ix) => {
          n = n + 1;
          let u = create_UUID();
          if (n == 1) {
            return <h2 key={u}>{ item }</h2>;
          }
          else {
            return <p key={u}>{ item }</p>;
          }
        });
      }
    });

    return (
      <div className={ cls } id="v-pills-cpu" role="tabpanel" aria-labelledby="v-pills-cpu-tab">
        <div className="row pb-4 panel-body">
          <div className="col-1">&nbsp;</div>
          <div className="col-10">
            <div className="row pb-4 pt-4">
              <div className="col-12">
                <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" name="cpu" id="cpu1" value="Intel" defaultChecked="true" onClick={ () => this.props.changeCPU('Intel') }/>
                  <label className="form-check-label" htmlFor="cpu1">Intel&nbsp;</label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" name="cpu" id="cpu2" value="AMD" onClick={ () => this.props.changeCPU('AMD') }/>
                  <label className="form-check-label" htmlFor="cpu2">AMD&nbsp;</label>
                </div>
              </div>
            </div>
            <div className="row pb-8">
              <div className="col-12 form-group">
                <label htmlFor="cpu3">Select the CPU family</label>
                <select className="form-control" id="cpu3" onChange={ this.changeSelectedFamily }>{ procFam }</select>
              </div>
            </div>
            <div className="row pb-8">
              <div className="col-12 form-group">
                <label htmlFor="cpu4">Select the CPU</label>
                <select className="form-control" id="cpu4" onChange={ this.changeSelected }>{ proc }</select>
              </div>
            </div>
            <div className="row pb-4 pt-4">
              <div className="col-12 sel-desc">{ description }<br/><img src={ img } title={ title }></img></div>
            </div>
          </div>
          <div className="col-1">&nbsp;</div>
        </div>
        <div className="row">
          <div className="col-12 pt-4">
            <button id="next1" className="btn btn-primary float-right mr-4" onClick={ this.nextStep }>Next</button>
          </div>
        </div>
      </div>
    );
  }

  componentDidUpdate() {
    let selFam = '';
    this.props.cpuFamilies.forEach( p => {
      if (p.selected) {
        selFam = p.key;
      }
    });
    if (selFam !== '') {
      this.props.changeSelectedFamily(selFam);
    }
  }
};

export default SelectCPU;
