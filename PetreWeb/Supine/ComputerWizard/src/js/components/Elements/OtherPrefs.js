import React from 'react';

const otherPrefs = (props) => {
  let nextStep = function() {
    props.nextStep("tab4");
  };
  let prevStep = function() {
    props.nextStep('tab2');
  };
  let cls = 'tab-pane fade';
  if (props.step === 'tab3') {
    cls = cls + ' show active';
  }
  let changeMemory = (e) => {
    let s = e.target.value;
    props.changeSelectedMemory(s);
  };

  let changeHDD = (e) => {
    let s = e.target.value;
    props.changeSelectedHDD(s);
  };
  return (
    <div className={ cls } id="v-pills-pref" role="tabpanel" aria-labelledby="v-pills-pref-tab">
      <div className="row pb-4 panel-body">
        <div className="col-1">&nbsp;</div>
        <div className="col-10">
          <div className="row pb-4 pt-4">
            <div className="col-12 form-group">
              <label htmlFor="memory">Select RAM:</label>
              <select className="form-control" id="memory" onChange={ changeMemory }>
                <option>8GB</option>
                <option defaultValue="true">16GB</option>
                <option>32GB</option>
              </select>
            </div>
          </div>
          <div className="row pb-4">
            <div className="col-12 form-group">
              <label htmlFor="storage">Select HDD/SDD:</label>
              <select className="form-control" id="storage" onChange={ changeHDD }>
                <option>256GB SDD</option>
                <option defaultValue="true">500GB SDD</option>
                <option>1T SDD</option>
              </select>
            </div>
          </div>
        </div>
        <div className="col-1">&nbsp;</div>
      </div>
      <div className="row">
        <div className="col-12 pt-4">
          <button type="button" className="btn btn-link ml-2" onClick={prevStep}>Back</button>
          <button id="next3" className="btn btn-primary float-right mr-4" onClick={ nextStep }>Next</button>
        </div>
      </div>
    </div>
  );
};

export default otherPrefs;
