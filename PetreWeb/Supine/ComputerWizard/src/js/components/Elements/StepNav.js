import React from 'react';
import { Nav } from 'react-bootstrap';

const stepNav = (props) => {

  return (
    <div className="row">
      <div className="col-2">
        <a href="https://www.gamertech.ca">Home</a>
      </div>
      <div className="col-10">
        <ul className="nav nav-pills" id="pills-tab" role="tablist">
          <li className={ props.step === 'tab1' ? "nav-item nav-item-active" : "nav-item" }>
            <a id="v-pills-cpu-tab" href="#" data-toggle="pill" className={ props.step === 'tab1' ? "nav-link active" : "nav-link" } role="tab"
               aria-controls="v-pills-cpu" aria-selected="{ props.step === 'tab1' }" onClick={ () => props.changeStep('tab1') }>CPU</a>
          </li>
          <li className={ props.step === 'tab2' ? "nav-item nav-item-active" : "nav-item" }>
            <a id="v-pills-video-tab" href="#" data-toggle="pill" className={ props.step === 'tab2' ? "nav-link active" : "nav-link" } role="tab"
               aria-controls="v-pills-video" aria-selected="{ props.step === 'tab2' }"  onClick={ () => props.changeStep('tab2') }>Video</a>
          </li>
          <li className={ props.step === 'tab3' ? "nav-item nav-item-active" : "nav-item" }>
            <a id="v-pills-pref-tab" href="#" data-toggle="pill" className={ props.step === 'tab3' ? "nav-link active" : "nav-link" } role="tab"
               aria-controls="v-pills-pref" aria-selected="{ props.step === 'tab3' }"  onClick={ () => props.changeStep('tab3') }>Other</a>
          </li>
          <li className={ props.step === 'tab4' ? "nav-item nav-item-active" : "nav-item" }>
            <a id="v-pills-submit-tab" href="#" data-toggle="pill" className={ props.step === 'tab4' ? "nav-link active" : "nav-link" } role="tab"
               aria-controls="v-pills-submit" aria-selected="{ props.step === 'tab4' }"  onClick={ () => props.changeStep('tab4') }>Quote</a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default stepNav;
