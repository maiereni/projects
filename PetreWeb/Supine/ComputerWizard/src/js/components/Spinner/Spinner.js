import React from 'react';
import classes from './Spinner.css';

let spinner = () => {
  return (
    <div className={ classes.loader }>Loading...</div>
  );
}

export default spinner;
