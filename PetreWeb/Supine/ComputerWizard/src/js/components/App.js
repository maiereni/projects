
import React, { Component } from 'react';
import axios from 'axios';
import StepNav from './Elements/StepNav';
import Panels from './Elements/Panels';
import create_UUID from './Elements/Utils';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step : "tab1",
      cpu : 'Intel',
      selCPUFam: '',
      selCPU : '',
      cpuThumbnailURL: '',
      selVideoMaker : '',
      selVideo : '',
      selVideoCard : '',
      videoCardThumbnailURL: '',
      selMemory : '16GB',
      selHDD : '500GB SDD',
      firstName : '',
      lastName : '',
      email : '',
      phone : '',
      comments : '',
      processorFamilies: [],
      processors: [],
      videoCardMakers: [],
      videoCards: [],
      others1: [],
      others2: [],
      posting: false,
      complete: false
    };
  }

  changeStep = (s) => {
    this.setState({
      step: s
    });
  }

  changeCPU = (s) => {
    this.setState({
      cpu: s,
      selCPUFam: '',
      selCPU: '',
      processors: []
    });
  }
  changeSelectedFamily = (s) => {
    if (this.state.selCPUFam !== s) {
      this.setState( {
        selCPUFam: s,
        processors: []
      });
      this.loadCPUs(s);
    }
  };

  changeSelectedCPU = (s) => {
    if (this.state.selCPU !== s) {
      this.setState({
        selCPU: s
      });
    }
  };

  changeSelectedMaker = (s) => {
    if (this.state.selVideoMaker !== s) {
      let defGru;
      this.state.videoCardMakers.forEach( p => {
        if (p.key === s) {
          defGru = p.videoCards[0].key;
        }
      });
      this.loadVideos(s, defGru);
    }
  };

  changeSelectedVideo = (s) => {
    if (this.state.selVideo !== s) {
      this.loadVideos(this.state.selVideoMaker, s);
    }
  };

  changeSelectedVideoCard = (s) => {
    if (this.state.selVideoCard !== s) {
      this.setState( {
        selVideoCard: s
      });
    }
  }

  changeSelectedMemory = (s) => {
    if (this.state.setMemory !== s) {
      this.setState ({
        selMemory: s
      })
    }
  };

  changeSelectedHDD = (s) => {
    if (this.state.selHDD !== s) {
      this.setState( {
        selHDD: s
      });
    }
  };

  setFirstName = (s) => {
    this.setState( {
      firstName: s
    })
  }

  setLastName = (s) => {
    this.setState( {
      lastName: s
    })
  }

  isNameError = (s) => {
    let r = true;
    if (/^[a-zA-Z]{0,10}$/.test(s)) {
      r = false;
    }
    return r;
  }

  setEmail = (s) => {
    this.setState( {
      email: s
    })
  }

  isEmailError = () => {
    let r = true;
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(this.state.email)) {
      r = false;
    }
    return r;
  }

  setPhone = (s) => {
    this.setState( {
      phone: s
    })
  }

  isPhomeError = () => {
    let r = true;
    if (/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(this.state.phone)) {
      r = false;
    }
    return r;
  }

  isCommentError = () => {
    let r = true;
    if (/^[\w\s.,|]{1,250}$/.test(this.state.comments)) {
      r = false;
    }
    return r;
  }

  setComment = (s) => {
    if (/^[\w\s.,|]{1,250}$/.test(s)) {
      this.setState({
        comments: s
      });
    }
    else {
      console.log('The text is too large');
    }
  }

  getProcessorFamilies = function () {
    let ret = [];
    let n = 1;
    this.state.processorFamilies.forEach( (p, ix) => {
      let name = p.name;
      if (name.toString().startsWith(this.state.cpu)) {
        let selected = false;
        if ((this.state.selCPUFam === '' && n === 1) || (this.state.selCPUFam === p.key)) {
          selected = true;
        }
        let description = p.description;
        if (description === '') {
          description = p.name;
        }
        let props = {key: p.key, name: p.name, description: description, thumbnail: p.image, selected: selected};
        ret.push(props);
        n = n + 1;
      }
    });
    return ret;
  }

  getProcessors = function() {
    let processors = [];
    this.state.processors.forEach( (p, ix) => {
      let name = p.name;
        let selected = false;
        if ((this.state.selCPU === '' && ix === 1) || (this.state.selCPU === p.key)) {
          selected = true;
        }
        let description = p.description;
        if (description === '') {
          description = p.name;
        }
        let props = {key: p.key, name: p.name, description: description, thumbnail: p.image, selected: selected};
        processors.push(props);
    });
    return processors;
  }

  getVideoCardMakers = function() {
    let makers = [];
    this.state.videoCardMakers.forEach( e => {
      let selected = false;
      if (this.state.selVideoMaker == e.key) {
        selected = true;
      }
      let props = { key: e.key, name: e.name, videoCards: e.videoCards, selected: selected };
      makers.push(props);
    });
    return makers;
  };

  getVideoCards = function () {
    let videos = [];
    if (this.state.videoCards) {
      this.state.videoCards.forEach( (v , ix) => {
        let selected = false;
        if ((this.state.selVideoCard === '' && ix === 0) || ( v.key === this.state.selVideoCard )) {
          selected = true;
        }
        let props = { key: v.key, name: v.name, description: v.description, thumbnail: v.image, selected: selected };
        videos.push( props );
      })
    }
    return videos;
  }

  render() {
    let canSubmit = !(this.isEmailError() || this.isPhomeError() || this.state.posting);
    if (this.state.complete) {
      return (
        <div className="container-fluid wzd2" >
          <div className="row">
            <div className="col-12">
              <h2>Thank you</h2>
              <br/>
              <p>
                We will get back to you as soon as we can with a price estimate and for further discussions on the purchase
                Meanwhile, please visit our website at <a href="https://www.gamertech.ca" title="Gametech.ca">Gamertech.ca</a>
              </p>
            </div>
          </div>
        </div>
      );
    }
    else {
      return (
        <div className="container-fluid wzd2" >
          <div className="row">
            <div className="col-12">
              <StepNav step={ this.state.step } changeStep={ this.changeStep } />
              <Panels step={ this.state.step }
                      changeStep={ this.changeStep }
                      cpuFamilies = { this.getProcessorFamilies() }
                      cpus={ this.getProcessors() }
                      cards={ this.getVideoCardMakers() }
                      videoCards={ this.getVideoCards() }
                      memories={ this.state.memories }
                      changeCPU={ this.changeCPU }
                      changeSelectedFamily = { this.changeSelectedFamily }
                      changeSelectedCPU = { this.changeSelectedCPU }
                      changeSelectedMaker = { this.changeSelectedMaker }
                      changeSelectedVideo = { this.changeSelectedVideo }
                      changeSelectedVideoCard = { this.changeSelectedVideoCard }
                      changeSelectedMemory = { this.changeSelectedMemory }
                      changeSelectedHDD = { this.changeSelectedHDD }
                      setFirstName = { this.setFirstName }
                      setLastName = { this.setLastName }
                      setEmail = { this.setEmail }
                      setPhone = { this.setPhone }
                      canSubmit = { canSubmit }
                      fnClass = { this.isNameError( this.state.firstName ) ? 'text-danger' : '' }
                      lnClass = { this.isNameError( this.state.lastName ) ? 'text-danger' : '' }
                      emClass = { this.isEmailError() ? 'text-danger' : '' }
                      pnClass = { this.isPhomeError() ? 'text-danger' : '' }
                      cmtClass = { this.isCommentError() ? 'text-danger' : '' }
                      sitekey = { this.props.sitekey ? this.props.sitekey : 'unknown' }
                      setComment = { this.setComment }
                      comments = { this.state.comments }
                      doSubmit = { this.submitForm }
                      isSubmitting = { this.state.posting }
              >
              </Panels>
            </div>
          </div>
        </div>
      );
    }
  }

  loadFamilies = () => {
    const backendUrl = this.props.config + '/selectors';
    axios.get(backendUrl, {
      crossDomain: true
    }).then(r => {
      this.setState({
        processorFamilies: r.data.processors,
        memories: r.data.memories
      });
      this.setDefaultVideoCardSelectors(r.data.videoCards);
    }).catch(reason => {
      console.log('error: ' + reason);
    });
  };

  setDefaultVideoCardSelectors = (videoCardMakers) => {
    if (videoCardMakers) {
      let cards = [];
      let selVideoMaker;
      Object.keys(videoCardMakers).forEach( (c,ix) => {
        let o = videoCardMakers[c];
        let n = c.split(':');
        let fn = n[0];
        cards.forEach( (card) => {
          if (card.name === fn) {
            card.videoCards.push(o);
            fn = null;
          }
        });
        if (fn !== null) {
          let selected = false;
          let key = create_UUID();
          if ( ix === 0 ) {
            selected = true;
            selVideoMaker = key;
          }
          let props = { key: key, name: fn, videoCards: new Array(), selected: selected };
          props.videoCards.push(o);
          cards.push(props);
        }
      });
      this.setState({
        videoCardMakers: cards
      });
      if (selVideoMaker) {
        this.changeSelectedMaker(selVideoMaker);
      }
    }
  }

  loadCPUs = (s) => {
    const backendUrl = this.props.config + '/cpus?id=' + s;
    axios.get(backendUrl, {
      crossDomain: true
    }).then(r => {
      let selCpu = '';
      if (r.data.cpus.length > 0) {
        selCpu = r.data.cpus[0].key;
      }
      this.setState({
        processors: r.data.cpus,
        selCPU: selCpu
      });
    }).catch(reason => {
      console.log('error: ' + reason);
    });
  };

  loadVideos = (maker, gru) => {
    const backendUrl = this.props.config + '/videos?id=' + gru;
    axios.get(backendUrl, {
      crossDomain: true
    }).then(r => {
      let selVideoCard = '';
      if (r.data.cards.length > 0) {
        selVideoCard = r.data.cards[0].key;
      }
      this.setState({
        selVideoMaker: maker,
        selVideo: gru,
        selVideoCard: selVideoCard,
        videoCards : r.data.cards
      });
    }).catch(reason => {
      console.log('error: ' + reason);
    });
  };

  submitForm = () => {
    this.setState( {
      posting: true
    });
    const backendUrl = this.props.config + '/requestQuote';
    /* var data = new FormData();
    data.set('proc', this.state.selCPU);
    data.set('videoCard',this.state.selVideoCard);
    data.set('mem',this.state.selMemory);
    data.set('hdd',this.state.selHDD);
    data.set('fn',this.state.firstName);
    data.set('ln',this.state.lastName);
    data.set('em',this.state.email);
    data.set('ph',this.state.phone);
    data.set('comments',this.state.comments);
    */
    const data = {
      proc:this.state.selCPU,
      videoCard:this.state.selVideoCard,
      mem:this.state.selMemory,
      hdd:this.state.selHDD,
      fn:this.state.firstName,
      ln:this.state.lastName,
      em:this.state.email,
      ph:this.state.phone,
      comments:this.state.comments
    };

    axios.post(backendUrl, data, {crossDomain: true}).then(r => {
      console.log("submitted");
      this.setState( {
        posting: false,
        complete: true
      });
    }).catch(e => {
      console.log('error: ' + e);
      this.setState( {
        posting: false,
        complete: true
      });
    });
  };

  // Here I do a server call
  componentDidMount() {
    if ( this.state.processorFamilies.length === 0 ) {
      this.loadFamilies();
    }
  }
}

export default App;
