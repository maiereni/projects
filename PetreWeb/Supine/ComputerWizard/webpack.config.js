const webpack = require('webpack');
const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.css$/i,
        use: [
           'style-loader',
            'css-loader'
        ]
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    contentBasePublicPath: '/supine/njs',
    compress: true,
    bonjour: true,
    allowedHosts: [
      'localhost', 'mylocaldomain.com'
    ],
    headers: {
      'host': 'mylocaldomain.com',
      'x-forwarded-for': '127.0.0.1',
      'x-forwarded-host': 'mylocaldomain.com',
      'x-forwarded-server': 'mylocaldomain.com'
    },
    clientLogLevel: 'debug',
    port: 8082
  },
  optimization: {
    minimizer: [new UglifyJsPlugin()],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};
