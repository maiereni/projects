#!/bin/bash
rm -rf /var/www/html/app
cp -R ./app /var/www/html/app
./stop.sh
java ${options} -jar ../libs/Database-${project.version}-jar-with-dependencies.jar
./start.sh
apachectl stop
apachectl start

