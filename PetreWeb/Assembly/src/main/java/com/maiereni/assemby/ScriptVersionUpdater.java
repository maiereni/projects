/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.assemby;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.util.Properties;

/**
 * A Maven plugin that updates the project scripts based on the build version
 * @author Petre Maierean
 */
@Mojo(name = "version-updater", defaultPhase = LifecyclePhase.PACKAGE)
public class ScriptVersionUpdater extends AbstractMojo {
    @Parameter( property = "version", defaultValue = "${project.version}" )
    private String version;
    @Parameter( property = "outputDirectory", defaultValue = "${project.build.outputDirectory}")
    private String outputDirectory;
    @Parameter( property = "excludeProperties", defaultValue = "(java|awt|sun|mvn|jdk|maven|idea|user|org|file|guice|os|intellij|jboss|classworlds|line|path).*")
    private String excludeProperties;
    @Parameter( property = "includeProperties", defaultValue = "(javax\\x2Enet\\x2Essl).*")
    private String includeProperties;
    private static final String PLACEHOLDER_VERSION = "${project.version}";
    private static final String PLACEHOLDER_OPTIONS = "${options}";

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File fDir = new File(outputDirectory, "scripts");
        getLog().debug("Update scripts for version " + version + " at " + fDir.getAbsolutePath());
        if (fDir.isDirectory()) {
            File[] children = fDir.listFiles();
            for(File child: children) {
                updateVersion(child, version);
            }
        }
    }

    private void updateVersion(File script, String version) throws MojoExecutionException {
        getLog().debug("Update version " + version + " in script " + script.getName());
        try {
            boolean hasChanges = false;
            String txt = FileUtils.readFileToString(script, "UTF-8");
            int ix = txt.indexOf(PLACEHOLDER_VERSION);
            if (ix > 0) {
                String actual = txt.substring(0, ix) + version + txt.substring(ix + PLACEHOLDER_VERSION.length());
                txt = actual;
                hasChanges = true;
            }
            ix = txt.indexOf(PLACEHOLDER_OPTIONS);
            if (ix > 0) {
                String actual = txt.substring(0, ix) + getOptions() + txt.substring(ix + PLACEHOLDER_OPTIONS.length());
                txt = actual;
                hasChanges = true;
            }
            FileUtils.writeStringToFile(script, txt, "UTF-8");
        }
        catch (Exception e) {
            getLog().error(e);
            throw new MojoExecutionException("Failed to process script at " + script.getPath());
        }
    }

    private String getOptions() {
        StringBuffer sb = new StringBuffer();
        Properties props = System.getProperties();
        for (Object k: props.keySet()) {
            String key = k.toString();
            if (excludeProperties != null && key.matches(excludeProperties)) {
                if (!(includeProperties != null && key.matches(includeProperties))) {
                    continue;
                }
            }
            String value = props.getProperty(key);
            if (sb.length() > 0)
                sb.append(" ");
            sb.append("-D").append(key).append("=").append(value);
        }
        getLog().info("The options are:\r\n" + sb.toString());
        return sb.toString();
    }
}
