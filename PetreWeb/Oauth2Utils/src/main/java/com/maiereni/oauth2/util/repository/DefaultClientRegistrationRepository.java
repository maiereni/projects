/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maiereni.oauth2.util.PropertyLoaderStrategy;
import com.maiereni.oauth2.util.config.Oauth2Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;

import javax.cache.Cache;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Petre Maierean
 */
public class DefaultClientRegistrationRepository implements ClientRegistrationRepository,
        Iterable<ClientRegistration> {
    public static final String AUTHORIZATION_GRANT_TYPE = "authorization.grant.type";
    public static final String CLIENT_ID = "client_id";
    private static final Logger logger = LogManager.getLogger(DefaultClientRegistrationRepository.class);
    Cache<String, ClientRegistration> clientRegistrationCache;
    ObjectMapper objectMapper;
    PropertyLoaderStrategy propertyLoaderStrategy;

    public DefaultClientRegistrationRepository(Cache<String, ClientRegistration> clientRegistrationCache,
                                               ObjectMapper objectMapper,
                                               PropertyLoaderStrategy propertyLoaderStrategy) {
        this.clientRegistrationCache = clientRegistrationCache;
        this.objectMapper = objectMapper;
        this.propertyLoaderStrategy = propertyLoaderStrategy;
    }

    @Override
    public ClientRegistration findByRegistrationId(String registrationId) {
        ClientRegistration ret = null;
        if (propertyLoaderStrategy.isKnown(registrationId)) {
            ret = clientRegistrationCache.get(registrationId);
            if (ret == null) {
                try {
                    Oauth2Properties properties = propertyLoaderStrategy.getProperties(registrationId);
                    ClientRegistration.Builder builder = ClientRegistration.withRegistrationId(registrationId)
                            .authorizationGrantType(getAuthorizationGrantType(properties))
                            .authorizationUri(properties.getAuthorizationUri())
                            .clientId(properties.getClientId())
                            .clientName(properties.getClientName())
                            .clientSecret(properties.getClientSecret())
                            .clientAuthenticationMethod(getClientAuthenticationMethod(properties))
                            .providerConfigurationMetadata(properties.getConfigurationMetadata())
                            .issuerUri(properties.getIssuerUri())
                            .jwkSetUri(properties.getJwkSetUri())
                            .redirectUri(properties.getRedirectUri())
                            .scope(getScopes(properties))
                            .tokenUri(properties.getTokenUri())
                            .userInfoAuthenticationMethod(getAuthenticationMethod(properties))
                            .userInfoUri(properties.getUserInfoUri())
                            .userNameAttributeName(properties.getUserNameAttributeName());
                    ret = builder.build();
                    clientRegistrationCache.put(registrationId, ret);
                }
                catch (Exception e) {
                    logger.error("Could not read properties for the ClientRegistration by id: " + registrationId, e);
                }
            }
        }
        return ret;
    }

    @Override
    public Iterator<ClientRegistration> iterator() {
        Iterator<Cache.Entry<String, ClientRegistration>> i = clientRegistrationCache.iterator();
        List<ClientRegistration> ret = new ArrayList<>();
        while(i.hasNext()) {
            ClientRegistration cr = i.next().getValue();
            if (cr != null) {
                ret.add(cr);
            }
        }
        return ret.iterator();
    }

    protected List<String> getScopes(Oauth2Properties properties ) {
        List<String> ret = new ArrayList<>();
        if (StringUtils.isNotBlank(properties.getScope())) {
            String[] toks = properties.getScope().split(",");
            for(String tok: toks) {
                if (StringUtils.isNotBlank(tok.trim())) {
                    ret.add(tok.trim());
                }
            }
        }
        return ret;
    }

    protected AuthorizationGrantType getAuthorizationGrantType(Oauth2Properties properties) {
        return AuthorizationGrantTypeBuilder.build(properties.getAuthorizationGrantType());
    }

    protected ClientAuthenticationMethod getClientAuthenticationMethod(Oauth2Properties properties) {
        return ClientAuthenticationMethodBuilder.build(properties.getClientAuthenticationMethod());
    }

    protected AuthenticationMethod getAuthenticationMethod(Oauth2Properties properties) {
        return AuthenticationMethodBuilder.build(properties.getAuthenticationMethod());
    }
}

