/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.repository;

import com.maiereni.oauth2.util.OAuth2AuthorizationRequestRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * An implementation of the OAuth2AuthorizationRequestRepository which saves the
 * @author Petre Maierean
 */
public abstract class AbstractOAuth2AuthorizationRequestRepositoryImpl
        implements OAuth2AuthorizationRequestRepository, AuthorizationRequestRepository<OAuth2AuthorizationRequest> {

    @Override
    public void saveAuthorizationRequest(HttpServletRequest request, OAuth2AuthorizationRequest authorizationRequest, String type) {
        if (!(request == null || authorizationRequest == null)) {
            String state = authorizationRequest.getState();
            String req = getFullURL(request);

            setAuthorizationRequest(state, req, type, authorizationRequest);
        }
    }

    @Override
    public OAuth2AuthorizationRequest getAuthorizationRequest(HttpServletRequest request) {
        OAuth2AuthorizationRequest ret = null;
        if (request != null) {
            String state = getStateParameter(request);
            if (StringUtils.isNotBlank(state)) {
                ret = getAuthorizationRequest(state);
            }
        }
        return ret;
    }

    private String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest(HttpServletRequest httpServletRequest) {
        Assert.notNull(httpServletRequest, "request cannot be null");

        return getAuthorizationRequest(httpServletRequest);
    }

    @Override
    public void saveAuthorizationRequest(
            OAuth2AuthorizationRequest oAuth2AuthorizationRequest,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse) {
        saveAuthorizationRequest(httpServletRequest, oAuth2AuthorizationRequest, "shopify");
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest httpServletRequest) {
        OAuth2AuthorizationRequest ret = null;
        if (httpServletRequest != null) {
            String state = getStateParameter(httpServletRequest);
            if (StringUtils.isNotBlank(state)) {
                ret = removeAuthorizationRequest(state);
            }
        }
        return ret;
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest request, HttpServletResponse response) {
        return removeAuthorizationRequest(request);
    }

    public abstract void setAuthorizationRequest(String state, String originalRequest, String type, OAuth2AuthorizationRequest request);

    public abstract OAuth2AuthorizationRequest getAuthorizationRequest(String state);

    public abstract OAuth2AuthorizationRequest removeAuthorizationRequest(String state);

    protected String getStateParameter(HttpServletRequest request) {
        return request.getParameter(OAuth2ParameterNames.STATE);
    }

}
