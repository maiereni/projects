/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.client;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.http.HttpClient;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Map;

/**
 * A utility class that constructs an SSL Client
 * @author Petre Maierean
 */
public class BaseSSLClient {
    public static final String PROTOCOLS = "TLSv1,TLSv1.1,TLSv1.2";
    public static final String TLS1_2 = "TLSv1.2";
    public static final String JAVAX_KEY_STORE = "javax.net.ssl.keyStore";
    public static final String JAVAX_KEY_STORE_PASSWORD = "javax.net.ssl.keyStorePassword";
    public static final String JAVAX_KEY_STORE_TYPE = "javax.net.ssl.keyStoreType";
    public static final String JAVAX_TRUST_STORE = "javax.net.ssl.trustStore";
    public static final String JAVAX_TRUST_STORE_PASSWORD = "javax.net.ssl.trustStorePassword";
    public static final String JAVAX_TRUST_STORE_TYPE = "javax.net.ssl.trustStoreType";

    private File keyStore, trustStore;
    private String keyStoreType, keyStorePassword, trustStoreType, trustStorePassword,
            protocols = "TLSv1,TLSv1.1,TLSv1.2", protocol = TLS1_2;

    public BaseSSLClient() throws Exception {
        this(null, "JKS", null, null, "JKS",
                null, null, null);
    }

    public BaseSSLClient(String sKeyStore, String keyStoreType, String keyStorePassword,
                         String sTrustStore, String trustStoreType, String trustStorePassword,
                         String protocols, String protocol) throws Exception  {
        this.keyStore = new File(System.getProperty(JAVAX_KEY_STORE, sKeyStore));
        this.keyStoreType = System.getProperty(JAVAX_KEY_STORE_TYPE, keyStoreType);
        this.keyStorePassword = System.getProperty(JAVAX_KEY_STORE_PASSWORD, keyStorePassword);
        String actualTrustStore = System.getProperty(JAVAX_TRUST_STORE, sTrustStore);
        if (StringUtils.isNotBlank(actualTrustStore)) {
            this.trustStore = new File(actualTrustStore);
            this.trustStoreType = System.getProperty(JAVAX_TRUST_STORE_TYPE, trustStoreType);
            this.trustStorePassword = System.getProperty(JAVAX_TRUST_STORE_PASSWORD, trustStorePassword);
        }
        else {
            this.trustStore = this.keyStore;
            this.trustStorePassword = this.keyStorePassword;
            this.trustStoreType = this.keyStoreType;
        }
        this.protocols = StringUtils.isNotBlank(protocols)? protocols: PROTOCOLS;
        this.protocol = StringUtils.isNotBlank(protocol)? protocol: TLS1_2;
    }

    private KeyStore loadStore(File f, String type, String password) throws Exception {
        KeyStore keystore = KeyStore.getInstance(type);
        try (InputStream in = new FileInputStream(f)) {
            keystore.load(in, password.toCharArray());
        }
        return keystore;
    }

    protected SSLContext getSSLContext() throws Exception {
        KeyStore keyStore = loadStore(this.keyStore, keyStoreType, keyStorePassword);
        KeyManagerFactory keyManagerFactory =
                KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, keyStorePassword.toCharArray());

        KeyStore trustStore = loadStore(this.trustStore, trustStoreType, trustStorePassword);
        TrustManagerFactory trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(trustStore);

        SSLContext sslContext = SSLContext.getInstance(protocol);
        sslContext.init(
                keyManagerFactory.getKeyManagers(),
                trustManagerFactory.getTrustManagers(),
                new SecureRandom());

        return sslContext;
    }

    public HttpClient getHttpClient() throws Exception {
        HttpClient ret = HttpClient.newBuilder()
                .sslContext(getSSLContext())
                .build();
        return ret;
    }

    public ClientHttpRequestFactory getClientHttpRequestFactory() throws Exception {
        HttpClient client = getHttpClient();
        return new SimpleClientHttpRequestFactory() {
            @Override
            protected HttpURLConnection openConnection(URL url, @Nullable Proxy proxy) throws IOException {
            HttpURLConnection connection = super.openConnection(url, proxy);
            if (connection instanceof HttpsURLConnection)
            try {
                SSLContext sslContext = getSSLContext();
                ((HttpsURLConnection)connection).setSSLSocketFactory(sslContext.getSocketFactory());
            }
            catch (Exception e) {
                throw new IOException("Failed to get the SSL Context", e);
            }
            return connection;
            }
        };
    }

    /**
     * Post request and obtain the response
     * @param url
     * @param props
     * @param p
     * @param <T>
     * @return
     * @throws Exception
     */
    public <T> T postRequest(String url, Map<String, String> props, ParameterizedTypeReference<T> p) throws Exception {
        RestTemplate client = new RestTemplate();
        client.setRequestFactory(getClientHttpRequestFactory());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        for(Map.Entry<String, String> prop: props.entrySet()) {
            map.add(prop.getKey(), prop.getValue());
        }
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<T> response = client.exchange(url, HttpMethod.POST, request, p);
        if (response.getStatusCode().isError()) {
            throw new Exception(response.getStatusCode().toString());
        }
        return response.getBody();
    }
}
