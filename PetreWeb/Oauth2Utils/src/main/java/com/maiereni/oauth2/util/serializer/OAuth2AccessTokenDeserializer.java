/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.serializer;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.core.OAuth2AccessToken;

import java.io.IOException;
import java.time.Instant;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Petre Maierean
 */
public class OAuth2AccessTokenDeserializer  extends StdDeserializer<OAuth2AccessToken> {

    public OAuth2AccessTokenDeserializer() {
        super(OAuth2AccessToken.class);
    }

    @Override
    public OAuth2AccessToken deserialize(JsonParser jsonParser, DeserializationContext ctxt)
        throws IOException, JacksonException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        OAuth2AccessToken.TokenType tokenType = getTokenType(jsonParser, node);
        Instant issuedAt = getInstant(jsonParser, node,"issuedAt");
        Instant expiresAt = getInstant(jsonParser, node,"expiresAt");
        Set<String> scopes = getScopes(jsonParser, node);
        String tokenValue = node.get("tokenValue").textValue();
        return new OAuth2AccessToken(
                tokenType, tokenValue, issuedAt, expiresAt, scopes);
    }

    private Set<String> getScopes(JsonParser jsonParser, JsonNode node)
        throws IOException, JacksonException {
        Set<String> ret = null;
        JsonNode jsonNode = node.get("scopes");
        if (jsonNode != null && jsonNode.isArray()) {
            ret = new HashSet<>();
            Iterator<JsonNode> i = jsonNode.iterator();
            while(i.hasNext()) {
                ret.add(i.next().textValue());
            }
        }
        return ret;
    }
    private Instant getInstant(JsonParser jsonParser, JsonNode node, String key)
            throws IOException, JacksonException {
        Instant ret = null;
        JsonNode jsonNode = node.get(key);
        if (jsonNode != null && jsonNode.isDouble()) {
            Double d = jsonNode.asDouble();
            Long epoque = d.longValue();
            d = ((d - epoque.doubleValue()) * 100000000);
            Long milisec = d.longValue();
            ret = Instant.ofEpochSecond(epoque, milisec);
        }
        return ret;
    }

    private OAuth2AccessToken.TokenType getTokenType(JsonParser jsonParser, JsonNode node)
            throws IOException, JacksonException {
        JsonNode subNode = node.get("tokenType");
        String val = subNode.get("value") != null ? subNode.get("value").asText() : null;
        OAuth2AccessToken.TokenType tokenType = OAuth2AccessToken.TokenType.BEARER;
        if (!(StringUtils.isNotBlank(val) && val.equals(OAuth2AccessToken.TokenType.BEARER.getValue()))) {
            throw new JsonParseException(jsonParser, "Invalid tokenType");
        }
        return tokenType;
    }

    public static void register(ObjectMapper objectMapper) {
        SimpleModule module = new SimpleModule();
        module.addDeserializer(OAuth2AccessToken.class, new OAuth2AccessTokenDeserializer());
        objectMapper.registerModule(module);
    }

}
