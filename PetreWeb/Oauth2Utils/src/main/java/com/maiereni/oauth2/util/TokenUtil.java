/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Petre Maierean
 */
public class TokenUtil {
    /**
     * Returns a specific token from a uri
     * @param token
     * @param uri
     * @return
     */
    public static String getQueryToken(String token, String uri) {
        String ret = null;
        if (StringUtils.isNoneBlank(token, uri)) {
            int ix = uri.indexOf("?");
            if (ix > -1) {
                String query = uri.substring(ix + 1);
                String[] toks = query.split("\\x26");
                for(String tok: toks) {
                    if (tok.startsWith(token)) {
                        ret = tok.substring(token.length() + 1);
                        break;
                    }
                }
            }
        }
        return ret;
    }
}
