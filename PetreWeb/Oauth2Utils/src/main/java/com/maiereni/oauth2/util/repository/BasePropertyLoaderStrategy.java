/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.repository;

import com.maiereni.oauth2.util.PropertyLoaderStrategy;
import com.maiereni.oauth2.util.config.Oauth2Properties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Petre Maierean
 */
public abstract class BasePropertyLoaderStrategy implements PropertyLoaderStrategy {
    public static final String AUTHENTICATION_METHOD = "authentication.method";
    public static final String AUTHORIZATION_GRANT_TYPE = "authorization.grant.type";
    public static final String AUTHORIZATION_URI = "authorization.uri";
    public static final String CLIENT_AUTHENTICATION_METHOD = "client.authentication.method";
    public static final String CLIENT_ID = "client.id";
    public static final String CLIENT_NAME = "client.name";
    public static final String CLIENT_SECRET = "client.secret";
    public static final String CONFIGURATION_METADATA = "configuration.metadata";
    public static final String ISSUER_URI = "issuer.uri";
    public static final String JWK_SET_URI = "jwk.set.uri";
    public static final String REDIRECT_URI = "redirect.uri";
    public static final String SCOPES = "scopes";
    public static final String TOKEN_URI = "token.uri";
    public static final String USER_INFO_URI = "user.info.uri";
    public static final String USER_NAME_ATTRIBUTE = "user.name.attribute.name";

    /**
     * Generate a properties file with the key value pair with blank value
     * @param outputPath
     * @throws Exception if the file cannot be created at the specified path
     */
    public void generateBlankProperties(String outputPath) throws Exception {
        if (StringUtils.isBlank(outputPath)) {
            throw new Exception("The argument is null");
        }
        File fOut = new File(outputPath);
        if (fOut.isDirectory()) {
            throw new Exception("The output path is to a directory");
        }
        File parent = fOut.getParentFile();
        if (!parent.isDirectory()) {
            if (!parent.mkdirs()) {
                throw new Exception("Could not make parent directory at " + parent.getPath());
            }
        }
        Properties properties = getProperties(new Oauth2Properties());
        try(FileOutputStream fos = new FileOutputStream(fOut)) {
            properties.store(fos, "Generated as a sample");
        }
    }

    /**
     * Convert an Oauth2Properties object to a props
     * @param props
     * @return
     */
    protected Properties getProperties(Oauth2Properties props) {
        Properties ret = new Properties();
        ret.setProperty(AUTHENTICATION_METHOD, getValue(props.getAuthenticationMethod(), AuthenticationMethod.HEADER.getValue()));
        ret.setProperty(AUTHORIZATION_GRANT_TYPE, getValue(props.getAuthorizationGrantType(), AuthorizationGrantType.AUTHORIZATION_CODE.getValue()));
        ret.setProperty(AUTHORIZATION_URI, getValue(props.getAuthorizationUri(), "/oauth2/authorization/"));
        ret.setProperty(CLIENT_AUTHENTICATION_METHOD, getValue(props.getClientAuthenticationMethod(), ClientAuthenticationMethod.NONE.getValue()));
        ret.setProperty(CLIENT_ID, getValue(props.getClientId(), "client_id"));
        ret.setProperty(CLIENT_NAME, getValue(props.getClientName(), "client_name"));
        ret.setProperty(CLIENT_SECRET, getValue(props.getClientSecret(), "client_secret"));
        ret.setProperty(CONFIGURATION_METADATA, getConfigurationMetadata(props.getConfigurationMetadata()));
        ret.setProperty(ISSUER_URI, getValue(props.getIssuerUri(), "issuer_uri"));
        ret.setProperty(JWK_SET_URI, getValue(props.getClientName(), "jwk_set_uri"));
        ret.setProperty(REDIRECT_URI, getValue(props.getRedirectUri(), "redirect_uri"));
        ret.setProperty(SCOPES, getValue(props.getScope(), "scopes"));
        ret.setProperty(TOKEN_URI, getValue(props.getTokenUri(), "token_uri"));
        ret.setProperty(USER_INFO_URI, getValue(props.getUserInfoUri(), "user_info_uri"));
        ret.setProperty(USER_NAME_ATTRIBUTE, getValue(props.getUserNameAttributeName(), "user_name_attribute"));
        return ret;
    }

    /**
     * Convert a properties object to an Oauth2Properties and validate the content in that process
     * @param properties
     * @return
     * @throws Exception
     */
    protected Oauth2Properties convert(Properties properties) throws Exception {
        Oauth2Properties ret = new Oauth2Properties();
        StringWriter errorLog = new StringWriter();
        ret.setAuthenticationMethod(getMandatory(properties, AUTHENTICATION_METHOD, errorLog));
        ret.setAuthorizationGrantType(properties.getProperty(AUTHORIZATION_GRANT_TYPE));
        ret.setAuthorizationUri(getMandatory(properties,AUTHORIZATION_URI, errorLog));
        ret.setClientAuthenticationMethod(properties.getProperty(CLIENT_AUTHENTICATION_METHOD));
        ret.setClientId(getMandatory(properties, CLIENT_ID, errorLog));
        ret.setClientName(properties.getProperty(CLIENT_NAME));
        ret.setClientSecret(getMandatory(properties, CLIENT_SECRET, errorLog));
        ret.setConfigurationMetadata(getConfigurationMetadata(properties, errorLog));
        ret.setIssuerUri(properties.getProperty(ISSUER_URI));
        ret.setJwkSetUri(properties.getProperty(JWK_SET_URI));
        ret.setRedirectUri(getMandatory(properties, REDIRECT_URI, errorLog));
        ret.setScope(properties.getProperty(SCOPES));
        ret.setTokenUri(properties.getProperty(TOKEN_URI));
        ret.setUserInfoUri(properties.getProperty(USER_INFO_URI));
        ret.setUserNameAttributeName(properties.getProperty(USER_NAME_ATTRIBUTE));
        String err = errorLog.toString();
        if (err.length() > 0) {
            throw new Exception("Failed to read properties \r\n" + err);
        }
        return ret;
    }

    // The value must be of pattern {key1=value1,key2=value2,...} where value could suffixed by an indicator of type
    // (s) for String, (i) for Integer, (b) for Boolean, (d) for Double
    private static final Pattern PATTERN = Pattern.compile("\\x7B.*\\x7D");
    protected Map<String, Object> getConfigurationMetadata(Properties properties, StringWriter errorLog) {
        String val = properties.getProperty(CONFIGURATION_METADATA);
        return getConfigurationMetadata(val, errorLog);
    }

    protected String getConfigurationMetadata(Map<String, Object> props) {
        String ret = "{}";
        if (props != null) {
            ret = props.keySet()
                    .stream()
                    .map(key -> key + "=" + convert(props.get(key)))
                    .collect(Collectors.joining(", ", "{", "}"));
        }
        return ret;
    }

    protected Map<String, Object> getConfigurationMetadata(String val)  throws Exception {
        final StringWriter errorLog = new StringWriter();
        Map<String, Object> ret = getConfigurationMetadata(val, errorLog);
        String err = errorLog.toString();
        if (err.length() > 0) {
            throw new Exception("Could not convert the string '" + val + "' to a map.\r\n" + err);
        }
        return ret;
    }

    protected Map<String, Object> getConfigurationMetadata(String val, StringWriter errorLog) {
        Map<String, Object> ret = null;
        if (StringUtils.isNotBlank(val)) {
            Matcher matcher = PATTERN.matcher(val);
            if (matcher.matches()) {
                String s = val.substring(0, val.length()-1).substring(1);
                ret = Arrays.stream(s.split(","))
                        .map(entry -> entry.split("="))
                        .collect(Collectors.toMap(entry -> entry[0].trim(), entry -> convert(entry[1].trim(), errorLog)));
            }
        }
        return ret;
    }

    private static final Pattern PATTERN_STRING = Pattern.compile("\\x27.*\\x27");
    private static final Pattern PATTERN_STRING_1 = Pattern.compile(".*\\x28(S|s)\\x29");
    private static final Pattern PATTERN_INTEGER = Pattern.compile("(\\d)*\\x28(I|i)\\x29");
    private static final Pattern PATTERN_BOOLEAN = Pattern.compile("(true|false)\\x28(B|b)\\x29");
    private static final Pattern PATTERN_DOUBLE = Pattern.compile("(\\d)*\\x2E(\\d)*\\x28(D|d)\\x29");

    protected Object convert(String s, StringWriter errorLog) {
        Object ret = null;
        int ix = s.indexOf("(");
        if (PATTERN_STRING.matcher(s).matches()) {
            ret = s.substring(0, s.length() - 1).substring(1);
        }
        else if (ix > 0) {
            String r = s.substring(0, ix);
            if (PATTERN_STRING_1.matcher(s).matches()) {
                ret = r;
            }
            else if (PATTERN_INTEGER.matcher(s).matches()) {
                try {
                    ret = Integer.parseInt(r);
                }
                catch (Exception e) {
                    errorLog.write("Could not convert string '" + r + "' to integer");
                }
            }
            else if (PATTERN_DOUBLE.matcher(s).matches()) {
                try {
                    ret = Double.parseDouble(r);
                }
                catch (Exception e) {
                    errorLog.write("Could not convert string '" + r + "' to integer");
                }
            }
            else if (PATTERN_BOOLEAN.matcher(s).matches()) {
                try {
                    ret = r.toUpperCase().equals("TRUE");
                }
                catch (Exception e) {
                    errorLog.write("Could not convert string '" + r + "' to integer");
                }
            }
        }

        return ret;
    }

    protected String convert(Object o) {
        String ret = "unknown";
        if (o instanceof String) {
            ret = "'" + o.toString() + "'";
        }
        else if (o instanceof Integer) {
            ret = ((Integer)o).toString() + "(i)";
        }
        else if (o instanceof Boolean) {
            ret = ((Boolean)o).toString() + "(b)";
        }
        else if (o instanceof Double) {
            ret = ((Double)o).toString() + "(d)";
        }
        return ret;
    }


    protected String getMandatory(Properties properties, String key, StringWriter errorLog) throws Exception {
        String value = properties.getProperty(key);
        if (StringUtils.isBlank(value)) {
            errorLog.write("The property '");
            errorLog.write(key);
            errorLog.write("' is mandatory to provide\r\n");
        }
        return value;
    }

    protected String getValue(String s, String defaultValue) {
        return StringUtils.isNotBlank(s)? s: defaultValue;
    }
}
