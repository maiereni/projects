/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.serializer;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import java.io.IOException;
import java.util.*;

/**
 * @author Petre Maierean
 */
public class OAuth2AuthorizationRequestDeserializer extends StdDeserializer<OAuth2AuthorizationRequest>  {

    protected OAuth2AuthorizationRequestDeserializer() {
        super(OAuth2AuthorizationRequest.class);
    }

    @Override
    public OAuth2AuthorizationRequest deserialize(JsonParser jsonParser, DeserializationContext ctxt)
        throws IOException, JacksonException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        OAuth2AuthorizationRequest.Builder builder = getBuilder(jsonParser, node);
        builder.additionalParameters(getParameterMap(node, "additionalParameters"));
        builder.attributes(getParameterMap(node, "attributes"));
        builder.authorizationUri(getParameterValue(node,"authorizationUri"));
        builder.authorizationRequestUri(getParameterValue(node, "authorizationRequestUri"));
        builder.clientId(getParameterValue(node, "clientId"));
        builder.redirectUri(getParameterValue(node, "redirectUri"));
        builder.scopes(getParameterValues(node,"scopes"));
        builder.state(getParameterValue(node, "state"));
        return builder.build();
    }

    protected Map<String, Object> getParameterMap(JsonNode node, String key) {
        Map<String, Object> ret = new HashMap<>();
        JsonNode subNode = node.get(key);
        Iterator<String> iter = subNode.fieldNames();
        while(iter.hasNext()) {
            String k = iter.next();
            JsonNode nv = subNode.get(k);
            JsonNodeType nodeType = nv.getNodeType();
            if (JsonNodeType.NUMBER.equals(nodeType)) {
                Integer r = nv.asInt();
                ret.put(k, r);
            }
            else {
                ret.put(k, nv.asText());
            }
        }
        return ret;
    }

    protected Set<String> getParameterValues(JsonNode node, String key) {
        Set<String> ret = new HashSet<>();
        JsonNode subNode = node.get(key);
        if (JsonNodeType.ARRAY.equals(subNode.getNodeType())) {
            Iterator<JsonNode> elements = subNode.elements();
            while(elements.hasNext()) {
                ret.add(elements.next().textValue());
            }
        }
        return ret;
    }

    private String getParameterValue(JsonNode node, String key) {
        JsonNode n = node.findValue(key);
        return n == null ? null : n.asText();
    }

    private OAuth2AuthorizationRequest.Builder getBuilder(JsonParser jsonParser, JsonNode node)
            throws IOException, JacksonException {
        JsonNode grantType = node.get("grantType");
        if (grantType == null) {
            throw MismatchedInputException.from(jsonParser,
                    AuthorizationGrantType.class,
                    "no grantType found in the string");
        }
        String value = grantType.get("value").textValue();
        if (StringUtils.isBlank(value)) {
            throw MismatchedInputException.from(jsonParser,
                    AuthorizationGrantType.class,
                    "value cannot be blank");
        }
        OAuth2AuthorizationRequest.Builder ret = null;
        if (value.equals(AuthorizationGrantType.AUTHORIZATION_CODE.getValue())) {
            ret = OAuth2AuthorizationRequest.authorizationCode();
        }
        else {
            ret = OAuth2AuthorizationRequest.implicit();
        }

        return ret;
    }

    public static void register(ObjectMapper objectMapper) {
        SimpleModule module = new SimpleModule();
        module.addDeserializer(OAuth2AuthorizationRequest.class, new OAuth2AuthorizationRequestDeserializer());
        objectMapper.registerModule(module);
    }
}
