/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util;

import com.maiereni.oauth2.util.config.Oauth2Properties;

/**
 * The API of a Property Loader Strategy
 * @author Petre Maierean
 */
public interface PropertyLoaderStrategy {
    /**
     * Get properties for ClientRegistration object
     * @param registrationId
     * @return
     * @throws Exception
     */
    Oauth2Properties getProperties(String registrationId) throws Exception;

    /**
     * Verifies if the registrationId is known
     * @param registrationId
     * @return
     */
    boolean isKnown(String registrationId);
}
