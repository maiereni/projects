/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.repository;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author Petre Maierean
 */
public class AuthorizationGrantTypeBuilder {
    private static final Map<String, AuthorizationGrantType> AUTHORIZATION_GRANT_TYPE_MAP = init();

    private static Map<String, AuthorizationGrantType> init() {
        Map<String, AuthorizationGrantType> ret = new Hashtable<String, AuthorizationGrantType>();
        ret.put(AuthorizationGrantType.AUTHORIZATION_CODE.getValue(), AuthorizationGrantType.AUTHORIZATION_CODE);
        ret.put(AuthorizationGrantType.CLIENT_CREDENTIALS.getValue(), AuthorizationGrantType.CLIENT_CREDENTIALS);
        ret.put(AuthorizationGrantType.PASSWORD.getValue(), AuthorizationGrantType.PASSWORD);
        ret.put(AuthorizationGrantType.REFRESH_TOKEN.getValue(), AuthorizationGrantType.REFRESH_TOKEN);
        ret.put(AuthorizationGrantType.IMPLICIT.getValue(), AuthorizationGrantType.IMPLICIT);

        return ret;
    }
    /**
     * Builds an authorization grant type
     * @param s
     * @return
     */
    public static AuthorizationGrantType build(String s) {
        AuthorizationGrantType ret = null;
        if (StringUtils.isNotBlank(s)) {
            ret = AUTHORIZATION_GRANT_TYPE_MAP.get(s);
        }
        return ret;
    }
}
