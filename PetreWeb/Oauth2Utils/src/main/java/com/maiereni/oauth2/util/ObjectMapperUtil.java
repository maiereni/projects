/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.maiereni.oauth2.util.serializer.OAuth2AccessTokenDeserializer;
import com.maiereni.oauth2.util.serializer.OAuth2AuthorizationRequestDeserializer;

/**
 * @author Petre Maierean
 */
public class ObjectMapperUtil {

    /**
     * Gets an augmented ObjectMapper which can serialize and deserialize OAuth2 objects properly
     *
     * @return
     */
    public static ObjectMapper getObjectMapper() {
        ObjectMapper ret = new ObjectMapper();
        OAuth2AuthorizationRequestDeserializer.register(ret);
        OAuth2AccessTokenDeserializer.register(ret);
        ret.registerModule(new JavaTimeModule());
        return ret;
    }
}
