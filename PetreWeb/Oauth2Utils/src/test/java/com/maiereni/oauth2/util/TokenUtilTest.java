/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Petre Maierean
 */
public class TokenUtilTest {
    @Test
    public void testExtractTokenNull() {
        assertNull(TokenUtil.getQueryToken(null, null));
        assertNull(TokenUtil.getQueryToken("", null));
        assertNull(TokenUtil.getQueryToken(null, ""));
        assertNull(TokenUtil.getQueryToken("", ""));
    }
    @Test
    public void testExtractStore() {
        assertEquals("123computerbuilder.myshopify.com", TokenUtil.getQueryToken("shop","/mvc/shopify/install.html?hmac=10b699627d3ff4eac6eed54e47ce1dcdbbd896d68f70de175abe94cf85452cb8&host=MTIzY29tcHV0ZXJidWlsZGVyLm15c2hvcGlmeS5jb20vYWRtaW4&locale=en&session=4c04d053c92457d44358f52eeefe44bc5722f412cee18e5b02d25d38ab8f1bfa&shop=123computerbuilder.myshopify.com&timestamp=1633991722"));
    }
}
