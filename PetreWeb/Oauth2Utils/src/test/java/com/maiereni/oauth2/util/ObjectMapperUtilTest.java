/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import static org.junit.Assert.*;

/**
 * Unit test for the ObjectMapperUtil
 * @author Petre Maierean
 */
public class ObjectMapperUtilTest {

    private static final String SAMPLE1 =
            "{\"authorizationUri\":\"https://123computerbuilder.myshopify.com/admin/oauth/authorize\",\"responseType\":{\"value\":\"code\"},\"clientId\":\"24f7206c30eef13beb86709a934360b8\",\"redirectUri\":\"https://mylocaldomain.com/supine/authorize/shopify\",\"scopes\":[\"write_orders\",\"read_products\"],\"state\":\"FlA7DJKJYUg_-l_69FERVet2gAIXdQuD83_rzqmZnm0=\",\"additionalParameters\":{\"shop\":\"123computerbuilder.myshopify.com\"},\"authorizationRequestUri\":\"https://123computerbuilder.myshopify.com/admin/oauth/authorize?response_type=code&client_id=24f7206c30eef13beb86709a934360b8&scope=write_orders%20read_products&state=FlA7DJKJYUg_-l_69FERVet2gAIXdQuD83_rzqmZnm0%3D&redirect_uri=https://mylocaldomain.com/supine/authorize/shopify&shop=123computerbuilder.myshopify.com\",\"attributes\":{\"registration_id\":\"shopify\"},\"grantType\":{\"value\":\"authorization_code\"}}";
    @Test
    public void testUnmarshallOAuth2AuthorizationRequest() {
        ObjectMapper objectMapper = ObjectMapperUtil.getObjectMapper();
        try {
            OAuth2AuthorizationRequest request = objectMapper.readValue(SAMPLE1, OAuth2AuthorizationRequest.class);
            assertNotNull(request);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    private static final String SAMPLE2 =
            "{\"tokenValue\":\"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\",\"issuedAt\":1634990072.525038700,\"expiresAt\":null,\"tokenType\":{\"value\":\"Bearer\"},\"scopes\":[\"read_products\",\"write_orders\"]}";

    @Test
    public void testUnmarshallOAuth2AccessToken() {
        ObjectMapper objectMapper = ObjectMapperUtil.getObjectMapper();
        try {
            OAuth2AccessToken request = objectMapper.readValue(SAMPLE2, OAuth2AccessToken.class);
            assertNotNull(request);
            assertEquals("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", request.getTokenValue());
            assertNotNull(request.getIssuedAt());
            assertEquals("", request.getIssuedAt().toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
