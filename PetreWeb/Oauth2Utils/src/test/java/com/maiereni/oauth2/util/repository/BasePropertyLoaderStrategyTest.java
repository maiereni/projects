/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.oauth2.util.repository;

import com.maiereni.oauth2.util.config.Oauth2Properties;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Unit test class for the BasePropertyLoaderStrategy
 * @author Petre Maierean
 */
public class BasePropertyLoaderStrategyTest {
    private BasePropertyLoaderStrategy strategy;

    @Before
    public void setUp() {
        strategy = new BasePropertyLoaderStrategy() {
            @Override
            public Oauth2Properties getProperties(String registrationId) throws Exception {
                return null;
            }

            @Override
            public boolean isKnown(String registrationId) {
                return false;
            }
        };
    }

    @Test
    public void testGetConfigurationMetadata() {
        final String sProps = "{key1='a method',key2=123(I),key3=true(B),key4=1.22(D)}";
        try {
            Map<String, Object> props = strategy.getConfigurationMetadata(sProps);
            assertNotNull(props);
            assertEquals("a method", props.get("key1"));
            assertEquals(123, props.get("key2"));
            assertEquals(true, props.get("key3"));
            assertEquals(1.22, props.get("key4"));
        }
        catch(Exception e) {
            e.printStackTrace();
            fail("Could not tokenize the string into a map");
        }
    }

    @Test
    public void testGetBlankProperties() {
        Oauth2Properties oauth2Properties = new Oauth2Properties();
        Properties properties = strategy.getProperties(oauth2Properties);
        assertNotNull(properties);
        assertEquals(AuthenticationMethod.HEADER.getValue(), properties.getProperty(BasePropertyLoaderStrategy.AUTHENTICATION_METHOD));
        assertEquals("/oauth2/authorization/", properties.getProperty(BasePropertyLoaderStrategy.AUTHORIZATION_URI));
        assertEquals(AuthorizationGrantType.AUTHORIZATION_CODE.getValue(), properties.getProperty(BasePropertyLoaderStrategy.AUTHORIZATION_GRANT_TYPE));
        assertEquals(ClientAuthenticationMethod.NONE.getValue(), properties.getProperty(BasePropertyLoaderStrategy.CLIENT_AUTHENTICATION_METHOD));
        assertEquals("client_id", properties.getProperty(BasePropertyLoaderStrategy.CLIENT_ID));
        assertEquals("client_name", properties.getProperty(BasePropertyLoaderStrategy.CLIENT_NAME));
        assertEquals("client_secret", properties.getProperty(BasePropertyLoaderStrategy.CLIENT_SECRET));
        assertEquals("{}", properties.getProperty(BasePropertyLoaderStrategy.CONFIGURATION_METADATA));
        assertEquals("issuer_uri", properties.getProperty(BasePropertyLoaderStrategy.ISSUER_URI));
        assertEquals("jwk_set_uri", properties.getProperty(BasePropertyLoaderStrategy.JWK_SET_URI));
        assertEquals("redirect_uri", properties.getProperty(BasePropertyLoaderStrategy.REDIRECT_URI));
        assertEquals("scopes", properties.getProperty(BasePropertyLoaderStrategy.SCOPES));
        assertEquals("token_uri", properties.getProperty(BasePropertyLoaderStrategy.TOKEN_URI));
        assertEquals("user_info_uri", properties.getProperty(BasePropertyLoaderStrategy.USER_INFO_URI));
        assertEquals("user_name_attribute", properties.getProperty(BasePropertyLoaderStrategy.USER_NAME_ATTRIBUTE));
    }

    @Test
    public void testGetConfigurationMetadataConvertMap() {
        Map<String, Object> props = new HashMap<>();
        props.put("key1", "This is a string");
        props.put("key2", 123);
        props.put("key3", Boolean.TRUE);
        props.put("key4", 1.234);
        String s = strategy.getConfigurationMetadata(props);
        assertEquals("{key1='This is a string', key2=123(i), key3=true(b), key4=1.234(d)}", s);
        try {
            Map<String, Object> r = strategy.getConfigurationMetadata(s);
            assertTrue(r.containsKey("key1"));
            assertEquals("This is a string", r.get("key1"));
            assertTrue(r.containsKey("key2"));
            assertEquals(123, r.get("key2"));
            assertTrue(r.containsKey("key3"));
            assertEquals(Boolean.TRUE, r.get("key3"));
            assertTrue(r.containsKey("key4"));
            assertEquals(1.234, r.get("key4"));
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("could not parse the string");
        }
    }

}
