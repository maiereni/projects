#!/bin/bash
sudo sudo

# install Apache
yum update -y
yum install -y httpd.x86_64
yum install -y mod_ssl

# /etc/httpd/conf.d/ssl.conf
# Create a dummy certificate
# /etc/pki/tls/certs/make-dummy-cert localhost.crt

systemctl start httpd.service
systemctl enable httpd.service
echo "Hello World from $(hostname -f)" > /var/www/html/index.html

# install Java 11
amazon-linux-extras install java-openjdk11

