/**
 * ================================================================
 * Copyright (c) 2020-2021 Maiereni Software and Consulting Inc
 * ================================================================
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.maiereni.petreweb.database;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;

/**
 * @author Petre Maierean
 */
public class DatabaseCreator {
    private static final Logger logger = LogManager.getLogger(DatabaseCreator.class);

    public static void main(String[] args) {
        try {
            DerbyProperties props = getProperties(args);
            DataSource dataSource = getDatasource(props);
            applyChanges(dataSource);
        }
        catch(Exception e) {
            logger.error("Failed to create or update the database", e);
        }
    }

    private static void applyChanges(DataSource dataSource) throws Exception {
        logger.debug("Apply changes");
        try (java.sql.Connection connection = dataSource.getConnection();) {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            Liquibase liquibase = new Liquibase("/changelog.xml", new ClassLoaderResourceAccessor(), database);
            liquibase.update(new Contexts(), new LabelExpression());
        }
    }

    private static DataSource getDatasource(DerbyProperties props) throws Exception {
        logger.debug("Create data source");
        BasicDataSource ret = new BasicDataSource();
        ret.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        String url = "jdbc:derby:" + props.getPath() + ";create=true";
        logger.debug("Datasource url is " + url);
        ret.setUrl(url);
        ret.setUsername(props.getUser());
        ret.setPassword(props.getPassword());
        ret.setInitialSize(1);
        ret.setMaxIdle(60);
        ret.setMinIdle(60);
        return ret;
    }

    private static DerbyProperties getProperties(String[] args) throws Exception {
        logger.debug("Read properties");
        DerbyProperties props = new DerbyProperties();
        props.setPath(System.getProperty("db.derby.path"));
        props.setUser(System.getProperty("db.derby.user"));
        props.setPassword(System.getProperty("db.derby.password"));
        if (props.getPath() == null || props.getUser() == null || props.getPassword() == null) {
            throw new Exception("The parameters of the database have not been found");
        }
        return props;
    }

}
