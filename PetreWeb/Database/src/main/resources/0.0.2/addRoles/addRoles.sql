delete from ACCOUNTROLE where NAME in ('ADMINISTRATOR','GUEST','USER');

insert into ACCOUNTROLE (ID, NAME, DESCRIPTION, CREATION_DATE)
values
    ('377208b4-39be-4bcf-be95-dfdc2eb43021','GUEST','A guest', DATE('2021-05-06')),
    ('c5bac6a4-7b29-4da6-9520-4f70d50d6bbd','USER','A simple user', DATE('2021-05-06')),
    ('9a986015-4eec-4780-bebe-6d819a9c0b01','ADMINISTRATOR','An administrator', DATE('2021-05-06'));
