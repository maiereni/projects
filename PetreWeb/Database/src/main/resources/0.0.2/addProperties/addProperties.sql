-- drop table USERPROPERTY;
create table USERPROPERTY
(
    ID CHAR(36) not null primary key,
    USER_ID       CHAR(36) not null,
    NAME           VARCHAR(32) not null,
    VALUE         VARCHAR(256) not null,
    TYPE          VARCHAR(16) not null,
    CREATION_DATE DATE
);
alter table USERPROPERTY add constraint FKrsutqw93jxn1bdsguxhqvq669 foreign key (USER_ID) references ACCOUNTUSER(ID);
