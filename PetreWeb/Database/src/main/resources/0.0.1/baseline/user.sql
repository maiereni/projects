-- drop table ACCOUNTUSER;
create table ACCOUNTUSER
(
    ID CHAR(36) not null primary key,
    USER_ID        VARCHAR(32) not null,
    FIRST_NAME     VARCHAR(64) not null,
    LAST_NAME      VARCHAR(64) not null,
    EMAIL         VARCHAR(256) not null,
    PICTURE_URL    VARCHAR(256),
    CREATION_DATE DATE,
    LAST_LOGIN_DATE DATE
);
