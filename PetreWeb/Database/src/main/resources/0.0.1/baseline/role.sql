-- drop table ACCOUNTROLE;
create table ACCOUNTROLE
(
    ID CHAR(36) not null primary key,
    NAME         VARCHAR(32) not null,
    DESCRIPTION  VARCHAR(256) not null,
    CREATION_DATE DATE
);
