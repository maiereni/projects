-- ALTER TABLE ACCOUNTUSERROLE DROP CONSTRAINT FKrlxeadjm8m7a8xfuh1jb59gf0;
-- ALTER TABLE ACCOUNTUSERROLE DROP CONSTRAINT FKrsutqw93jxn1bdsguxhqvq519;
-- drop table ACCOUNTUSERROLE;

create table ACCOUNTUSERROLE
(
    ID CHAR(36) not null primary key,
    ACTIVE BOOLEAN default false,
    CREATION_DATE DATE,
    ROLE_ID CHAR(36) not null,
    USER_ID CHAR(36) not null
);
alter table ACCOUNTUSERROLE add constraint FKrlxeadjm8m7a8xfuh1jb59gf0 foreign key (ROLE_ID) references ACCOUNTROLE(ID);
alter table ACCOUNTUSERROLE add constraint FKrsutqw93jxn1bdsguxhqvq519 foreign key (USER_ID) references ACCOUNTUSER(ID);
