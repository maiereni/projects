--DELETE FROM CPU_FAMILY;

INSERT INTO CPU_FAMILY
    (ID, NAME, AVAILABLE, ORDER_NUMBER)
VALUES
    ('e70bf219-0098-463d-915d-1291b2301f41','AMD Ryzen 7',True,0),
    ('3735e197-7fe5-4879-89e8-515ffba3df1c','Intel Core i7',True,0),
    ('009a202d-5a89-4879-aca8-16e682554d42','AMD Ryzen 9',True,1),
    ('05cbb395-f790-4658-8aaa-b2d39e048bca','Intel Core i9',True,1),
    ('46ff97e6-693b-4494-87d6-064cb1e21ac6','AMD Ryzen 5',True,2),
    ('44f54780-1cbc-4006-9ba0-b366f9e4ffa4','Intel Core i5',True,2),
    ('fb596370-19d3-4829-86a7-3c4f42a8acdc','AMD Ryzen 3',True,3),
    ('6de53c9c-2314-47dd-ab36-0232b689e3f5','Intel Core i3',True,3),
    ('62a08e8b-59aa-4aa7-a8e9-362443e2ca05','Intel Pentium Gold',True,4),
    ('9e52bffc-6ce9-4037-b71f-a99167507b27','AMD Ryzen Threadripper',True,4),
    ('70832e2b-cd99-4883-941a-b24c981fe5c1','AMD Athlon',False,4)

