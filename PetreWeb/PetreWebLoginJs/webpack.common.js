var webpack = require('webpack');
var jQuery = require("jquery");
//var jqueryDlg = require('webpack-jquery-ui/dialog');
var htmlWebpackPlugin = require('html-webpack-plugin');
var extractTextPlugin = require('extract-text-webpack-plugin');
var miniCssExtractPlugin = require('mini-css-extract-plugin');
var helpers = require('./helpers');

module.exports = {
  entry: {
      'login': './src/login.ts'
  },

  resolve: {
      extensions: [
          '.ts', '.js'
      ]
  },

  module: {
      rules:  [
          {
              test: /\.ts$/,
              exclude: /node_modules/,
              use: 'babel-loader'
          },
          {
              test: /\.html$/,
              use: 'html-loader'
          },
          {
              test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
              use: 'file-loader?name=assets/[name].[hash].[ext]'
          },
          {
              test: /\.css$/,
              exclude: helpers.root('src', 'app'),
              use: [miniCssExtractPlugin.loader, 'css-loader']
          },
      ]
  },

  plugins: [
        /* Use the ProvidePlugin constructor to inject jquery implicit globals */
      new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery'",
            "window.$": "jquery"
      })
  ]
};
