var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common');
var path = require('path');

console.log('Build with webpack DEV')

module.exports = webpackMerge.merge(commonConfig, {
    devtool : 'source-map',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: path.resolve(__dirname, './'),
        filename: '[name].js'
    }
});
