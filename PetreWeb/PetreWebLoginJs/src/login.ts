import 'jquery-ui-bundle';

$(function() {
    $.post('/api/login/list', function(data) {
        console.log(data);
        var s = '';
        $(data).each(function(l, el) {
           s = s + '<li><a class="dropdown-item" href="' + el.link + '">' + el.title + '</a></li>';
        });
        $('#loginDropdown .dropdown-menu').html(s);
    });
});
