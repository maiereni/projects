var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common');
var path = require('path');
var uglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = webpackMerge(commonConfig, {
    devtool : 'source-map',
    mode: 'production',
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: path.resolve(__dirname, './'),
        filename: '[name].js'
    },

    plugins: [
        new uglifyJsPlugin({
          output: {
              ascii_only: true,
              comments: false,
              beautify: false
          }
        })
    ]
});
