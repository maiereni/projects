var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var extractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common');
var path = require('path');
const ENV = process.env.NODE_ENV = process.env.ENV = 'development';

module.exports = webpackMerge.merge(commonConfig, {
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, './dist/js'),
        publicPath: path.resolve(__dirname, './'),
        filename: '[name].js'
    }
});
