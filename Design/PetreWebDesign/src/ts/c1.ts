import 'jquery';

$(function () {
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const cc = $('#emailHelp').css('color');
    const cc1 = $('#textHelp').css('color');
    $('#inputEmail').on('keyup', (e) => {
        const v = $(e.target).val().toString();
        if (v.length === 0) {
            $('#emailHelp').css('color', cc); // this
        }
        else if (!validateEmail(v)) {
            $('#emailHelp').css('color', 'red');
        }
        else {
            $('#emailHelp').css('color', 'green');
        }
    });
    $('#inputText').on('keyup', (e) => {
        const v  = $(e.target).val().toString();
        if (v.length === 0) {
            $('#textHelp').css('color', cc); // this
        }
        else {
            $('#textHelp').html('Expected text of maximum 256 characters. Current: ' + v.length + ' characters');
            if (v.length > 256) {
                $('#textHelp').css('color', 'red');
            }
            else {
                $('#textHelp').css('color', 'green');
            }
        }
    });

});
