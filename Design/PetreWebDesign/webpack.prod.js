var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var extractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common');
var path = require('path');
var UglifyPlugin = require('uglifyjs-webpack-plugin');
const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge.merge(commonConfig, {
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, './dist/js'),
        publicPath: path.resolve(__dirname, './'),
        filename: '[name].js'
    },
    plugins: [
        new UglifyPlugin({
            output: {
                ascii_only: true,
                comments: false,
                beautify: false
            }
        })
    ]
});
