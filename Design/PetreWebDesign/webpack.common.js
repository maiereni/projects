var webpack = require('webpack');
var htmlWebpackPlugin = require('html-webpack-plugin');
var extractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');

module.exports = {
    entry: {
        'contactus': './src/ts/c1.ts'
    },
    resolve: {
        extensions : [ '.ts', '.js' ]
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|gif|jp?g|svg|woff?|ttf|eot|ico)$/,
                loader: 'file-loader'
            },
            {
                test: /\.css$/,
                loader: 'raw-loader'
            }
        ]
    }
};
